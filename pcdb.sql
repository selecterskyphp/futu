-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 01 月 02 日 11:26
-- 服务器版本: 5.5.16
-- PHP 版本: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `pcdb`
--

-- --------------------------------------------------------

--
-- 表的结构 `pc_access`
--

DROP TABLE IF EXISTS `pc_access`;
CREATE TABLE IF NOT EXISTS `pc_access` (
  `role_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `node_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` varchar(50) DEFAULT '',
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `pc_addvalue`
--

DROP TABLE IF EXISTS `pc_addvalue`;
CREATE TABLE IF NOT EXISTS `pc_addvalue` (
  `id` int(11) NOT NULL,
  `oper_type` varchar(32) CHARACTER SET utf8 NOT NULL,
  `account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `oper` varchar(50) CHARACTER SET utf8 NOT NULL,
  `points` int(11) NOT NULL,
  `createtime` datetime NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 表的结构 `pc_application`
--

DROP TABLE IF EXISTS `pc_application`;
CREATE TABLE IF NOT EXISTS `pc_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(24) CHARACTER SET utf8 NOT NULL,
  `createtime` int(11) NOT NULL,
  `query_value` int(11) NOT NULL,
  `state` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_application`
--

INSERT INTO `pc_application` (`id`, `account`, `createtime`, `query_value`, `state`) VALUES
(1, 'sunnyboy', 13445555, 100, '1'),
(2, 'admin', 2434, 1234, '0'),
(3, 'sunnyboy', 13445555, 100, '1'),
(4, 'admin', 2434, 1234, '0');

-- --------------------------------------------------------

--
-- 表的结构 `pc_article`
--

DROP TABLE IF EXISTS `pc_article`;
CREATE TABLE IF NOT EXISTS `pc_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `copyfrom` varchar(255) NOT NULL DEFAULT '',
  `fromlink` varchar(80) NOT NULL DEFAULT '0',
  `description` mediumtext NOT NULL,
  `content` text NOT NULL,
  `template` varchar(30) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `readgroup` varchar(255) NOT NULL DEFAULT '',
  `readpoint` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `url` varchar(50) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_attachment`
--

DROP TABLE IF EXISTS `pc_attachment`;
CREATE TABLE IF NOT EXISTS `pc_attachment` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(8) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(50) NOT NULL DEFAULT '',
  `filepath` varchar(80) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` char(10) NOT NULL DEFAULT '',
  `isimage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isthumb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0',
  `uploadip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_attachment`
--

INSERT INTO `pc_attachment` (`aid`, `moduleid`, `catid`, `id`, `filename`, `filepath`, `filesize`, `fileext`, `isimage`, `isthumb`, `userid`, `createtime`, `uploadip`, `status`) VALUES
(1, 4, 0, 0, '未命名.jpg', './Uploads/201112/4ed725b1558a3.jpg', 85222, 'jpg', 1, 0, 1, 1322722737, '127.0.0.1', 0),
(2, 4, 0, 0, 'soft.jpg', './Uploads/201112/4ed725b1947ac.jpg', 20293, 'jpg', 1, 0, 1, 1322722737, '127.0.0.1', 0),
(3, 4, 0, 0, '未命名.jpg', './Uploads/201112/4ed725b1b731c.jpg', 85222, 'jpg', 1, 0, 1, 1322722737, '127.0.0.1', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_buy_images`
--

DROP TABLE IF EXISTS `pc_buy_images`;
CREATE TABLE IF NOT EXISTS `pc_buy_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(24) CHARACTER SET utf8 NOT NULL,
  `url` varchar(64) CHARACTER SET utf8 NOT NULL,
  `uptime` int(11) NOT NULL,
  `buytime` int(11) NOT NULL,
  `buy_values` int(11) NOT NULL,
  `images_id` int(11) NOT NULL,
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `down_id` varchar(15) CHARACTER SET utf8 NOT NULL,
  `phototitle` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_buy_images`
--

INSERT INTO `pc_buy_images` (`id`, `username`, `url`, `uptime`, `buytime`, `buy_values`, `images_id`, `up_account`, `down_id`, `phototitle`) VALUES
(1, 'photoer', './Uploads/images/b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', 1325175095, 1325402558, 0, 31, 'photoer', '192.168.2.100', ''),
(2, 'photoer', './Uploads/images/12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', 1325411003, 1325265076, 0, 33, 'photoer', '192.168.2.100', ''),
(3, 'photoer', './Uploads/images/12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', 1325411003, 1325265077, 0, 33, 'photoer', '192.168.2.100', '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_category`
--

DROP TABLE IF EXISTS `pc_category`;
CREATE TABLE IF NOT EXISTS `pc_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(30) NOT NULL DEFAULT '',
  `catdir` varchar(30) NOT NULL DEFAULT '',
  `parentdir` varchar(50) NOT NULL DEFAULT '',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `module` char(24) NOT NULL DEFAULT '',
  `arrparentid` varchar(100) NOT NULL DEFAULT '',
  `arrchildid` varchar(100) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `keywords` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ishtml` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `image` varchar(100) NOT NULL DEFAULT '',
  `child` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `url` varchar(100) NOT NULL DEFAULT '',
  `template_list` varchar(20) NOT NULL DEFAULT '',
  `template_show` varchar(20) NOT NULL DEFAULT '',
  `pagesize` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `listtype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `urlruleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `presentpoint` tinyint(3) NOT NULL DEFAULT '0',
  `chargepoint` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `repeatchargedays` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `postgroup` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `listorder` (`listorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- 转存表中的数据 `pc_category`
--

INSERT INTO `pc_category` (`id`, `catname`, `catdir`, `parentdir`, `parentid`, `moduleid`, `module`, `arrparentid`, `arrchildid`, `type`, `title`, `keywords`, `description`, `listorder`, `ishtml`, `ismenu`, `hits`, `image`, `child`, `url`, `template_list`, `template_show`, `pagesize`, `readgroup`, `listtype`, `lang`, `urlruleid`, `presentpoint`, `chargepoint`, `paytype`, `repeatchargedays`, `postgroup`) VALUES
(1, '新闻中心', 'news', '', 0, 2, 'Article', '0', '1,2,3,10,16', 0, '公司新闻11', '公司新闻', '公司新闻', 0, 0, 1, 0, '', 1, '/index.php?m=Article&a=index&id=1', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, ''),
(2, '行业新闻', 'hangye', 'news/', 1, 2, 'Article', '0,1', '2', 0, '行业新闻', '行业新闻', '行业新闻', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=2', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(3, '公司新闻', 'gongsi', 'news/', 1, 2, 'Article', '0,1', '3', 0, '公司新闻', '公司新闻', '公司新闻', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=3', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(4, '产品展示', 'Product', '', 0, 3, 'Product', '0', '4,5,6,7,9,13', 0, '产品展示标题', '产品展示关键词', '产品展示栏目简介', 0, 0, 1, 0, '', 1, '/index.php?m=Product&a=index&id=4', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(5, '产品分类1', 'cp1', 'Product/', 4, 3, 'Product', '0,4', '5', 0, '产品分类1', '产品分类1产品分类1', '产品分类1', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=5', '', '', 0, '2,3,4', 0, 0, 0, 0, 0, 0, 0, ''),
(6, '产品分类2', 'cp2', 'Product/', 4, 3, 'Product', '0,4', '6', 0, '产品分类2', '产品分类2', '产品分类2', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=6', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(7, '产品分类3', 'cp3', 'Product/', 4, 3, 'Product', '0,4', '7', 0, '产品分类3', '产品分类3', '产品分类3', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=7', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(8, '关于我们', 'about', '', 0, 1, 'Page', '0', '8,11,12', 0, '', '', '', 99, 0, 1, 0, '', 1, '/index.php?m=Page&a=index&id=8', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(10, '行业资讯', 'zixun', 'news/', 1, 2, 'Article', '0,1', '10', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=10', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(13, '产品分类5', 'cp5', 'Product/cp4/', 9, 3, 'Product', '0,4,9', '13', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=13', '', 'Product_show', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(9, '产品分类4', 'cp4', 'Product/', 4, 3, 'Product', '0,4', '9,13', 0, '', '', '', 0, 0, 1, 0, '', 1, '/index.php?m=Product&a=index&id=9', '', '', 0, '2,3', 0, 0, 0, 0, 0, 0, 0, ''),
(11, '公司简介', 'info', 'about/', 8, 1, 'Page', '0,8', '11', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Page&a=index&id=11', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(12, '联系我们', 'contactus', 'about/', 8, 1, 'Page', '0,8', '12', 0, '联系我们', '联系我们', '联系我们', 0, 0, 1, 0, '', 0, '/index.php?m=Page&a=index&id=12', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(14, '图片展示', 'pics', '', 0, 4, 'Picture', '0', '14', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Picture&a=index&id=14', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(17, '文档下载', 'down', '', 0, 5, 'Download', '0', '17', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Download&a=index&id=17', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(16, '国内新闻', 'cnnews', 'news/', 1, 2, 'Article', '0,1', '16', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=16', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, ''),
(18, '信息反馈', 'Feedback', 'Guestbook/', 19, 6, 'Feedback', '0,19', '18', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Feedback&a=index&id=18', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '1,2,3,4'),
(19, '在线留言', 'Guestbook', '', 0, 8, 'Guestbook', '0', '19,18', 0, '', '', '', 0, 0, 1, 0, '', 1, '/index.php?m=Guestbook&a=index&id=19', '', '', 5, '', 0, 0, 0, 0, 0, 0, 0, '1,2,3,4');

-- --------------------------------------------------------

--
-- 表的结构 `pc_column`
--

DROP TABLE IF EXISTS `pc_column`;
CREATE TABLE IF NOT EXISTS `pc_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) NOT NULL,
  `secondid` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL,
  `groupid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=314 ;

--
-- 转存表中的数据 `pc_column`
--

INSERT INTO `pc_column` (`id`, `parentid`, `secondid`, `name`, `groupid`) VALUES
(180, 0, 0, '新闻图片', 1000),
(181, 0, 0, '创意图片', 2000),
(183, 0, 0, '历史图片', 1000),
(184, 0, 0, '素材图片', 4000),
(185, 1000, 0, '时事', 1100),
(186, 1000, 0, '体育', 1200),
(187, 1000, 0, '文娱', 1300),
(188, 1000, 0, '财经', 1400),
(189, 1000, 0, '时尚', 1500),
(195, 4000, 0, '自然', 4100),
(196, 4000, 0, '美食', 4200),
(197, 4000, 0, '家居', 4300),
(198, 4000, 0, '旅游', 4400),
(199, 4000, 0, '亲子', 4500),
(200, 4000, 0, '养生', 4600),
(201, 1100, 1000, '中国时事', 1101),
(202, 1100, 1000, '国际时事', 1102),
(210, 1200, 1000, '中国时事', 1201),
(211, 1200, 1000, '国际时事', 1202),
(219, 1300, 1000, '中国时事', 1301),
(220, 1300, 1000, '国际时事', 1302),
(228, 1400, 1000, '中国时事', 1401),
(229, 1400, 1000, '国际时事', 1402),
(237, 1500, 1000, '中国时事', 1501),
(238, 1500, 1000, '国际时事', 1502),
(292, 4100, 4000, '野生动物', 4101),
(293, 4100, 4000, '家庭宠物', 4102),
(294, 4100, 4000, '野生植物', 4103),
(295, 4100, 4000, '景观植物', 4104),
(296, 4100, 4000, '自然风光', 4105),
(297, 4200, 4000, '中国菜系', 4201),
(298, 4200, 4000, '西式菜系', 4202),
(299, 4200, 4000, '日韩料理', 4203),
(300, 4200, 4000, '小吃美食', 4204),
(301, 4200, 4000, '茶饮咖啡', 4205),
(302, 4200, 4000, '蔬菜水果', 4206),
(303, 4200, 4000, '美食制作', 4207),
(304, 4200, 4000, '酒水饮料', 4208),
(305, 4300, 4000, '家庭装饰', 4301),
(306, 4300, 4000, '装修建材', 4302),
(307, 4300, 4000, '家居摆设', 4303),
(308, 4300, 4000, '厨卫用品', 4304),
(309, 4400, 4000, '旅游景点', 4401),
(310, 4400, 4000, '旅游用品', 4402),
(311, 4400, 4000, '酒店旅社', 4403),
(312, 0, 0, '亲子', 4501),
(313, 0, 0, '养生', 4601);

-- --------------------------------------------------------

--
-- 表的结构 `pc_config`
--

DROP TABLE IF EXISTS `pc_config`;
CREATE TABLE IF NOT EXISTS `pc_config` (
  `id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `varname` varchar(20) NOT NULL DEFAULT '',
  `info` varchar(100) NOT NULL DEFAULT '',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `value` text NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- 转存表中的数据 `pc_config`
--

INSERT INTO `pc_config` (`id`, `varname`, `info`, `groupid`, `value`, `type`) VALUES
(1, 'site_name', '网站名称', 2, '富图中国', 2),
(2, 'site_url', '网站网址', 2, 'http://localhost', 2),
(3, 'logo', '网站LOGO', 2, './Public/Images/logo.gif', 2),
(4, 'site_company_name', '企业名称', 2, 'yourphp企业建站系统', 2),
(5, 'site_email', '站点邮箱', 2, 'admin@yourphp.cn', 2),
(6, 'site_contact_name', '联系人', 2, 'liuxun', 2),
(7, 'site_tel', '联系电话', 2, '0317-5022625', 2),
(8, 'site_mobile', '手机号码', 2, '13292793176', 2),
(9, 'site_fax', '传真号码', 2, '0317-5022625', 2),
(10, 'site_address', '公司地址', 2, '河北省沧州市肃宁县宅南村', 2),
(11, 'qq', '客服QQ', 2, '147613338', 2),
(12, 'seo_title', '网站标题', 3, 'yourphp企业网站管理系统-企业建站-企业网站-行业网站建设-门户网站建设', 2),
(13, 'seo_keywords', '关键词', 3, '富图中国', 2),
(14, 'seo_description', '网站简介', 3, '', 2),
(15, 'mail_type', '邮件发送模式', 4, '1', 2),
(16, 'mail_server', '邮件服务器', 4, 'smtp.yourphp.cn', 2),
(17, 'mail_port', '邮件发送端口', 4, '25', 2),
(18, 'mail_from', '发件人地址', 4, 'admin@yourphp.cn', 2),
(19, 'mail_auth', 'AUTH LOGIN验证', 4, '1', 2),
(20, 'mail_user', '验证用户名', 4, 'admin@yourphp.cn', 2),
(21, 'mail_password', '验证密码', 4, '', 2),
(22, 'attach_maxsize', '允许上传附件大小', 5, '5200000', 1),
(23, 'attach_allowext', '允许上传附件类型', 5, 'jpg,jpeg,gif,png,doc,docx,rar,zip,swf', 2),
(24, 'watermark_enable', '是否开启图片水印', 5, '1', 1),
(25, 'watemard_text', '水印文字内容', 5, 'YourPHP', 2),
(26, 'watemard_text_size', '文字大小', 5, '18', 1),
(27, 'watemard_text_color', 'watemard_text_color', 5, '#FFFFFF', 2),
(28, 'watemard_text_face', '字体', 5, 'elephant.ttf', 2),
(29, 'watermark_minwidth', '图片最小宽度', 5, '300', 1),
(30, 'watermark_minheight', '水印最小高度', 5, '300', 1),
(31, 'watermark_img', '水印图片名称', 5, 'mark.png', 2),
(32, 'watermark_pct', '水印透明度', 5, '80', 1),
(33, 'watermark_quality', 'JPEG 水印质量', 5, '100', 1),
(34, 'watermark_pospadding', '水印边距', 5, '10', 1),
(35, 'watermark_pos', '水印位置', 5, '9', 1),
(36, 'PAGE_LISTROWS', '列表分页数', 6, '15', 1),
(37, 'URL_MODEL', 'URL访问模式', 6, '0', 1),
(38, 'URL_PATHINFO_DEPR', '参数分割符', 6, '/', 2),
(39, 'URL_HTML_SUFFIX', 'URL伪静态后缀', 6, '.html', 2),
(40, 'TOKEN_ON', '令牌验证', 6, '1', 1),
(41, 'TOKEN_NAME', '令牌表单字段', 6, '__hash__', 2),
(42, 'TMPL_CACHE_ON', '模板编译缓存', 6, '0', 1),
(43, 'TMPL_CACHE_TIME', '模板缓存有效期', 6, '-1', 1),
(44, 'HTML_CACHE_ON', '静态缓存', 6, '0', 1),
(45, 'HTML_CACHE_TIME', '缓存有效期', 6, '60', 1),
(46, 'HTML_READ_TYPE', '缓存读取方式', 6, '0', 1),
(47, 'HTML_FILE_SUFFIX', '静态文件后缀', 6, '.html', 2),
(48, 'ADMIN_ACCESS', 'ADMIN_ACCESS', 6, '114400dbcf82d642937e34559a21e475', 2),
(49, 'DEFAULT_THEME', '默认模板', 6, 'Default', 2),
(50, 'HOME_ISHTML', '首页生成html', 6, '0', 1),
(51, 'URL_URLRULE', 'URL', 6, '', 2);

-- --------------------------------------------------------

--
-- 表的结构 `pc_credit_detail`
--

DROP TABLE IF EXISTS `pc_credit_detail`;
CREATE TABLE IF NOT EXISTS `pc_credit_detail` (
  `id` int(11) NOT NULL,
  `account` varchar(50) NOT NULL,
  `send_time` int(11) NOT NULL,
  `credit_value` int(11) NOT NULL,
  `pay_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `pc_credit_detail`
--

INSERT INTO `pc_credit_detail` (`id`, `account`, `send_time`, `credit_value`, `pay_time`) VALUES
(0, 'wangxb', 1322756890, 500, 1322756890),
(0, 'wangxb', 1322756890, 500, 1322756890);

-- --------------------------------------------------------

--
-- 表的结构 `pc_dbsource`
--

DROP TABLE IF EXISTS `pc_dbsource`;
CREATE TABLE IF NOT EXISTS `pc_dbsource` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `host` varchar(20) NOT NULL,
  `port` int(5) NOT NULL DEFAULT '3306',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `dbname` varchar(50) NOT NULL,
  `dbtablepre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_download`
--

DROP TABLE IF EXISTS `pc_download`;
CREATE TABLE IF NOT EXISTS `pc_download` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `template` varchar(40) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `readpoint` smallint(5) unsigned NOT NULL,
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `url` varchar(60) NOT NULL DEFAULT '',
  `file` varchar(80) NOT NULL DEFAULT '',
  `ext` varchar(10) NOT NULL DEFAULT '',
  `size` varchar(10) NOT NULL DEFAULT '',
  `downs` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_downloader`
--

DROP TABLE IF EXISTS `pc_downloader`;
CREATE TABLE IF NOT EXISTS `pc_downloader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sex` varchar(8) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(32) CHARACTER SET utf8 NOT NULL,
  `post_code` varchar(16) CHARACTER SET utf8 NOT NULL,
  `province` varchar(32) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `work_company` varchar(128) CHARACTER SET utf8 NOT NULL,
  `new_tip` tinyint(4) NOT NULL,
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `head_image_path` varchar(255) CHARACTER SET utf8 NOT NULL,
  `state` tinyint(11) NOT NULL DEFAULT '0',
  `role` varchar(16) CHARACTER SET utf8 NOT NULL,
  `user_values` int(11) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- 转存表中的数据 `pc_downloader`
--

INSERT INTO `pc_downloader` (`id`, `account`, `name`, `sex`, `password`, `email`, `phone`, `post_code`, `province`, `city`, `address`, `work_company`, `new_tip`, `createtime`, `head_image_path`, `state`, `role`, `user_values`, `discount`) VALUES
(33, 'user', 'user', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '123@126.com', '13857190634', '333333', '广西壮族自治区', '南宁市', '33333', '33343434', 0, 1324886017, '', 0, '下载用户', 0, 95),
(34, 'xieyd2', '谢有定', '1', 'c137340e996daae5fec608dcffd7683442cf8024', 'jingya8825@163.com', '13588038843', '310000', '浙江省', '杭州市', '杭州', '富图', 0, 1325426964, '', 1, '下载用户', 0, 100),
(35, 'cjx', '陈甲新', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '234@qq.com', '13712341231', '320000', '湖南省', '衡阳市', '滨江', '淘宝', 0, 1325494824, '', 0, '下载用户', 0, 100),
(36, 'cjx2324', '111', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '1@q.com', '11111111111', '111111', '西藏自治区', '昌都地区', '1111', '111', 0, 1325494923, '', 1, '下载用户', 0, 100);

-- --------------------------------------------------------

--
-- 表的结构 `pc_editgroup`
--

DROP TABLE IF EXISTS `pc_editgroup`;
CREATE TABLE IF NOT EXISTS `pc_editgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `type_one` int(11) NOT NULL,
  `type_two` int(11) NOT NULL,
  `type_three` int(11) NOT NULL,
  `type_one_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type_two_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type_three_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `main_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `check_time` int(11) NOT NULL,
  `location` varchar(128) CHARACTER SET utf8 NOT NULL,
  `key` varchar(128) CHARACTER SET utf8 NOT NULL,
  `key_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(1024) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- 转存表中的数据 `pc_editgroup`
--

INSERT INTO `pc_editgroup` (`id`, `username`, `up_account`, `up_time`, `type_one`, `type_two`, `type_three`, `type_one_name`, `type_two_name`, `type_three_name`, `main_url`, `check_time`, `location`, `key`, `key_type`, `title`, `remark`) VALUES
(16, 'admin', 'xieyd', 1325443396, 0, 0, 0, '', '', '', './Uploads/thumb//20120102/s_f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', 1325498132, 'London', '', '', 'SPT_CHELSEA_ASTON_VILLA_007', 'Chelsea''s Didier Drogba jokes with Aston Villa''s Stephen Warnock pretending to ride him a like a horse after they tangle. Chelsea are level with Aston Villa 1:1\nChelsea 31/12/11\nChelsea V Aston Villa 31/12/11\nThe Premier League\nPhoto: Richard Washbrooke Fotosports International\n Photo via Newscom');

-- --------------------------------------------------------

--
-- 表的结构 `pc_editgroup_detail`
--

DROP TABLE IF EXISTS `pc_editgroup_detail`;
CREATE TABLE IF NOT EXISTS `pc_editgroup_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `up_acct` varchar(64) CHARACTER SET utf8 NOT NULL,
  `image_id` int(11) NOT NULL,
  `up_time` int(11) NOT NULL,
  `check_time` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `small_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `big_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `is_main_face` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `remark` varchar(512) CHARACTER SET utf8 NOT NULL,
  `key` varchar(128) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `yiming` varchar(50) CHARACTER SET utf8 NOT NULL,
  `texie` int(11) NOT NULL,
  `ren` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `province` varchar(50) CHARACTER SET utf8 NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- 转存表中的数据 `pc_editgroup_detail`
--

INSERT INTO `pc_editgroup_detail` (`id`, `group_id`, `up_acct`, `image_id`, `up_time`, `check_time`, `state`, `url`, `small_url`, `big_url`, `is_main_face`, `points`, `remark`, `key`, `name`, `yiming`, `texie`, `ren`, `position`, `width`, `height`, `size`, `country`, `province`, `city`) VALUES
(36, 16, 'xieyd', 158, 1325443410, 1325498134, 0, './Uploads/images//20120102/f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', './Uploads/thumb//20120102/s_f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', './Uploads/thumb//20120102/m_f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', 0, 0, 'Chelsea''s Didier Drogba celebrates by pointing at the penalty spot after he scores from the penalty spot to give Chelsea a 1:0 lead\r\nChelsea 31/12/11\r\nChelsea V Aston Villa 31/12/11\r\nThe Premier League\r\nPhoto: Richard Washbrooke Fotosports International\r\n', '', '', '', 0, 0, 0, 2448, 1815, 1922, 'England', '', 'London');

-- --------------------------------------------------------

--
-- 表的结构 `pc_feedback`
--

DROP TABLE IF EXISTS `pc_feedback`;
CREATE TABLE IF NOT EXISTS `pc_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `telephone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `ip` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `typeid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_field`
--

DROP TABLE IF EXISTS `pc_field`;
CREATE TABLE IF NOT EXISTS `pc_field` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `field` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `tips` varchar(150) NOT NULL DEFAULT '',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `pattern` varchar(255) NOT NULL DEFAULT '',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(20) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `setup` mediumtext NOT NULL,
  `ispost` tinyint(1) NOT NULL DEFAULT '0',
  `unpostgroup` varchar(60) NOT NULL DEFAULT '',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=103 ;

--
-- 转存表中的数据 `pc_field`
--

INSERT INTO `pc_field` (`id`, `moduleid`, `field`, `name`, `tips`, `required`, `minlength`, `maxlength`, `pattern`, `errormsg`, `class`, `type`, `setup`, `ispost`, `unpostgroup`, `listorder`, `status`, `issystem`) VALUES
(1, 1, 'title', '标题', '', 1, 3, 80, '', '标题必填3-80个字', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''0'',\n  ''size'' => '''',\n)', 1, '', 0, 1, 1),
(2, 1, 'keywords', '关键词', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n)', 1, '', 0, 1, 1),
(3, 1, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(4, 1, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => ''*.jpg;*.jpeg;*.gif;*.doc;*.rar;*.zip;*.xls'',\n)', 1, '', 0, 1, 1),
(5, 2, 'catid', '栏目', '', 1, 1, 6, 'digits', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(6, 2, 'title', '标题', '', 1, 0, 0, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(7, 2, 'keywords', '关键词', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n)', 1, '', 0, 1, 1),
(8, 2, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(9, 2, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''show_add_description'' => ''1'',\n  ''show_auto_thumb'' => ''1'',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 0, 1, 1),
(10, 2, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 0, 1, 1),
(11, 2, 'copyfrom', '来源', '', 0, 0, 0, '0', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(12, 2, 'fromlink', '来源网址', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n)', 1, '', 0, 1, 1),
(13, 2, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''varchar'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 0, 1, 1),
(14, 2, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 0, 1, 1),
(15, 2, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 0, 1, 1),
(16, 2, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 0, 1, 1),
(17, 3, 'catid', '栏目', '', 1, 1, 6, '', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(18, 3, 'title', '标题', '', 1, 1, 80, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(19, 3, 'keywords', '关键词', '', 0, 0, 80, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(20, 3, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(21, 3, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 10, 1, 1),
(22, 3, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(31, 2, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''1'',\n)', 1, '3,4', 0, 0, 0),
(30, 3, 'xinghao', '型号', '', 0, 0, 30, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(25, 3, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 96, 0, 1),
(26, 3, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 97, 1, 1),
(27, 3, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 98, 1, 1),
(28, 3, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(29, 3, 'price', '价格', '', 0, 0, 0, '', '', '', 'number', 'array (\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''2'',\n  ''default'' => ''0'',\n)', 1, '', 0, 1, 1),
(34, 3, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(32, 2, 'readpoint', '阅读收费', '', 0, 0, 3, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(33, 2, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => '''',\n)', 1, '', 0, 0, 0),
(35, 3, 'readpoint', '阅读收费', '', 0, 0, 5, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '3,4', 0, 0, 0),
(36, 3, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '', 0, 0, 0),
(37, 4, 'catid', '栏目', '', 1, 1, 6, '', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(38, 4, 'title', '标题', '', 1, 1, 80, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(39, 4, 'keywords', '关键词', '', 0, 0, 80, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(40, 4, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(41, 4, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 10, 1, 1),
(42, 4, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(43, 4, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(44, 4, 'readpoint', '阅读收费', '', 0, 0, 5, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '3,4', 0, 0, 0),
(45, 4, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '', 0, 0, 0),
(46, 4, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 96, 0, 1),
(47, 4, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 97, 1, 1),
(48, 4, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 98, 1, 1),
(49, 4, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(50, 5, 'catid', '栏目', '', 1, 1, 6, '', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(51, 5, 'title', '标题', '', 1, 1, 80, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(52, 5, 'keywords', '关键词', '', 0, 0, 80, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(53, 5, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(54, 5, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 10, 1, 1),
(55, 5, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(56, 5, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(57, 5, 'readpoint', '阅读收费', '', 0, 0, 5, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '3,4', 0, 0, 0),
(58, 5, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '', 0, 0, 0),
(59, 5, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 96, 0, 1),
(60, 5, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 97, 1, 1),
(61, 5, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 98, 1, 1),
(62, 5, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(63, 3, 'pics', '图片', '', 0, 0, 0, '', '', '', 'images', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''upload_maxnum'' => ''10'',\n  ''upload_maxsize'' => ''2'',\n  ''upload_allowext'' => ''*.jpeg;*.jpg;*.gif'',\n  ''watermark'' => ''0'',\n  ''more'' => ''1'',\n)', 1, '', 0, 1, 0),
(64, 4, 'pics', '图组', '', 0, 0, 0, '', '', '', 'images', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''upload_maxnum'' => ''20'',\n  ''upload_maxsize'' => ''2'',\n  ''upload_allowext'' => ''*.jpeg;*.jpg;*.png;*.gif'',\n  ''watermark'' => ''0'',\n  ''more'' => ''1'',\n)', 1, '', 0, 1, 0),
(65, 5, 'file', '上传文件', '', 0, 0, 0, '', '', '', 'file', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''upload_maxsize'' => ''2'',\n  ''upload_allowext'' => ''*.zip;*.rar;*.doc;*.ppt'',\n  ''more'' => ''1'',\n)', 1, '', 0, 1, 0),
(66, 5, 'ext', '文档类型', '', 0, 0, 10, '', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(67, 5, 'size', '文档大小', '', 0, 0, 10, '', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(68, 5, 'downs', '下载次数', '', 0, 0, 0, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => '''',\n)', 1, '', 0, 0, 0),
(69, 6, 'username', '姓名', '', 1, 2, 20, 'cn_username', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 2, 1, 0),
(70, 6, 'telephone', '电话', '', 0, 0, 0, 'tel', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 4, 1, 0),
(71, 6, 'email', '邮箱', '', 1, 0, 50, 'email', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 2, 1, 0),
(72, 6, 'content', '内容', '', 1, 4, 200, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''5'',\n  ''cols'' => ''60'',\n  ''default'' => '''',\n)', 1, '', 5, 1, 0),
(81, 7, 'status', '状态', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''已审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(73, 6, 'ip', '提交IP', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 0, '3,4', 6, 1, 0),
(74, 6, 'title', '标题', '', 1, 4, 50, '', '', '', 'text', 'array (\n  ''size'' => ''40'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '3,4', 1, 1, 0),
(80, 35, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(76, 6, 'createtime', '提交时间', '', 0, 0, 0, '', '', '', 'datetime', '', 0, '3,4', 98, 1, 0),
(79, 6, 'typeid', '反馈类别', '', 0, 0, 0, '', '', '', 'typeid', 'array (\n  ''inputtype'' => ''select'',\n  ''fieldtype'' => ''smallint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''4'',\n)', 1, '', 0, 1, 0),
(78, 6, 'status', '审核状态', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''己审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''0'',\n)', 0, '3,4', 99, 1, 0),
(82, 7, 'name', '网站名称', '', 1, 2, 50, '', '', '', 'text', 'array (\n  ''size'' => ''40'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 1, 1, 0),
(83, 7, 'logo', '网站LOGO', '', 0, 0, 0, '', '', '', 'image', 'array (\n  ''size'' => ''50'',\n  ''default'' => '''',\n  ''upload_maxsize'' => '''',\n  ''upload_allowext'' => ''jpg,jpeg,gif,png'',\n  ''watermark'' => ''0'',\n  ''more'' => ''0'',\n)', 1, '', 2, 1, 0),
(84, 7, 'siteurl', '网站地址', '', 1, 10, 150, 'url', '', '', 'text', 'array (\n  ''size'' => ''50'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 3, 1, 0),
(85, 7, 'typeid', '友情链接分类', '', 0, 0, 0, '', '', '', 'typeid', 'array (\n  ''inputtype'' => ''select'',\n  ''fieldtype'' => ''smallint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''1'',\n)', 1, '', 0, 1, 0),
(86, 7, 'linktype', '链接类型', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''文字链接|1\r\nLOGO链接|2'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''1'',\n)', 1, '', 0, 1, 0),
(87, 7, 'siteinfo', '站点简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''3'',\n  ''cols'' => ''60'',\n  ''default'' => '''',\n)', 1, '', 4, 1, 0),
(88, 8, 'createtime', '提交时间', '', 1, 0, 0, '', '', '', 'datetime', '', 0, '3,4', 93, 1, 1),
(89, 8, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''已审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''0'',\n)', 0, '3,4', 99, 1, 1),
(90, 8, 'title', '标题', '', 1, 2, 50, '', '', '', 'text', 'array (\n  ''size'' => ''40'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(91, 8, 'username', '姓名', '', 1, 2, 20, '', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(92, 8, 'telephone', '电话', '', 0, 0, 0, 'tel', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 1, 1, 0),
(93, 8, 'email', '邮箱', '', 1, 0, 40, 'email', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(94, 8, 'content', '内容', '', 1, 2, 200, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''50'',\n  ''default'' => '''',\n)', 1, '', 10, 1, 0),
(95, 8, 'reply_content', '回复', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''50'',\n  ''default'' => '''',\n)', 0, '3,4', 10, 1, 0),
(96, 8, 'ip', 'IP', '', 0, 0, 15, '', '', '', 'text', 'array (\n  ''size'' => ''15'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 0, '3,4', 90, 1, 0),
(97, 9, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 0, '3,4', 93, 1, 1),
(98, 9, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''已审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 0, '3,4', 99, 1, 1),
(99, 9, 'name', '客服名称', '', 0, 2, 20, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 0, '', 0, 1, 0),
(100, 9, 'type', '客服类型', '', 1, 1, 2, '0', '', '', 'select', 'array (\n  ''options'' => ''QQ|1\r\nMSN|2\r\n旺旺|3\r\n贸易通|6\r\n电话|4\r\n代码|5'',\n  ''multiple'' => ''0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''size'' => '''',\n  ''default'' => '''',\n)', 0, '', 0, 1, 0),
(101, 9, 'code', 'ID或代码', '', 0, 2, 0, '0', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''50'',\n  ''default'' => '''',\n)', 0, '', 10, 1, 0),
(102, 9, 'skin', '风格样式', '', 0, 0, 3, '0', '', '', 'select', 'array (\n  ''options'' => ''无风格图标|0\r\nQQ风格1|q1\r\nQQ风格2|q2\r\nQQ风格3|q3\r\nQQ风格4|q4\r\nQQ风格5|q5\r\nQQ风格6|q6\r\nQQ风格7|q7\r\nMSN小图|m1\r\nMSN大图1|m2\r\nMSN大图2|m3\r\nMSN大图3|m4\r\n旺旺小图|w2\r\n旺旺大图|w1\r\n贸易通|al1'',\n  ''multiple'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n  ''numbertype'' => ''1'',\n  ''size'' => '''',\n  ''default'' => '''',\n)', 0, '', 0, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_group_detail`
--

DROP TABLE IF EXISTS `pc_group_detail`;
CREATE TABLE IF NOT EXISTS `pc_group_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `image_count` int(11) NOT NULL,
  `check_time` int(11) NOT NULL,
  `release_time` int(11) NOT NULL,
  `check_oper` varchar(50) CHARACTER SET utf8 NOT NULL,
  `main_filename` varchar(50) CHARACTER SET utf8 NOT NULL,
  `main_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type_one` int(11) NOT NULL,
  `type_two` int(11) NOT NULL,
  `type_three` int(11) NOT NULL,
  `type_one_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type_two_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type_three_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 NOT NULL,
  `group_key` varchar(64) CHARACTER SET utf8 NOT NULL,
  `book_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `group_remark` varchar(512) CHARACTER SET utf8 NOT NULL,
  `state` int(11) NOT NULL,
  `photo_size` int(11) NOT NULL,
  `is_top` tinyint(4) NOT NULL,
  `is_24hot` tinyint(4) NOT NULL,
  `is_editor_tj` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- 转存表中的数据 `pc_group_detail`
--

INSERT INTO `pc_group_detail` (`id`, `up_account`, `up_time`, `image_count`, `check_time`, `release_time`, `check_oper`, `main_filename`, `main_url`, `type_one`, `type_two`, `type_three`, `type_one_name`, `type_two_name`, `type_three_name`, `title`, `group_key`, `book_name`, `group_remark`, `state`, `photo_size`, `is_top`, `is_24hot`, `is_editor_tj`) VALUES
(23, '', 0, 1, 1326237314, 0, 'admin', '', './Uploads/images/s_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', 0, 0, 0, '', '', '', '2324', '', '', '32432', 3, 0, 0, 1, 0),
(24, 'photoer', 1325175090, 1, 1325177097, 0, 'admin', '', './Uploads/images/s_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', 0, 0, 0, '', '', '', '2324', '', '', '32432', 0, 0, 0, 0, 0),
(25, 'photoer', 1325175090, 2, 1325177305, 0, 'admin', '', './Uploads/images/s_7bf50cce831b89ce2b1547d9b83de1f85d2eaa08.jpg', 1101, 4101, 0, '中国时事', '野生动物', '', '2324', '', '', '32432', 1, 0, 1, 0, 1),
(26, 'photoer', 1325410997, 1, 1325411076, 0, 'admin', '', './Uploads/images/s_12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', 1101, 1101, 0, '中国时事', '中国时事', '', '热图测试', '', '', '1111', 1, 0, 0, 1, 0),
(27, 'photoer', 1325322787, 1, 1325323113, 0, 'admin', '', './Uploads/images/s_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', 1000, 4000, 4000, '新闻图片', '素材图片', '素材图片', '测试2', '', '', '总说明', 1, 0, 0, 0, 0),
(28, 'photoer', 1325322787, 2, 1325344113, 0, 'admin', '', './Uploads/images/s_66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', 0, 0, 0, '', '', '', '测试2', '', '', '总说明', 1, 0, 0, 0, 0),
(29, 'selectersky', 1325358602, 2, 1325363158, 0, 'admin', '', './Uploads/thumb/31/20120101/s_ddad4b1dd0c4849be4f2f409f95931dfe2cd0610.JPG', 1101, 4101, 4202, '中国时事', '野生动物', '西式菜系', '运河燃放点的烟花', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 1, 0, 0, 0, 0),
(30, 'selectersky', 1325358602, 2, 1325411969, 0, 'admin', '', './Uploads/thumb/31/20120101/s_45be50102871bb4f201119991ed7767002882bb1.JPG', 0, 0, 0, '', '', '', '运河燃放点的烟花', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 0, 0, 0, 0, 1),
(31, 'xieyd', 1325423889, 3, 1325424472, 0, 'admin', '', './Uploads/thumb//20120101/s_c8716822f23f9095cf767b0435c5e029993dcf00.JPG', 1101, 1102, 1200, '中国时事', '国际时事', '体育', '杭州西博烟花大会', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 1, 0, 0, 0, 1),
(32, 'xieyd', 1325425377, 6, 1325425730, 0, 'admin', '', './Uploads/thumb//20120101/s_2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', 1101, 1102, 0, '中国时事', '国际时事', '', '杭州西博烟花大会', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 1, 0, 1, 0, 1),
(33, 'xieyd', 1325456027, 6, 1325456332, 0, 'admin', '', './Uploads/thumb//20120102/s_389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', 1200, 1100, 2000, '体育', '时事', '创意图片', 'NHL: DEC 30 Sabres at Capitals', '', '', '30 December 2011:   Buffalo Sabres goalie Ryan Miller (30) gives up a goal in action against Washington Capitals center Brooks Laich (21), at the Verizon Center in Washington, D.C. where the Washington Capitals defeated the Buffalo Sabres 3-1. Photo via Newscom', 1, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `pc_guestbook`
--

DROP TABLE IF EXISTS `pc_guestbook`;
CREATE TABLE IF NOT EXISTS `pc_guestbook` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `telephone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(40) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `reply_content` mediumtext NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_hot_picture`
--

DROP TABLE IF EXISTS `pc_hot_picture`;
CREATE TABLE IF NOT EXISTS `pc_hot_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picturename` varchar(64) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `begintime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `place` varchar(32) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_hot_picture`
--

INSERT INTO `pc_hot_picture` (`id`, `picturename`, `url`, `begintime`, `endtime`, `number`, `place`) VALUES
(1, '热图测试', './Uploads/images/s_12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', 1325260858, 1325865658, 1, '热门图片展示');

-- --------------------------------------------------------

--
-- 表的结构 `pc_images_detail`
--

DROP TABLE IF EXISTS `pc_images_detail`;
CREATE TABLE IF NOT EXISTS `pc_images_detail` (
  `state` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `check_time` int(11) NOT NULL,
  `release_time` int(11) NOT NULL,
  `check_oper` varchar(50) CHARACTER SET utf8 NOT NULL,
  `filename` varchar(50) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type_one` varchar(50) CHARACTER SET utf32 NOT NULL,
  `type_two` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type_three` varchar(32) CHARACTER SET utf8 NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 NOT NULL,
  `images_key` varchar(64) CHARACTER SET utf8 NOT NULL,
  `book_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `image_remark` varchar(512) CHARACTER SET utf8 NOT NULL,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `province` varchar(50) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `value` int(24) NOT NULL,
  `down_number` int(11) NOT NULL DEFAULT '0',
  `big_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `small_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `is_top` tinyint(4) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `yiming` varchar(50) CHARACTER SET utf8 NOT NULL,
  `texie` tinyint(4) NOT NULL,
  `ren` tinyint(4) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `filesize` int(11) NOT NULL,
  `photo_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- 转存表中的数据 `pc_images_detail`
--

INSERT INTO `pc_images_detail` (`state`, `id`, `group_id`, `up_account`, `up_time`, `check_time`, `release_time`, `check_oper`, `filename`, `url`, `type_one`, `type_two`, `type_three`, `title`, `images_key`, `book_name`, `image_remark`, `country`, `province`, `city`, `value`, `down_number`, `big_url`, `small_url`, `is_top`, `name`, `yiming`, `texie`, `ren`, `position`, `width`, `height`, `filesize`, `photo_time`) VALUES
(0, 29, 23, 'photoer', 1325175095, 1325176125, 0, 'admin', '', './Uploads/images/b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', '', '', '', '', '', '', '23423', '', '', '', 0, 0, './Uploads/images/m_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', './Uploads/images/s_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 30, 24, 'photoer', 1325175095, 1325177097, 0, 'admin', '', './Uploads/images/b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', '', '', '', '', 'bbb', '', 'aaa', '', '', '', 300, 0, './Uploads/images/m_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', './Uploads/images/s_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', 0, '111', '222', 1, 1, 1, 0, 0, 0, 0),
(200, 31, 25, 'photoer', 1325175095, 1325177305, 0, 'admin', '', './Uploads/images/b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', '', '', '', '', '', '', '23423', '', '', '', 0, 0, './Uploads/images/m_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', './Uploads/images/s_b4ca6f5faa5317b9f9c50c4a497de52102be257d.jpg', 1, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 25, 'photoer', 0, 0, 0, '', '', './Uploads/images/7bf50cce831b89ce2b1547d9b83de1f85d2eaa08.jpg', '', '', '', '', '', '', '分说明', '', '', '', 100, 0, './Uploads/images/m_7bf50cce831b89ce2b1547d9b83de1f85d2eaa08.jpg', './Uploads/images/s_7bf50cce831b89ce2b1547d9b83de1f85d2eaa08.jpg', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 33, 26, 'photoer', 1325411003, 1325411076, 0, 'admin', '', './Uploads/images/12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', '', '', '', '', '', '', '测试说明', '', '', '', 0, 0, './Uploads/images/m_12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', './Uploads/images/s_12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 34, 27, 'photoer', 1325322793, 1325323113, 0, 'admin', '', './Uploads/images/bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', '', '', '', '', '', '', '第三方', '', '', '', 0, 0, './Uploads/images/m_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', './Uploads/images/s_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 35, 28, 'photoer', 1325322793, 1325344113, 0, 'admin', '', './Uploads/images/66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', '', '', '', '', 'qwewqewq', '', '第三方', '', '', '', 200, 0, './Uploads/images/m_66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', './Uploads/images/s_66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(200, 36, 28, 'photoer', 1325322793, 1325345875, 1325345875, 'admin', '', './Uploads/images/bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', '', '', '', '', '', '', '第三方', '', '', '', 100, 0, './Uploads/images/m_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', './Uploads/images/s_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 29, 'selectersky', 1325358611, 1325363158, 0, 'admin', '', './Uploads/images/31/20120101/45be50102871bb4f201119991ed7767002882bb1.JPG', '', '', '', '', 'safdsadADS', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '中国', '吉林省', '长春市', 500, 0, './Uploads/thumb/31/20120101/m_45be50102871bb4f201119991ed7767002882bb1.JPG', './Uploads/thumb/31/20120101/s_45be50102871bb4f201119991ed7767002882bb1.JPG', 1, '111', '222', 1, 1, 1, 0, 0, 0, 0),
(200, 38, 29, 'selectersky', 1325358614, 1325363158, 0, 'admin', '', './Uploads/images/31/20120101/ddad4b1dd0c4849be4f2f409f95931dfe2cd0610.JPG', '', '', '', '', '', '', '', '', '', '', 0, 0, './Uploads/thumb/31/20120101/m_ddad4b1dd0c4849be4f2f409f95931dfe2cd0610.JPG', './Uploads/thumb/31/20120101/s_ddad4b1dd0c4849be4f2f409f95931dfe2cd0610.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 39, 30, 'selectersky', 1325358611, 1325411969, 0, 'admin', '', './Uploads/images/31/20120101/45be50102871bb4f201119991ed7767002882bb1.JPG', '', '', '', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb/31/20120101/m_45be50102871bb4f201119991ed7767002882bb1.JPG', './Uploads/thumb/31/20120101/s_45be50102871bb4f201119991ed7767002882bb1.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 40, 30, 'selectersky', 1325358614, 1325411969, 0, 'admin', '', './Uploads/images/31/20120101/ddad4b1dd0c4849be4f2f409f95931dfe2cd0610.JPG', '', '', '', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb/31/20120101/m_ddad4b1dd0c4849be4f2f409f95931dfe2cd0610.JPG', './Uploads/thumb/31/20120101/s_ddad4b1dd0c4849be4f2f409f95931dfe2cd0610.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 31, 'xieyd', 1325423902, 1325424472, 0, 'admin', '', './Uploads/images//20120101/c8716822f23f9095cf767b0435c5e029993dcf00.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_c8716822f23f9095cf767b0435c5e029993dcf00.JPG', './Uploads/thumb//20120101/s_c8716822f23f9095cf767b0435c5e029993dcf00.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 31, 'xieyd', 1325423905, 1325424472, 0, 'admin', '', './Uploads/images//20120101/cc558b7e2c5ca6f553d47b1ec3d48ebefbc7c626.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_cc558b7e2c5ca6f553d47b1ec3d48ebefbc7c626.JPG', './Uploads/thumb//20120101/s_cc558b7e2c5ca6f553d47b1ec3d48ebefbc7c626.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 43, 31, 'xieyd', 1325423910, 1325424656, 1325424656, 'admin', '', './Uploads/images//20120101/8d42d1d72864f2d927ef668637268e6cb9dbcc4d.JPG', '', '', '', '', '运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_8d42d1d72864f2d927ef668637268e6cb9dbcc4d.JPG', './Uploads/thumb//20120101/s_8d42d1d72864f2d927ef668637268e6cb9dbcc4d.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 32, 'xieyd', 1325425385, 1325425730, 0, 'admin', '', './Uploads/images//20120101/2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', './Uploads/thumb//20120101/s_2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 32, 'xieyd', 1325425395, 1325425730, 0, 'admin', '', './Uploads/images//20120101/08461a1d2fcaec0193edbd62c27d078def8eb8f9.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_08461a1d2fcaec0193edbd62c27d078def8eb8f9.JPG', './Uploads/thumb//20120101/s_08461a1d2fcaec0193edbd62c27d078def8eb8f9.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 32, 'xieyd', 1325425402, 1325425730, 0, 'admin', '', './Uploads/images//20120101/a2df00eb775515850264130412ff5a746c21abb5.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_a2df00eb775515850264130412ff5a746c21abb5.JPG', './Uploads/thumb//20120101/s_a2df00eb775515850264130412ff5a746c21abb5.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 32, 'xieyd', 1325425405, 1325425730, 0, 'admin', '', './Uploads/images//20120101/86e090c6b29e0ea763a5116b49973f32c376f504.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_86e090c6b29e0ea763a5116b49973f32c376f504.JPG', './Uploads/thumb//20120101/s_86e090c6b29e0ea763a5116b49973f32c376f504.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 32, 'xieyd', 1325425408, 1325425730, 0, 'admin', '', './Uploads/images//20120101/91549e3ba306903fed143cc3e391cf47931b1fc7.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_91549e3ba306903fed143cc3e391cf47931b1fc7.JPG', './Uploads/thumb//20120101/s_91549e3ba306903fed143cc3e391cf47931b1fc7.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 32, 'xieyd', 1325425412, 1325425730, 0, 'admin', '', './Uploads/images//20120101/f4f01520f1fc2d03c874606e72eaf82c2a95315a.JPG', '', '', '', '', '烟花，运河，节日，庆典', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', '', '', 100, 0, './Uploads/thumb//20120101/m_f4f01520f1fc2d03c874606e72eaf82c2a95315a.JPG', './Uploads/thumb//20120101/s_f4f01520f1fc2d03c874606e72eaf82c2a95315a.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 50, 33, 'xieyd', 1325456045, 1325456332, 0, 'admin', '', './Uploads/images//20120102/389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', '', '', '', '', '冰球，运动，比赛', '', 'NHL: DEC 30 Sabres at Capitals', '', '', '', 100, 0, './Uploads/thumb//20120102/m_389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', './Uploads/thumb//20120102/s_389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 51, 33, 'xieyd', 1325456047, 1325456332, 0, 'admin', '', './Uploads/images//20120102/9dc5824f73533fcf51aae48fb16da37bbaef7e15.JPG', '', '', '', '', '冰球，运动，比赛', '', 'NHL: DEC 30 Sabres at Capitals', '', '', '', 100, 0, './Uploads/thumb//20120102/m_9dc5824f73533fcf51aae48fb16da37bbaef7e15.JPG', './Uploads/thumb//20120102/s_9dc5824f73533fcf51aae48fb16da37bbaef7e15.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 52, 33, 'xieyd', 1325456050, 1325456332, 0, 'admin', '', './Uploads/images//20120102/e96980178617fa1fdd35d3897dd210089d9bbb60.JPG', '', '', '', '', '冰球，运动，比赛', '', 'NHL: DEC 30 Sabres at Capitals', '', '', '', 100, 0, './Uploads/thumb//20120102/m_e96980178617fa1fdd35d3897dd210089d9bbb60.JPG', './Uploads/thumb//20120102/s_e96980178617fa1fdd35d3897dd210089d9bbb60.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 53, 33, 'xieyd', 1325456052, 1325456332, 0, 'admin', '', './Uploads/images//20120102/326395963e073c6ff8111d71ab0e3bd509a3300f.JPG', '', '', '', '', '冰球，运动，比赛', '', 'NHL: DEC 30 Sabres at Capitals', '', '', '', 100, 0, './Uploads/thumb//20120102/m_326395963e073c6ff8111d71ab0e3bd509a3300f.JPG', './Uploads/thumb//20120102/s_326395963e073c6ff8111d71ab0e3bd509a3300f.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 54, 33, 'xieyd', 1325456055, 1325456332, 0, 'admin', '', './Uploads/images//20120102/f213a7ff54477a6c52f37ccdd1ed30b03d719546.JPG', '', '', '', '', '冰球，运动，比赛', '', 'NHL: DEC 30 Sabres at Capitals', '', '', '', 100, 0, './Uploads/thumb//20120102/m_f213a7ff54477a6c52f37ccdd1ed30b03d719546.JPG', './Uploads/thumb//20120102/s_f213a7ff54477a6c52f37ccdd1ed30b03d719546.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0),
(0, 55, 33, 'xieyd', 1325456057, 1325456332, 0, 'admin', '', './Uploads/images//20120102/595cdf966399b1cf84acc011a72a47727f12759c.JPG', '', '', '', '', '冰球，运动，比赛', '', 'NHL: DEC 30 Sabres at Capitals', '', '', '', 100, 0, './Uploads/thumb//20120102/m_595cdf966399b1cf84acc011a72a47727f12759c.JPG', './Uploads/thumb//20120102/s_595cdf966399b1cf84acc011a72a47727f12759c.JPG', 0, '', '', 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_kefu`
--

DROP TABLE IF EXISTS `pc_kefu`;
CREATE TABLE IF NOT EXISTS `pc_kefu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL DEFAULT '',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `code` mediumtext NOT NULL,
  `skin` varchar(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `pc_kefu`
--

INSERT INTO `pc_kefu` (`id`, `status`, `listorder`, `createtime`, `name`, `type`, `code`, `skin`) VALUES
(1, 1, 4, 1306807701, '咨询电话', 4, '0317-5022625', '0'),
(2, 1, 3, 1306808546, '技术咨询', 1, '147613338', 'q3'),
(3, 1, 3, 1306808886, 'QQ客服', 1, '147613338', 'q3'),
(4, 1, 2, 1306811439, 'MSN客服', 2, 'snliuxun@msn.cn', 'm2'),
(5, 1, 0, 1306830001, '旺旺客服', 3, 'snliuxun', 'w1');

-- --------------------------------------------------------

--
-- 表的结构 `pc_keyword`
--

DROP TABLE IF EXISTS `pc_keyword`;
CREATE TABLE IF NOT EXISTS `pc_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `type_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `pc_keyword`
--

INSERT INTO `pc_keyword` (`id`, `type_id`, `name`, `type_name`) VALUES
(1, 1, '轻轻巧巧', '11111'),
(2, 1, '23染色法', '11111'),
(3, 1, '', '11111'),
(4, 1, '', '11111'),
(5, 1, '阿凡达是否', '11111'),
(6, 2, '天天天天天', '222');

-- --------------------------------------------------------

--
-- 表的结构 `pc_keyword_type`
--

DROP TABLE IF EXISTS `pc_keyword_type`;
CREATE TABLE IF NOT EXISTS `pc_keyword_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `pc_keyword_type`
--

INSERT INTO `pc_keyword_type` (`id`, `name`, `parent_id`, `parent_name`) VALUES
(1, '11111', 0, ''),
(2, '222', 1, '11111');

-- --------------------------------------------------------

--
-- 表的结构 `pc_link`
--

DROP TABLE IF EXISTS `pc_link`;
CREATE TABLE IF NOT EXISTS `pc_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `logo` varchar(80) NOT NULL DEFAULT '',
  `siteurl` varchar(150) NOT NULL DEFAULT '',
  `typeid` smallint(5) unsigned NOT NULL,
  `linktype` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `siteinfo` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `pc_link`
--

INSERT INTO `pc_link` (`id`, `status`, `listorder`, `createtime`, `name`, `logo`, `siteurl`, `typeid`, `linktype`, `siteinfo`) VALUES
(1, 1, 0, 1306547518, 'Yourphp企业网站管理系统', 'http://demo.yourphp.cn/Public/Images/logo.gif', 'http://www.yourphp.cn', 2, 2, 'php企业网站管理系统'),
(2, 1, 0, 1306554684, '企业网站管理系统', '', 'http://www.yourphp.cn', 2, 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_mainpicture`
--

DROP TABLE IF EXISTS `pc_mainpicture`;
CREATE TABLE IF NOT EXISTS `pc_mainpicture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(128) CHARACTER SET utf8 NOT NULL,
  `begintime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `imgurl` varchar(128) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_menu`
--

DROP TABLE IF EXISTS `pc_menu`;
CREATE TABLE IF NOT EXISTS `pc_menu` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` char(20) NOT NULL DEFAULT '',
  `action` char(20) NOT NULL DEFAULT '',
  `data` char(50) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `parentid` (`parentid`),
  KEY `model` (`model`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=105 ;

--
-- 转存表中的数据 `pc_menu`
--

INSERT INTO `pc_menu` (`id`, `parentid`, `model`, `action`, `data`, `type`, `status`, `group_id`, `name`, `remark`, `listorder`) VALUES
(1, 0, 'Main', 'main', 'menuid=42', 1, 1, 0, '后台首页', '', 0),
(2, 0, 'User', '', '', 1, 1, 0, '系统管理', '系统设置', 6),
(73, 2, 'User', '', '', 1, 1, 0, '用户管理', '用户管理', 0),
(5, 0, 'Photoer', '', 'menuid=9', 1, 1, 0, '会员管理', '', 1),
(71, 2, 'Role', 'index', '', 1, 1, 0, '角色管理', '操作员角色管理', 1),
(39, 2, 'Menu', '', '', 1, 1, 0, '后台管理菜单', '后台管理菜单', 2),
(15, 39, 'Menu', 'add', '', 1, 1, 0, '添加菜单', '', 0),
(86, 85, 'PhotoerImageGroup', 'query', '', 1, 1, 0, '摄影师图组', '', 0),
(76, 5, 'Downloader', '', '', 1, 1, 0, '下载者管理', '', 1),
(74, 73, 'User', 'add', '', 1, 1, 0, '添加用户', '', 0),
(9, 5, 'Photoer', 'index', '', 1, 1, 0, '摄影师资料管理', '会员资料管理', 0),
(79, 5, 'AddvalueQuery', '', '', 1, 1, 0, '充值记录查询', '充值记录查询', 2),
(11, 5, 'Role', '', '', 1, 1, 0, '会员组管理', '', 4),
(12, 11, 'Role', 'add', '', 1, 1, 0, '添加会员组', '', 0),
(13, 5, 'Node', '', '', 1, 1, 0, '权限节点管理', '', 6),
(14, 13, 'Node', 'add', '', 1, 1, 0, '添加权限节点', '', 0),
(85, 0, 'PhotoerImageGroup', 'query', '', 1, 1, 0, '相片管理中心', '', 2),
(17, 3, 'Category', '', '', 1, 1, 0, '栏目管理', '栏目管理', 1),
(83, 81, 'Picture', 'add', '', 1, 1, 0, '图片上传', '', 0),
(82, 0, 'User', '', '', 1, 1, 0, '报表统计', '', 5),
(80, 0, 'User', '', '', 1, 1, 0, '新闻管理', '新闻管理', 3),
(84, 80, 'request', '', '', 1, 1, 0, '需求留言管理', '', 0),
(75, 71, 'Role', 'add', '', 1, 1, 0, '添加角色', '角色添加', 0),
(31, 17, 'Category', 'repair_cache', '', 1, 1, 0, '恢复栏目数据', '', 0),
(78, 5, 'tuijian', '', '', 1, 1, 0, '推荐管理', '', 3),
(34, 6, 'Createhtml', 'Createlist', '', 1, 1, 0, '更新列表页', '', 0),
(35, 6, 'Createhtml', 'Createshow', '', 1, 1, 0, '更新内容页', '', 0),
(37, 26, 'Picture', 'add', '', 1, 1, 0, '添加图片', '', 0),
(38, 27, 'Download', 'add', '', 1, 1, 0, '添加文件', '', 0),
(40, 1, 'Main', 'password', '', 1, 1, 0, '修改密码', '', 2),
(41, 1, 'Main', 'profile', '', 1, 1, 0, '个人资料', '', 3),
(42, 1, 'Main', 'main', '', 1, 1, 0, '后台首页', '', 1),
(46, 44, 'Database', 'recover', '', 1, 1, 0, '恢复数据库', '', 0),
(52, 28, 'Type', 'add', '', 1, 1, 0, '添加类别', '', 0),
(53, 4, 'Link', 'index', '', 1, 1, 0, '友情链接', '', 0),
(54, 53, 'Link', 'add', '', 1, 1, 0, '添加链接', '', 0),
(56, 4, 'Order', 'index', '', 1, 1, 0, '订单管理', '', 0),
(99, 82, 'Baobiao', 'photojs', '', 1, 1, 0, '摄影师结算管理', '', 0),
(88, 85, 'waitrealsephotoer', '', '', 1, 1, 0, '待发布图组管理', '', 1),
(70, 69, 'Dbsource', 'add', '', 1, 1, 0, '添加数据源', '', 0),
(89, 85, 'realsephotoer', '', '', 1, 1, 0, '发布图组管理', '', 2),
(90, 85, 'hotphotogroup', '', '', 1, 1, 0, '热门图组管理', '', 4),
(91, 85, 'Phototype', '', '', 1, 1, 0, '图片类型管理', '', 7),
(92, 85, 'Topicimagegroup', '', '', 1, 1, 0, '专题图组管理', '', 3),
(93, 2, 'Role', 'auth', '', 1, 1, 0, '角色权限管理', '', 0),
(94, 5, 'Downloader', 'add', '', 1, 1, 0, '会员充值管理', '', 5),
(95, 85, 'Keyword', '', '', 1, 1, 0, '关键字管理', '', 6),
(96, 95, 'Keyword', 'type', '', 1, 1, 0, '关键字类型管理', '', 0),
(97, 85, 'Mainpicture', '', '', 1, 1, 0, '广告图片管理', '', 5),
(100, 82, 'Baobiao', 'phototj', '', 1, 1, 0, '摄影师上传图片统计', '', 0),
(101, 82, 'Baobiao', 'supplement', '', 1, 1, 0, '下载用户充值查询', '', 0),
(102, 82, 'Baobiao', 'downer_info', '', 1, 1, 0, '下载用户账户管理', '', 0),
(103, 82, 'Baobiao', '', '', 1, 1, 0, '富图图片销售统计', '', 0),
(104, 86, 'PhotoerImageGroup', 'editgroup', '', 1, 1, 0, '编辑图组', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_module`
--

DROP TABLE IF EXISTS `pc_module`;
CREATE TABLE IF NOT EXISTS `pc_module` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listfields` varchar(255) NOT NULL DEFAULT '',
  `setup` mediumtext NOT NULL,
  `listorder` smallint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `postgroup` varchar(100) NOT NULL DEFAULT '',
  `ispost` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `pc_module`
--

INSERT INTO `pc_module` (`id`, `title`, `name`, `description`, `type`, `issystem`, `issearch`, `listfields`, `setup`, `listorder`, `status`, `postgroup`, `ispost`) VALUES
(1, '单页模型', 'Page', '单页模型', 1, 1, 0, '*', '', 0, 1, '', 0),
(2, '文章模型', 'Article', '新闻文章', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status', '', 0, 1, '', 0),
(3, '产品模型', 'Product', '产品展示', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status,xinghao,price', '', 0, 1, '', 0),
(4, '图片模型', 'Picture2', '图片展示', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status', '', 0, 1, '', 0),
(5, '下载模型', 'Download', '文件下载', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status,ext,size', '', 0, 1, '', 0),
(6, '信息反馈', 'Feedback', '信息反馈', 1, 0, 0, '*', '', 0, 1, '1,2,3,4', 0),
(7, '友情链接', 'Link', '友情链接', 2, 0, 0, '*', '', 0, 1, '', 0),
(8, '在线留言', 'Guestbook', '在线留言', 1, 0, 0, '*', '', 0, 1, '1,2,3,4', 0),
(9, '在线客服', 'Kefu', '在线客服', 2, 0, 0, '*', '', 0, 1, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_node`
--

DROP TABLE IF EXISTS `pc_node`;
CREATE TABLE IF NOT EXISTS `pc_node` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`status`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- 转存表中的数据 `pc_node`
--

INSERT INTO `pc_node` (`id`, `name`, `title`, `status`, `remark`, `listorder`, `pid`, `level`) VALUES
(1, 'Admin', '后台管理', 1, '后台项目', 0, 0, 1),
(2, 'Index', '后台默认', 1, '控制器', 0, 1, 2),
(3, 'Config', '系统设置', 1, '控制器', 0, 1, 2),
(4, 'index', '站点配置', 1, '动作', 0, 3, 3),
(5, 'sys', '系统参数', 1, '动作', 0, 3, 3),
(6, 'seo', 'SEO设置', 1, '动作', 0, 3, 3),
(7, 'add', '添加变量', 1, '动作', 0, 3, 3),
(8, 'Menu', '菜单管理', 1, '菜单控制器', 0, 1, 2),
(9, 'index', '菜单列表', 1, '菜单列表-动作', 0, 8, 3),
(10, 'add', '添加菜单', 1, '添加菜单-动作', 0, 8, 3),
(11, 'index', '后台默认动作', 1, '后台默认动作', 0, 2, 3),
(12, 'Main', '后台首页', 1, '控制器', 0, 1, 2),
(13, 'profile', '个人资料', 1, '动作', 0, 12, 3),
(14, 'password', '修改密码', 1, '动作', 0, 12, 3),
(15, 'index', '后台首页', 1, '动作', 0, 12, 3),
(16, 'main', '欢迎首页', 1, '动作', 0, 12, 3),
(17, 'attach', '附件设置', 1, '动作', 0, 3, 3),
(18, 'mail', '系统邮箱', 1, '动作', 0, 3, 3),
(19, 'Posid', '推荐位', 1, '控制器', 0, 1, 2),
(20, 'index', '列表', 1, '动作', 0, 19, 3),
(21, 'Category', '模型管理', 1, '', 0, 1, 2),
(22, 'User', '会员管理', 1, '', 0, 1, 2),
(23, 'Createhtml', '网站更新', 1, '', 0, 1, 2),
(24, 'index', '栏目管理', 1, '', 0, 21, 3),
(25, 'index', '会员资料管理', 1, '', 0, 22, 3),
(26, 'index', '更新首页', 1, '', 0, 23, 3),
(27, 'Createlist', '更新列表页', 1, '', 0, 23, 3),
(28, 'Createshow', '更新内容页', 1, '', 0, 23, 3),
(29, 'Updateurl', '更新URL', 1, '', 0, 23, 3);

-- --------------------------------------------------------

--
-- 表的结构 `pc_order`
--

DROP TABLE IF EXISTS `pc_order`;
CREATE TABLE IF NOT EXISTS `pc_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sn` char(22) NOT NULL DEFAULT '',
  `password` varchar(40) NOT NULL DEFAULT '',
  `module` varchar(20) NOT NULL DEFAULT '',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `price` decimal(10,0) unsigned NOT NULL DEFAULT '0',
  `productlist` mediumtext NOT NULL,
  `note` mediumtext NOT NULL,
  `realname` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `tel` varchar(50) NOT NULL DEFAULT '',
  `mobile` varchar(18) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(80) NOT NULL DEFAULT '',
  `zipcode` varchar(10) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL DEFAULT '',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sn` (`sn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_page`
--

DROP TABLE IF EXISTS `pc_page`;
CREATE TABLE IF NOT EXISTS `pc_page` (
  `id` smallint(5) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `keywords` varchar(250) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `template` varchar(30) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_page`
--

INSERT INTO `pc_page` (`id`, `title`, `thumb`, `keywords`, `description`, `content`, `template`, `listorder`) VALUES
(8, '关于我们', './Uploads/201104/4d9764a7b953d.jpg', 'yourphp,企业建站系统,发布', 'yourphp,企业建站系统,发布', '<p>\r\n	关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们lllll</p>\r\n<p>\r\n	[page]</p>\r\n<p>\r\n	sdfgasdfaf中华人民共和国关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我</p>\r\n<p>\r\n	111</p>\r\n', '', 0),
(11, '公司简介', '', '', '公司简介', '<p>\r\n	公司简介</p>\r\n', '', 0),
(12, '联系我们', './Uploads/201104/4d96e3f1522b3.jpg', '联系我们', '联系我们', '<p>\r\n	联系我们</p>\r\n', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_photoer`
--

DROP TABLE IF EXISTS `pc_photoer`;
CREATE TABLE IF NOT EXISTS `pc_photoer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sex` varchar(8) CHARACTER SET utf8 NOT NULL,
  `card_no` varchar(32) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `mobile_phone` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(32) CHARACTER SET utf8 NOT NULL,
  `province` varchar(32) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `post_code` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `link_addr` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `company_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `book_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `pen_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `work_years` tinyint(4) DEFAULT NULL,
  `main_photo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `main_post_local` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `all_equipment` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `cameras` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `settlement_model` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `which_bank` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `which_open_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `bank_card_no` varchar(50) CHARACTER SET utf8 NOT NULL,
  `join_team` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `user_say` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `head_image_path` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `createtime` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `pvalue` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- 转存表中的数据 `pc_photoer`
--

INSERT INTO `pc_photoer` (`id`, `account`, `password`, `name`, `sex`, `card_no`, `email`, `mobile_phone`, `phone`, `province`, `city`, `post_code`, `link_addr`, `company_name`, `book_name`, `pen_name`, `work_years`, `main_photo`, `main_post_local`, `all_equipment`, `cameras`, `settlement_model`, `which_bank`, `which_open_name`, `bank_card_no`, `join_team`, `user_say`, `head_image_path`, `createtime`, `state`, `pvalue`) VALUES
(30, 'photoer', '2830f342c6dff02782d3cbc43c917c1b96bf4cb3', 'photoer22', '男', '888888888888888888', '123@126.com', NULL, '13757190063', '重庆市', '市辖区', '333333', '', '', '', '中文', 1, '新闻摄影', '书籍', 'Olympus', '', '招商银行', '航站', '爬虫', '888888888888888', '121212', '121212', 'photoer.jpg', 1324883103, 0, 0),
(31, 'selectersky', 'c137340e996daae5fec608dcffd7683442cf8024', '陈甲新11', '男', '432524122190121', 'sss@qq.com', NULL, '13755632345', '浙江省', '杭州市', '322000', '2424', '2424', '24', '陈甲新', 1, '新闻摄影', '书籍', 'Horseman', '', '招商银行', '浦沿', '陈甲新', '52232323', '  您所参加的摄影团体\r\n曾举办过的                       ', '您的宝贵意见', './Uploads/headimage//image003.jpg', 1325400662, 0, 0),
(32, 'xieyd', 'c137340e996daae5fec608dcffd7683442cf8024', '谢有定', '男', '333333333333333333', 'jingya8825@sina.com', NULL, '13588038843', '浙江省', '杭州市', '310004', '', '', '', 'xieyouding', 1, NULL, NULL, NULL, '', '招商银行', '', '文晖路', '333333333333333', '                         ', '', './Uploads/headimage//富图中国LOGO-B2.jpg', 1325326438, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_picture`
--

DROP TABLE IF EXISTS `pc_picture`;
CREATE TABLE IF NOT EXISTS `pc_picture` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `url` varchar(60) NOT NULL DEFAULT '',
  `template` varchar(40) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `readpoint` smallint(5) NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `pics` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_posid`
--

DROP TABLE IF EXISTS `pc_posid`;
CREATE TABLE IF NOT EXISTS `pc_posid` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  `listorder` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_posid`
--

INSERT INTO `pc_posid` (`id`, `name`, `listorder`) VALUES
(1, '首页推荐', 0),
(2, '首页幻灯片', 0),
(3, '推荐产品', 0),
(4, '促销产品', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_product`
--

DROP TABLE IF EXISTS `pc_product`;
CREATE TABLE IF NOT EXISTS `pc_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `keywords` varchar(80) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `template` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `readpoint` smallint(5) NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `url` varchar(60) NOT NULL DEFAULT '',
  `xinghao` varchar(30) NOT NULL DEFAULT '',
  `pics` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_role`
--

DROP TABLE IF EXISTS `pc_role`;
CREATE TABLE IF NOT EXISTS `pc_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0',
  `allowpost` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowpostverify` tinyint(1) unsigned NOT NULL,
  `allowsearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowupgrade` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `allowsendmessage` tinyint(1) unsigned NOT NULL,
  `allowattachment` tinyint(1) NOT NULL,
  `maxpostnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `maxmessagenum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `price_y` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `price_m` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `price_d` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `pc_role`
--

INSERT INTO `pc_role` (`id`, `name`, `status`, `remark`, `pid`, `listorder`, `allowpost`, `allowpostverify`, `allowsearch`, `allowupgrade`, `allowsendmessage`, `allowattachment`, `maxpostnum`, `maxmessagenum`, `price_y`, `price_m`, `price_d`) VALUES
(1, '超级管理员', 1, '超级管理员', 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0.00, 0.00, 0.00),
(2, '普通管理员', 1, '普通管理员', 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0.00, 0.00, 0.00),
(5, '图片总监', 1, '图片总监', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0.00, 0.00, 0.00),
(6, '摄影师管理', 1, '摄影师管理', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0.00, 0.00, 0.00),
(7, '图片编辑', 1, '图片编辑', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0.00, 0.00, 0.00),
(8, '网站编辑', 1, '网站编辑', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0.00, 0.00, 0.00),
(9, '摄影师结算', 1, '摄影师结算', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0.00, 0.00, 0.00),
(10, '外联部', 1, '外联部', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0.00, 0.00, 0.00),
(11, '营销部', 1, '营销部', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- 表的结构 `pc_role_auth`
--

DROP TABLE IF EXISTS `pc_role_auth`;
CREATE TABLE IF NOT EXISTS `pc_role_auth` (
  `id` int(11) NOT NULL,
  `roleid` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_role_auth`
--

INSERT INTO `pc_role_auth` (`id`, `roleid`, `menu_id`) VALUES
(0, 1, 1),
(0, 1, 40),
(0, 1, 41),
(0, 1, 42),
(0, 1, 2),
(0, 1, 73),
(0, 1, 74),
(0, 1, 71),
(0, 1, 75),
(0, 1, 39),
(0, 1, 15),
(0, 1, 93),
(0, 1, 5),
(0, 1, 76),
(0, 1, 9),
(0, 1, 79),
(0, 1, 11),
(0, 1, 12),
(0, 1, 13),
(0, 1, 14),
(0, 1, 78),
(0, 1, 94),
(0, 1, 85),
(0, 1, 86),
(0, 1, 104),
(0, 1, 88),
(0, 1, 89),
(0, 1, 90),
(0, 1, 91),
(0, 1, 92),
(0, 1, 95),
(0, 1, 96),
(0, 1, 97),
(0, 1, 82),
(0, 1, 99),
(0, 1, 100),
(0, 1, 101),
(0, 1, 102),
(0, 1, 103),
(0, 1, 81),
(0, 1, 83),
(0, 1, 80),
(0, 1, 84);

-- --------------------------------------------------------

--
-- 表的结构 `pc_role_user`
--

DROP TABLE IF EXISTS `pc_role_user`;
CREATE TABLE IF NOT EXISTS `pc_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT '0',
  `user_id` char(32) DEFAULT '0',
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_role_user`
--

INSERT INTO `pc_role_user` (`role_id`, `user_id`) VALUES
(0, '28'),
(NULL, '2'),
(NULL, '3'),
(NULL, '33'),
(NULL, '34'),
(NULL, '35'),
(NULL, '36');

-- --------------------------------------------------------

--
-- 表的结构 `pc_shopping`
--

DROP TABLE IF EXISTS `pc_shopping`;
CREATE TABLE IF NOT EXISTS `pc_shopping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(50) CHARACTER SET utf8 NOT NULL,
  `photoid` int(20) NOT NULL,
  `phototime` int(25) NOT NULL,
  `phototitle` char(50) CHARACTER SET utf8 NOT NULL,
  `photoplace` char(50) CHARACTER SET utf8 NOT NULL,
  `photovalue` int(20) NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `up_account` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_shopping`
--

INSERT INTO `pc_shopping` (`id`, `username`, `photoid`, `phototime`, `phototitle`, `photoplace`, `photovalue`, `url`, `up_account`) VALUES
(1, 'photoer', 33, 1325265085, '', '', 0, './Uploads/images/12c37b21189ef9f82ec7775ea8c3d784a687809b.jpg', 0),
(2, 'selectersky', 37, 1325453637, '', '长春市', 500, './Uploads/images/31/20120101/45be50102871bb4f201119991ed7767002882bb1.JPG', 0),
(3, 'selectersky', 37, 1325453642, '', '长春市', 500, './Uploads/images/31/20120101/45be50102871bb4f201119991ed7767002882bb1.JPG', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_topic_detail`
--

DROP TABLE IF EXISTS `pc_topic_detail`;
CREATE TABLE IF NOT EXISTS `pc_topic_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_topic_detail`
--

INSERT INTO `pc_topic_detail` (`id`, `topic_id`, `group_id`) VALUES
(2, 1, 25),
(3, 1, 32),
(4, 1, 32);

-- --------------------------------------------------------

--
-- 表的结构 `pc_topic_group`
--

DROP TABLE IF EXISTS `pc_topic_group`;
CREATE TABLE IF NOT EXISTS `pc_topic_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `remark` varchar(1024) NOT NULL,
  `key` varchar(512) NOT NULL,
  `group_count` int(11) NOT NULL,
  `type_one_id` int(11) NOT NULL,
  `type_two_id` int(11) NOT NULL,
  `type_three_id` int(11) NOT NULL,
  `type_one_name` varchar(50) NOT NULL,
  `type_two_name` varchar(50) NOT NULL,
  `type_three_name` varchar(50) NOT NULL,
  `createtime` int(11) NOT NULL,
  `lastedittime` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_topic_group`
--

INSERT INTO `pc_topic_group` (`id`, `title`, `remark`, `key`, `group_count`, `type_one_id`, `type_two_id`, `type_three_id`, `type_one_name`, `type_two_name`, `type_three_name`, `createtime`, `lastedittime`, `state`) VALUES
(1, '专题图组11111', '说明2222222222', '', 0, 4101, 4101, 4104, '野生动物', '野生动物', '景观植物', 1325423315, 1325423315, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_trade_detail`
--

DROP TABLE IF EXISTS `pc_trade_detail`;
CREATE TABLE IF NOT EXISTS `pc_trade_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer` varchar(50) CHARACTER SET utf8 NOT NULL,
  `seller` varchar(50) CHARACTER SET utf8 NOT NULL,
  `image_id` int(11) NOT NULL,
  `trade_time` int(11) NOT NULL,
  `image_up_time` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `jifen` int(11) NOT NULL,
  `client_ip` varchar(32) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_trade_detail`
--

INSERT INTO `pc_trade_detail` (`id`, `buyer`, `seller`, `image_id`, `trade_time`, `image_up_time`, `points`, `jifen`, `client_ip`, `url`) VALUES
(1, 'test', 'wangxb', 3, 1322756890, 1322756890, 100, 0, '127.0.0.1', 'Uploads\\images\\60ab016a82a3b4bd308d0055435e91d2b5db2975.jpg'),
(2, 'wangxb', 'wangxb', 4, 1322756890, 1322756890, 100, 0, '127.0.0.1', 'Uploads\\images\\60ab016a82a3b4bd308d0055435e91d2b5db2975.jpg'),
(3, 'test', 'test', 4, 1322756890, 1322756890, 100, 0, '127.0.0.1', 'Uploads\\images\\60ab016a82a3b4bd308d0055435e91d2b5db2975.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `pc_tuijian`
--

DROP TABLE IF EXISTS `pc_tuijian`;
CREATE TABLE IF NOT EXISTS `pc_tuijian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `position` varchar(50) CHARACTER SET utf8 NOT NULL,
  `modi_info` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_tuijian`
--

INSERT INTO `pc_tuijian` (`id`, `account`, `startdate`, `enddate`, `position`, `modi_info`) VALUES
(1, 'wangxb', '2011-01-02', '2012-01-01', '优秀摄影师推荐', '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_type`
--

DROP TABLE IF EXISTS `pc_type`;
CREATE TABLE IF NOT EXISTS `pc_type` (
  `typeid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`typeid`),
  KEY `parentid` (`parentid`,`listorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `pc_type`
--

INSERT INTO `pc_type` (`typeid`, `name`, `parentid`, `description`, `status`, `listorder`, `keyid`) VALUES
(1, '友情链接', 0, '友情链接分类', 1, 0, 1),
(3, '合作伙伴', 1, '合作伙伴', 1, 1, 1),
(2, '默认分类', 1, '默认分类', 1, 0, 1),
(4, '反馈类别', 0, '信息反馈类别', 1, 0, 4),
(5, '产品购买', 4, '产品购买', 1, 0, 5),
(6, '商务合作', 4, '商务合作', 1, 0, 6),
(7, '其他反馈', 4, '其他反馈', 1, 0, 7);

-- --------------------------------------------------------

--
-- 表的结构 `pc_upload_group_detail`
--

DROP TABLE IF EXISTS `pc_upload_group_detail`;
CREATE TABLE IF NOT EXISTS `pc_upload_group_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `photo_date` int(11) NOT NULL,
  `check_time` int(11) NOT NULL,
  `check_oper` varchar(50) CHARACTER SET utf8 NOT NULL,
  `main_filename` varchar(128) CHARACTER SET utf8 NOT NULL,
  `main_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `image_num` int(11) NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(512) CHARACTER SET utf8 NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 NOT NULL,
  `check_state` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=182 ;

--
-- 转存表中的数据 `pc_upload_group_detail`
--

INSERT INTO `pc_upload_group_detail` (`id`, `up_account`, `up_time`, `photo_date`, `check_time`, `check_oper`, `main_filename`, `main_url`, `image_num`, `type`, `title`, `remark`, `city`, `check_state`) VALUES
(50, 'photoer', 1325322787, 2012, 0, '', '', './Uploads/images/s_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', 2, '时效图片', '测试2', '总说明', '', 0),
(51, 'xieyd', 1325326682, 2012, 0, '', '', './Uploads/images/s_04bb30cd23dc02bc3503effb2e84d0849dde751c.jpg', 3, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(52, 'photoer', 1325341635, 2012, 0, '', '', '', 0, '时效图片', '2223', '3232323', '', 10),
(53, 'photoer', 1325342056, 2012, 0, '', '', '', 0, '时效图片', '111', '3333', '', 100),
(62, 'photoer', 1325347162, 2012, 0, '', '', '', 0, '时效图片', '232', '1232131', '', 100),
(63, 'photoer', 1325347236, 2012, 0, '', '', '', 0, '时效图片', 'nihao', 'ds ', '', 100),
(120, 'selectersky', 1325485562, 0, 0, '', '', '', 2, '资料图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(121, 'selectersky', 1325485871, 1325433600, 0, '', '', '', 3, '时效图片', '组图标题', '图组说明', '', 10),
(122, 'selectersky', 1325423204, 0, 0, '', '', '', 1, '资料图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(123, 'selectersky', 1325423646, 0, 0, '', '', '', 2, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(124, 'selectersky', 1325423714, 0, 0, '', '', '', 3, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(125, 'xieyd', 1325423889, 0, 0, '', '', '', 4, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(128, 'xieyd', 1325425377, 0, 0, '', '', '', 10, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(129, 'selectersky', 1325425998, 0, 0, '', '', './Uploads/thumb//20120101/s_1b9c9649d141c1191779ec6ec65f1ab529299fdc.jpg', 1, '时效图片', '', '', '', 10),
(130, 'selectersky', 1325490310, 0, 0, '', '', './Uploads/thumb//20120102/s_e100e808fbedddcf4e4c5bd04ebc537bb876155d.jpg', 1, '时效图片', '', '', '', 10),
(131, 'selectersky', 1325427766, 0, 0, '', '', './Uploads/thumb//20120101/s_62216675f42c4223ea63d47faf539e6d982829a8.JPG', 1, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(132, 'selectersky', 1325427800, 0, 0, '', '', './Uploads/thumb//20120101/s_8b95de75d8d216722807f3d9ec5cc3b78647443e.JPG', 3, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(133, 'xieyd', 1325427870, 0, 0, '', '', './Uploads/thumb//20120101/s_fec11986891b8e4e5fbe5dc1a6cb993a700939fa.JPG', 3, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(134, 'selectersky', 1325491886, 0, 0, '', '', './Uploads/thumb//20120102/s_920713cb469415517addb523ea618b2394a6be0f.jpg', 3, '时效图片', '', '', '', 10),
(135, 'xieyd', 1325428356, 0, 0, '', '', './Uploads/thumb//20120101/s_d24343eea7f7c560b12de71c18928c1ded5416ca.JPG', 4, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(136, 'xieyd', 1325443396, 0, 0, '', '', './Uploads/thumb//20120102/s_e86d13c1a957f57ca13e541fd2466995af9a83a2.JPG', 15, '时效图片', 'SPT_CHELSEA_ASTON_VILLA_007', 'Chelsea''s Didier Drogba jokes with Aston Villa''s Stephen Warnock pretending to ride him a like a horse after they tangle. Chelsea are level with Aston Villa 1:1\rChelsea 31/12/11\rChelsea V Aston Villa 31/12/11\rThe Premier League\rPhoto: Richard Washbrooke Fotosports International\r Photo via Newscom', 'London', 0),
(137, 'selectersky', 1325443909, 0, 0, '', '', './Uploads/thumb//20120102/s_7179a6d2c1577f0298f2be24760750af89953747.JPG', 3, '资料图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(138, 'xieyd', 1325445202, 0, 0, '', '', './Uploads/thumb//20120102/s_3f7650dd97a99f10c3af526c3eaf267144555ad9.JPG', 15, '时效图片', '', 'Dec 29 2011: \rduring the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r Photo via Newscom', 'San Antonio', 0),
(139, 'xieyd', 1325445910, 0, 0, '', '', './Uploads/thumb//20120102/s_0cb40958f76b672df47d6cd12dd5f750c806548f.JPG', 5, '时效图片', '', '30 December 2011: Western Michigan defenseman Danny DeKeyser (5) against the St. Cloud State Huskies at the National Hockey Center in St. Cloud, MN. Western Michigan defeated St. Cloud State 2-1 in overtime. Photo via Newscom', 'St. Cloud', 0),
(141, 'selectersky', 1325446996, 0, 0, '', '', './Uploads/thumb//20120102/s_a18c30f221d25051bed34b6527eccd44f4df9b43.JPG', 2, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(150, 'selectersky', 1325447845, 0, 0, '', '', './Uploads/thumb//20120102/s_463fd3cfc7de963427c4e365dc610aeb0ae97816.JPG', 1, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(151, 'selectersky', 1325449378, 0, 0, '', '', './Uploads/thumb//20120102/s_e54a006b1bc5a700a56c071bbf2cb0ca7e8840fe.JPG', 2, '资料图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(152, 'selectersky', 1325449590, 0, 0, '', '', './Uploads/thumb//20120102/s_d8a716a3df76c625c346bf57df43b8440147d18d.JPG', 2, '资料图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(153, 'selectersky', 1325449837, 0, 0, '', '', './Uploads/thumb//20120102/s_e723328213a6c7b288ccc616843888582aa6ade6.JPG', 2, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(154, 'selectersky', 1325449970, 0, 0, '', '', './Uploads/thumb//20120102/s_bcbfe521f2633b8c7966184e58517a0f3f6b458f.JPG', 2, '资料图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(156, 'selectersky', 1325514017, 0, 0, '', '', './Uploads/thumb//20120102/s_886a1c99773bfc30bcd9d6d24bb6b76048032caa.JPG', 2, '资料图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 10),
(157, 'selectersky', 1325451428, 0, 0, '', '', './Uploads/thumb//20120102/s_2866eee0963197bc1027d3251de34d3ce099d8ef.JPG', 2, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(161, 'xieyd', 1325451726, 0, 0, '', '', './Uploads/thumb//20120102/s_6fd50ff76181a63fd2bc34e5c5eccdf0ee362aa5.jpg', 13, '时效图片', '', '', 'Paris', 0),
(164, 'xieyd', 1325452535, 0, 0, '', '', './Uploads/thumb//20120102/s_5784ee2433a2c8a42d5337fb1f294467fdf68ac8.JPG', 4, '时效图片', 'EVA JOLY', 'PHOTOPQR/LE PARISIEN/MARC MENOU ; 20/12/2011. PARVIS DE LA GARE ET DE LA TOUR MONTPARNASSE. EVA JOLY LA CANDIDATE DES VERTS A L'' ELECTION PRESIDENTIELLE SUR LE MARCHE DE NOEL \r\n2011/12/20. Norway-born politician Eva Joly, the French Green movement''s presidential candidate. Photo via Newscom', 'PARIS XIV', 0),
(165, 'xieyd', 1325452621, 0, 0, '', '', './Uploads/thumb//20120102/s_b61475243f0582b5dc7bc837db363b58dd027602.JPG', 15, '时效图片', '', 'Christophe Morin / IP3 . France''s Socialist Party (PS) candidate for the 2012 presidential election Francois Hollande (C) waves to as he visits the market of the Val Fourre district in Mantes la Jolie, a suburb located west of Paris, on December 20, 2011\r\rFrance, Mantes la Jolie le 20 decembre 2011. Deplacement a Mantes la Jolie de Francois Hollande, candidat du Parti Socialiste pour les elections presidentielles de 2012, pour soutenir le Mouvement de Jeunes Socialistes dans le cadre de la caravane d inscri', 'Paris', 0),
(167, 'selectersky', 1325516937, 0, 0, '', '', './Uploads/thumb//20120102/s_6699669df0329a8fdaf281f38b961905c405fd89.JPG', 2, '时效图片', '杭州西博烟花大会', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(168, 'selectersky', 1325453185, 0, 0, '', '', './Uploads/thumb//20120102/s_27c9e3f8e28629484c783ae93729fe050bfd622e.jpg', 4, '时效图片', '', '', 'Paris', 0),
(169, 'selectersky', 1325453372, 0, 0, '', '', './Uploads/thumb//20120102/s_4b2629b4eff9f98469d88ff7608ae29b46a0de9d.jpg', 3, '时效图片', '', '', 'Paris', 0),
(170, 'selectersky', 1325517211, 0, 0, '', '', './Uploads/thumb//20120102/s_b6aa2d9da096d123e32a6db52b51406176ff6e75.JPG', 2, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(171, 'selectersky', 1325453461, 0, 0, '', '', './Uploads/thumb//20120102/s_e31c6c2bafe3b347264d1dfb11b8b61a5787b3c1.JPG', 2, '时效图片', '运河燃放点的烟花', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', '', 0),
(172, 'selectersky', 1325453673, 0, 0, '', '', './Uploads/thumb//20120102/s_7979cb6fc1609813f92def53dfb125c0f3662669.jpg', 7, '时效图片', '', '', 'Paris', 0),
(173, 'xieyd', 1325453898, 0, 0, '', '', './Uploads/thumb//20120102/s_c5df881b262cc873cb18f0be1adddfaefd6b0dbc.jpg', 4, '时效图片', '302227_012', 'Actor Harrison Ford hanging out in Los Angeles, CA, USA, December 20, 2011. Photo by Frederic Madison/ABACAPRESS.COM  # 302227_012', 'Los Angeles', 0),
(175, 'selectersky', 1325518338, 0, 0, '', '', './Uploads/thumb//20120102/s_9317b4a1e00ff2e74c5fd2fb38f995590ae20657.jpg', 4, '时效图片', '', '', 'Paris', 0),
(176, 'selectersky', 1325518466, 0, 0, '', '', './Uploads/thumb//20120102/s_d1dffeb4d0cdc138b169289975d642d2a979e4e7.jpg', 3, '时效图片', '', '', 'Paris', 0),
(177, 'selectersky', 1325518741, 0, 0, '', '', './Uploads/thumb//20120102/s_b4f6efb46f8fe475e276e071426e20571feca479.jpg', 4, '时效图片', '', '', 'Paris', 0),
(178, 'selectersky', 1325519173, 0, 0, '', '', './Uploads/thumb//20120102/s_b029eb68dae0e5e76cac9b1051e9c81e290bf7f7.jpg', 3, '时效图片', 'Eiffel Tower Skating Rink - Pari', 'Skaters gliding along the ice on the first floor of the Eiffel Tower, 57 meters above ground level, on the first floor of the Eiffel Tower in Paris, France, December 20, 2011. The rink will only be in operation until February 1st. It is the second consecutive year that the ice skating rink opens in the Eiffel Tower. At 200 square meters (2,150 sq feet), it is only about a third of the size of New Yorks famed Rockefeller Center ice rink. Last year, more than 1,000 skaters visited the Towers frozen surface, d', 'Paris', 0),
(179, 'selectersky', 1325519306, 0, 0, '', '', './Uploads/thumb//20120102/s_96027b933be930ad48d5b2a8884e12c30893385d.jpg', 4, '时效图片', 'Eiffel Tower Skating Rink - Pari', 'Skaters gliding along the ice on the first floor of the Eiffel Tower, 57 meters above ground level, on the first floor of the Eiffel Tower in Paris, France, December 20, 2011. The rink will only be in operation until February 1st. It is the second consecutive year that the ice skating rink opens in the Eiffel Tower. At 200 square meters (2,150 sq feet), it is only about a third of the size of New Yorks famed Rockefeller Center ice rink. Last year, more than 1,000 skaters visited the Towers frozen surface, d', 'Paris', 0),
(181, 'xieyd', 1325456027, 0, 0, '', '', './Uploads/thumb//20120102/s_389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', 6, '时效图片', 'NHL: DEC 30 Sabres at Capitals', '30 December 2011:   Buffalo Sabres goalie Ryan Miller (30) gives up a goal in action against Washington Capitals center Brooks Laich (21), at the Verizon Center in Washington, D.C. where the Washington Capitals defeated the Buffalo Sabres 3-1. Photo via Newscom', 'WASHINGTON', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_upload_images_detail`
--

DROP TABLE IF EXISTS `pc_upload_images_detail`;
CREATE TABLE IF NOT EXISTS `pc_upload_images_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `account` varchar(32) CHARACTER SET utf8 NOT NULL,
  `createtime` int(11) NOT NULL,
  `photo_time` int(11) NOT NULL,
  `filename` varchar(64) CHARACTER SET utf8 NOT NULL,
  `cate` varchar(20) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type` varchar(64) CHARACTER SET utf8 NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `province` varchar(50) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL,
  `point` int(11) NOT NULL,
  `check_state` tinyint(4) NOT NULL,
  `big_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `small_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `filesize` int(11) NOT NULL,
  `author` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=350 ;

--
-- 转存表中的数据 `pc_upload_images_detail`
--

INSERT INTO `pc_upload_images_detail` (`id`, `group_id`, `account`, `createtime`, `photo_time`, `filename`, `cate`, `url`, `type`, `title`, `country`, `province`, `city`, `remark`, `point`, `check_state`, `big_url`, `small_url`, `width`, `height`, `filesize`, `author`) VALUES
(82, 50, 'photoer', 1325322793, 0, 'bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', '', './Uploads/images/bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', '', '', '', '', '成都市', '第三方', 100, 1, './Uploads/images/m_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', './Uploads/images/s_bd90eb1c8bc4f06a4710cd157f4c7cd2856875a4.jpg', 343, 342, 95, ''),
(83, 50, 'photoer', 1325322793, 0, '66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', '', './Uploads/images/66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', '', '', '', '', '成都市', '第三方', 100, 1, './Uploads/images/m_66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', './Uploads/images/s_66d4c419a39760b02a0f68ae88dbce577a5aa976.jpg', 504, 517, 92, ''),
(84, 51, 'xieyd', 1325326704, 0, '04bb30cd23dc02bc3503effb2e84d0849dde751c.jpg', '', './Uploads/images/04bb30cd23dc02bc3503effb2e84d0849dde751c.jpg', '', '', '', '', '杭州市', '运河燃放点的烟花.', 100, 1, './Uploads/images/m_04bb30cd23dc02bc3503effb2e84d0849dde751c.jpg', './Uploads/images/s_04bb30cd23dc02bc3503effb2e84d0849dde751c.jpg', 4256, 2832, 1711, ''),
(85, 51, 'xieyd', 1325326706, 0, '69f0a923d97e7f1f952bd4aaa39d325ece39b20d.jpg', '', './Uploads/images/69f0a923d97e7f1f952bd4aaa39d325ece39b20d.jpg', '', '', '', '', '杭州市', '运河燃放点的烟花.', 100, 1, './Uploads/images/m_69f0a923d97e7f1f952bd4aaa39d325ece39b20d.jpg', './Uploads/images/s_69f0a923d97e7f1f952bd4aaa39d325ece39b20d.jpg', 4256, 2832, 1843, ''),
(86, 51, 'xieyd', 1325326710, 0, '97a516b1aa3f440d64d4055a6c872bfdd9ca95cb.jpg', '', './Uploads/images/97a516b1aa3f440d64d4055a6c872bfdd9ca95cb.jpg', '', '', '', '', '杭州市', '运河燃放点的烟花.', 100, 0, './Uploads/images/m_97a516b1aa3f440d64d4055a6c872bfdd9ca95cb.jpg', './Uploads/images/s_97a516b1aa3f440d64d4055a6c872bfdd9ca95cb.jpg', 4256, 2832, 1798, ''),
(117, 120, 'selectersky', 1325485575, 0, '78cfa527d5e9e4247d0088b03b1172e2a796e39d.JPG', '', './Uploads/images//20120102/78cfa527d5e9e4247d0088b03b1172e2a796e39d.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.222', 100, 0, './Uploads/thumb//20120102/m_78cfa527d5e9e4247d0088b03b1172e2a796e39d.JPG', './Uploads/thumb//20120102/s_78cfa527d5e9e4247d0088b03b1172e2a796e39d.JPG', 4256, 2832, 1711, 'Xie Youding'),
(118, 120, 'selectersky', 1325485584, 0, '10be5af8adee909103cbbe680680cbbb03de8706.JPG', '', './Uploads/images//20120102/10be5af8adee909103cbbe680680cbbb03de8706.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.333', 100, 0, './Uploads/thumb//20120102/m_10be5af8adee909103cbbe680680cbbb03de8706.JPG', './Uploads/thumb//20120102/s_10be5af8adee909103cbbe680680cbbb03de8706.JPG', 4256, 2832, 1843, 'Xie Youding'),
(119, 121, 'selectersky', 1325485878, 0, '64b3610ff6b911f181cadfdfa51087583ebdc3b4.jpg', '', './Uploads/images//20120102/64b3610ff6b911f181cadfdfa51087583ebdc3b4.jpg', '', '', '', '海南省', '海口市', '234243', 100, 0, './Uploads/thumb//20120102/m_64b3610ff6b911f181cadfdfa51087583ebdc3b4.jpg', './Uploads/thumb//20120102/s_64b3610ff6b911f181cadfdfa51087583ebdc3b4.jpg', 596, 375, 53, ''),
(120, 121, 'selectersky', 1325485878, 0, '43685dc4d1412375c5de2fef2961c95786cd07db.jpg', '', './Uploads/images//20120102/43685dc4d1412375c5de2fef2961c95786cd07db.jpg', '', '', '', '海南省', '海口市', '2342424', 100, 0, './Uploads/thumb//20120102/m_43685dc4d1412375c5de2fef2961c95786cd07db.jpg', './Uploads/thumb//20120102/s_43685dc4d1412375c5de2fef2961c95786cd07db.jpg', 599, 506, 80, ''),
(121, 121, 'selectersky', 1325485879, 0, '14414f7bc869cfbadc10487637693f7a78e1ec25.jpg', '', './Uploads/images//20120102/14414f7bc869cfbadc10487637693f7a78e1ec25.jpg', '', '', '', '海南省', '海口市', '2342424', 100, 0, './Uploads/thumb//20120102/m_14414f7bc869cfbadc10487637693f7a78e1ec25.jpg', './Uploads/thumb//20120102/s_14414f7bc869cfbadc10487637693f7a78e1ec25.jpg', 983, 520, 100, ''),
(122, 122, 'selectersky', 1325423212, 0, '965c444cf6f70f181670555417e8f349168acc59.JPG', '', './Uploads/images//20120101/965c444cf6f70f181670555417e8f349168acc59.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_965c444cf6f70f181670555417e8f349168acc59.JPG', './Uploads/thumb//20120101/s_965c444cf6f70f181670555417e8f349168acc59.JPG', 4256, 2832, 1843, 'Xie Youding'),
(123, 123, 'selectersky', 1325423654, 0, '18744a1256d9fead143ddde634fb5e1159a9ba09.JPG', '', './Uploads/images//20120101/18744a1256d9fead143ddde634fb5e1159a9ba09.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_18744a1256d9fead143ddde634fb5e1159a9ba09.JPG', './Uploads/thumb//20120101/s_18744a1256d9fead143ddde634fb5e1159a9ba09.JPG', 4256, 2832, 1711, 'Xie Youding'),
(124, 123, 'selectersky', 1325423657, 0, '4696d52add5d1c5854cca82793df1da543451bd7.JPG', '', './Uploads/images//20120101/4696d52add5d1c5854cca82793df1da543451bd7.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_4696d52add5d1c5854cca82793df1da543451bd7.JPG', './Uploads/thumb//20120101/s_4696d52add5d1c5854cca82793df1da543451bd7.JPG', 4256, 2832, 1843, 'Xie Youding'),
(125, 124, 'selectersky', 1325423722, 0, '095076003595f88b56e888fd96317677b8dc8471.JPG', '', './Uploads/images//20120101/095076003595f88b56e888fd96317677b8dc8471.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_095076003595f88b56e888fd96317677b8dc8471.JPG', './Uploads/thumb//20120101/s_095076003595f88b56e888fd96317677b8dc8471.JPG', 4256, 2832, 1843, 'Xie Youding'),
(126, 124, 'selectersky', 1325423724, 0, 'fa91ada3930b6372e0d84a63e22aa4ff806c93d1.JPG', '', './Uploads/images//20120101/fa91ada3930b6372e0d84a63e22aa4ff806c93d1.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_fa91ada3930b6372e0d84a63e22aa4ff806c93d1.JPG', './Uploads/thumb//20120101/s_fa91ada3930b6372e0d84a63e22aa4ff806c93d1.JPG', 4256, 2832, 2159, 'Xie Youding'),
(127, 124, 'selectersky', 1325423727, 0, '8ac05e2ed8ad324225951814d0ba7ffe06ddb929.JPG', '', './Uploads/images//20120101/8ac05e2ed8ad324225951814d0ba7ffe06ddb929.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_8ac05e2ed8ad324225951814d0ba7ffe06ddb929.JPG', './Uploads/thumb//20120101/s_8ac05e2ed8ad324225951814d0ba7ffe06ddb929.JPG', 4256, 2832, 1798, 'Xie Youding'),
(128, 125, 'xieyd', 1325423902, 0, 'c8716822f23f9095cf767b0435c5e029993dcf00.JPG', '时事', './Uploads/images//20120101/c8716822f23f9095cf767b0435c5e029993dcf00.JPG', '', '杭州西博烟花大会', '中国', '浙江', '杭州', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_c8716822f23f9095cf767b0435c5e029993dcf00.JPG', './Uploads/thumb//20120101/s_c8716822f23f9095cf767b0435c5e029993dcf00.JPG', 4256, 2832, 1711, 'Xie Youding'),
(129, 125, 'xieyd', 1325423905, 0, 'cc558b7e2c5ca6f553d47b1ec3d48ebefbc7c626.JPG', '时事', './Uploads/images//20120101/cc558b7e2c5ca6f553d47b1ec3d48ebefbc7c626.JPG', '', '杭州西博烟花大会', '中国', '浙江', '杭州', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_cc558b7e2c5ca6f553d47b1ec3d48ebefbc7c626.JPG', './Uploads/thumb//20120101/s_cc558b7e2c5ca6f553d47b1ec3d48ebefbc7c626.JPG', 4256, 2832, 1843, 'Xie Youding'),
(130, 125, 'xieyd', 1325423907, 0, '9c2118281f5f626960e29056188a16f124c8c1bd.JPG', '时事', './Uploads/images//20120101/9c2118281f5f626960e29056188a16f124c8c1bd.JPG', '', '杭州西博烟花大会', '中国', '浙江', '杭州', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_9c2118281f5f626960e29056188a16f124c8c1bd.JPG', './Uploads/thumb//20120101/s_9c2118281f5f626960e29056188a16f124c8c1bd.JPG', 4256, 2832, 2159, 'Xie Youding'),
(131, 125, 'xieyd', 1325423910, 0, '8d42d1d72864f2d927ef668637268e6cb9dbcc4d.JPG', '时事', './Uploads/images//20120101/8d42d1d72864f2d927ef668637268e6cb9dbcc4d.JPG', '', '杭州西博烟花大会', '中国', '浙江', '杭州', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_8d42d1d72864f2d927ef668637268e6cb9dbcc4d.JPG', './Uploads/thumb//20120101/s_8d42d1d72864f2d927ef668637268e6cb9dbcc4d.JPG', 4256, 2832, 1798, 'Xie Youding'),
(132, 128, 'xieyd', 1325425385, 0, '2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', '时事', './Uploads/images//20120101/2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', './Uploads/thumb//20120101/s_2f33c4d505e1bda2193b54e2a237c3fa53510e29.JPG', 2832, 4256, 4100, 'Xie Youding'),
(133, 128, 'xieyd', 1325425388, 0, 'ff6660b12699b57abbb44c7b5311192cb1c3fff9.JPG', '时事', './Uploads/images//20120101/ff6660b12699b57abbb44c7b5311192cb1c3fff9.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_ff6660b12699b57abbb44c7b5311192cb1c3fff9.JPG', './Uploads/thumb//20120101/s_ff6660b12699b57abbb44c7b5311192cb1c3fff9.JPG', 2832, 4256, 4643, 'Xie Youding'),
(134, 128, 'xieyd', 1325425391, 0, '1aef7644feb0842cca30232ddfff79ef54ae2ce0.JPG', '时事', './Uploads/images//20120101/1aef7644feb0842cca30232ddfff79ef54ae2ce0.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_1aef7644feb0842cca30232ddfff79ef54ae2ce0.JPG', './Uploads/thumb//20120101/s_1aef7644feb0842cca30232ddfff79ef54ae2ce0.JPG', 4256, 2832, 4324, 'Xie Youding'),
(135, 128, 'xieyd', 1325425395, 0, '08461a1d2fcaec0193edbd62c27d078def8eb8f9.JPG', '时事', './Uploads/images//20120101/08461a1d2fcaec0193edbd62c27d078def8eb8f9.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_08461a1d2fcaec0193edbd62c27d078def8eb8f9.JPG', './Uploads/thumb//20120101/s_08461a1d2fcaec0193edbd62c27d078def8eb8f9.JPG', 4256, 2832, 5337, 'Xie Youding'),
(136, 128, 'xieyd', 1325425398, 0, '57b6e35b2df18531a7cb002d8c823cf7910545dd.JPG', '时事', './Uploads/images//20120101/57b6e35b2df18531a7cb002d8c823cf7910545dd.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_57b6e35b2df18531a7cb002d8c823cf7910545dd.JPG', './Uploads/thumb//20120101/s_57b6e35b2df18531a7cb002d8c823cf7910545dd.JPG', 4256, 2832, 4615, 'Xie Youding'),
(137, 128, 'xieyd', 1325425402, 0, 'a2df00eb775515850264130412ff5a746c21abb5.JPG', '时事', './Uploads/images//20120101/a2df00eb775515850264130412ff5a746c21abb5.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_a2df00eb775515850264130412ff5a746c21abb5.JPG', './Uploads/thumb//20120101/s_a2df00eb775515850264130412ff5a746c21abb5.JPG', 4256, 2832, 5239, 'Xie Youding'),
(138, 128, 'xieyd', 1325425405, 0, '86e090c6b29e0ea763a5116b49973f32c376f504.JPG', '时事', './Uploads/images//20120101/86e090c6b29e0ea763a5116b49973f32c376f504.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_86e090c6b29e0ea763a5116b49973f32c376f504.JPG', './Uploads/thumb//20120101/s_86e090c6b29e0ea763a5116b49973f32c376f504.JPG', 4256, 2832, 5495, 'Xie Youding'),
(139, 128, 'xieyd', 1325425408, 0, '91549e3ba306903fed143cc3e391cf47931b1fc7.JPG', '时事', './Uploads/images//20120101/91549e3ba306903fed143cc3e391cf47931b1fc7.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_91549e3ba306903fed143cc3e391cf47931b1fc7.JPG', './Uploads/thumb//20120101/s_91549e3ba306903fed143cc3e391cf47931b1fc7.JPG', 4256, 2832, 3601, 'Xie Youding'),
(140, 128, 'xieyd', 1325425412, 0, 'f4f01520f1fc2d03c874606e72eaf82c2a95315a.JPG', '时事', './Uploads/images//20120101/f4f01520f1fc2d03c874606e72eaf82c2a95315a.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 1, './Uploads/thumb//20120101/m_f4f01520f1fc2d03c874606e72eaf82c2a95315a.JPG', './Uploads/thumb//20120101/s_f4f01520f1fc2d03c874606e72eaf82c2a95315a.JPG', 4256, 2832, 6632, 'Xie Youding'),
(141, 128, 'xieyd', 1325425415, 0, 'e606bf0c10531837f5a1844c7988a3e0e5c091c5.JPG', '时事', './Uploads/images//20120101/e606bf0c10531837f5a1844c7988a3e0e5c091c5.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 100, 0, './Uploads/thumb//20120101/m_e606bf0c10531837f5a1844c7988a3e0e5c091c5.JPG', './Uploads/thumb//20120101/s_e606bf0c10531837f5a1844c7988a3e0e5c091c5.JPG', 4256, 2832, 6117, 'Xie Youding'),
(142, 129, 'selectersky', 1325426001, 0, '1b9c9649d141c1191779ec6ec65f1ab529299fdc.jpg', '', './Uploads/images//20120101/1b9c9649d141c1191779ec6ec65f1ab529299fdc.jpg', '', '', '', '', '', '', 0, 0, './Uploads/thumb//20120101/m_1b9c9649d141c1191779ec6ec65f1ab529299fdc.jpg', './Uploads/thumb//20120101/s_1b9c9649d141c1191779ec6ec65f1ab529299fdc.jpg', 596, 375, 53, ''),
(143, 130, 'selectersky', 1325490314, 0, 'e100e808fbedddcf4e4c5bd04ebc537bb876155d.jpg', '', './Uploads/images//20120102/e100e808fbedddcf4e4c5bd04ebc537bb876155d.jpg', '', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_e100e808fbedddcf4e4c5bd04ebc537bb876155d.jpg', './Uploads/thumb//20120102/s_e100e808fbedddcf4e4c5bd04ebc537bb876155d.jpg', 596, 375, 53, ''),
(144, 131, 'selectersky', 1325427778, 0, '62216675f42c4223ea63d47faf539e6d982829a8.JPG', '', './Uploads/images//20120101/62216675f42c4223ea63d47faf539e6d982829a8.JPG', '', '运河燃放点的烟花', '', '', '', '', 0, 1, './Uploads/thumb//20120101/m_62216675f42c4223ea63d47faf539e6d982829a8.JPG', './Uploads/thumb//20120101/s_62216675f42c4223ea63d47faf539e6d982829a8.JPG', 4256, 2832, 1711, 'Xie Youding'),
(145, 132, 'selectersky', 1325427806, 0, '8b95de75d8d216722807f3d9ec5cc3b78647443e.JPG', '', './Uploads/images//20120101/8b95de75d8d216722807f3d9ec5cc3b78647443e.JPG', '', '运河燃放点的烟花', '', '', '', '', 0, 0, './Uploads/thumb//20120101/m_8b95de75d8d216722807f3d9ec5cc3b78647443e.JPG', './Uploads/thumb//20120101/s_8b95de75d8d216722807f3d9ec5cc3b78647443e.JPG', 4256, 2832, 1843, 'Xie Youding'),
(146, 132, 'selectersky', 1325427809, 0, 'da7ed893ad2183c7d4e45e636debf12cc341b70b.JPG', '', './Uploads/images//20120101/da7ed893ad2183c7d4e45e636debf12cc341b70b.JPG', '', '运河燃放点的烟花', '', '', '', '', 0, 0, './Uploads/thumb//20120101/m_da7ed893ad2183c7d4e45e636debf12cc341b70b.JPG', './Uploads/thumb//20120101/s_da7ed893ad2183c7d4e45e636debf12cc341b70b.JPG', 4256, 2832, 2159, 'Xie Youding'),
(147, 132, 'selectersky', 1325427811, 0, '8d8eacee21b0878aac1d50fd8687999cd83dba5c.JPG', '', './Uploads/images//20120101/8d8eacee21b0878aac1d50fd8687999cd83dba5c.JPG', '', '运河燃放点的烟花', '', '', '', '', 0, 0, './Uploads/thumb//20120101/m_8d8eacee21b0878aac1d50fd8687999cd83dba5c.JPG', './Uploads/thumb//20120101/s_8d8eacee21b0878aac1d50fd8687999cd83dba5c.JPG', 4256, 2832, 1798, 'Xie Youding'),
(148, 133, 'xieyd', 1325427880, 0, 'fec11986891b8e4e5fbe5dc1a6cb993a700939fa.JPG', '时事', './Uploads/images//20120101/fec11986891b8e4e5fbe5dc1a6cb993a700939fa.JPG', '', '杭州西博烟花大会', '', '', '', '', 0, 1, './Uploads/thumb//20120101/m_fec11986891b8e4e5fbe5dc1a6cb993a700939fa.JPG', './Uploads/thumb//20120101/s_fec11986891b8e4e5fbe5dc1a6cb993a700939fa.JPG', 4256, 2832, 3601, 'Xie Youding'),
(149, 133, 'xieyd', 1325427884, 0, '4471b81296c0d27e3d9c1ff0d598702c812a81a5.JPG', '时事', './Uploads/images//20120101/4471b81296c0d27e3d9c1ff0d598702c812a81a5.JPG', '', '杭州西博烟花大会', '', '', '', '', 0, 1, './Uploads/thumb//20120101/m_4471b81296c0d27e3d9c1ff0d598702c812a81a5.JPG', './Uploads/thumb//20120101/s_4471b81296c0d27e3d9c1ff0d598702c812a81a5.JPG', 4256, 2832, 6632, 'Xie Youding'),
(150, 133, 'xieyd', 1325427889, 0, 'b2ab63e0cb5543ebec2a3e802b442a4b3e0f6c39.JPG', '时事', './Uploads/images//20120101/b2ab63e0cb5543ebec2a3e802b442a4b3e0f6c39.JPG', '', '杭州西博烟花大会', '', '', '', '', 0, 1, './Uploads/thumb//20120101/m_b2ab63e0cb5543ebec2a3e802b442a4b3e0f6c39.JPG', './Uploads/thumb//20120101/s_b2ab63e0cb5543ebec2a3e802b442a4b3e0f6c39.JPG', 4256, 2832, 6117, 'Xie Youding'),
(151, 134, 'selectersky', 1325491890, 0, '9376a21beef3cfa0b71b71bbde4af60726161275.jpg', '', './Uploads/images//20120102/9376a21beef3cfa0b71b71bbde4af60726161275.jpg', '', '', '', '', '', '12', 0, 0, './Uploads/thumb//20120102/m_9376a21beef3cfa0b71b71bbde4af60726161275.jpg', './Uploads/thumb//20120102/s_9376a21beef3cfa0b71b71bbde4af60726161275.jpg', 596, 375, 53, ''),
(152, 134, 'selectersky', 1325491910, 0, '920713cb469415517addb523ea618b2394a6be0f.jpg', '', './Uploads/images//20120102/920713cb469415517addb523ea618b2394a6be0f.jpg', '', '299390_001', 'France', '', 'Paris', '12', 0, 0, './Uploads/thumb//20120102/m_920713cb469415517addb523ea618b2394a6be0f.jpg', './Uploads/thumb//20120102/s_920713cb469415517addb523ea618b2394a6be0f.jpg', 2864, 4500, 4990, 'Coppee Patrice'),
(153, 134, 'selectersky', 1325491913, 0, 'e0a2968f29f68e93b0680debcbf939d022de4b6d.jpg', '', './Uploads/images//20120102/e0a2968f29f68e93b0680debcbf939d022de4b6d.jpg', '', '', '', '', '', '1212', 0, 0, './Uploads/thumb//20120102/m_e0a2968f29f68e93b0680debcbf939d022de4b6d.jpg', './Uploads/thumb//20120102/s_e0a2968f29f68e93b0680debcbf939d022de4b6d.jpg', 1494, 2112, 360, ''),
(154, 135, 'xieyd', 1325428366, 0, 'b3b928edfbcce15e1e2524d553c6a1a63754045a.JPG', '时事', './Uploads/images//20120101/b3b928edfbcce15e1e2524d553c6a1a63754045a.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 0, 0, './Uploads/thumb//20120101/m_b3b928edfbcce15e1e2524d553c6a1a63754045a.JPG', './Uploads/thumb//20120101/s_b3b928edfbcce15e1e2524d553c6a1a63754045a.JPG', 4256, 2832, 5495, 'Xie Youding'),
(155, 135, 'xieyd', 1325428370, 0, '43cfdf80a14fcb67544b6bd0a2204dd691a24a77.JPG', '时事', './Uploads/images//20120101/43cfdf80a14fcb67544b6bd0a2204dd691a24a77.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 0, 0, './Uploads/thumb//20120101/m_43cfdf80a14fcb67544b6bd0a2204dd691a24a77.JPG', './Uploads/thumb//20120101/s_43cfdf80a14fcb67544b6bd0a2204dd691a24a77.JPG', 4256, 2832, 5337, 'Xie Youding'),
(156, 135, 'xieyd', 1325428374, 0, 'bb0253ece8bcfa0a3208d88440392ab2c98a196e.JPG', '时事', './Uploads/images//20120101/bb0253ece8bcfa0a3208d88440392ab2c98a196e.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 0, 0, './Uploads/thumb//20120101/m_bb0253ece8bcfa0a3208d88440392ab2c98a196e.JPG', './Uploads/thumb//20120101/s_bb0253ece8bcfa0a3208d88440392ab2c98a196e.JPG', 4256, 2832, 4615, 'Xie Youding'),
(157, 135, 'xieyd', 1325428378, 0, 'd24343eea7f7c560b12de71c18928c1ded5416ca.JPG', '时事', './Uploads/images//20120101/d24343eea7f7c560b12de71c18928c1ded5416ca.JPG', '', '杭州西博烟花大会', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.', 0, 0, './Uploads/thumb//20120101/m_d24343eea7f7c560b12de71c18928c1ded5416ca.JPG', './Uploads/thumb//20120101/s_d24343eea7f7c560b12de71c18928c1ded5416ca.JPG', 4256, 2832, 5239, 'Xie Youding'),
(158, 136, 'xieyd', 1325443410, 0, 'f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', 'SOCCER CLUB', './Uploads/images//20120102/f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', '', 'SPT_CHELSEA_ASTON_VILLA_008', 'England', '', 'London', 'Chelsea''s Didier Drogba celebrates by pointing at the penalty spot after he scores from the penalty spot to give Chelsea a 1:0 lead\r\nChelsea 31/12/11\r\nChelsea V Aston Villa 31/12/11\r\nThe Premier League\r\nPhoto: Richard Washbrooke Fotosports International\r\n', 0, 1, './Uploads/thumb//20120102/m_f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', './Uploads/thumb//20120102/s_f872bcdf8054a397f2d863fba70ef95b922d3214.JPG', 2448, 1815, 1922, 'Richard Washbrooke'),
(159, 136, 'xieyd', 1325443412, 0, 'a47fc5014a0996c735fe402772dc9955c185f9db.JPG', '', './Uploads/images//20120102/a47fc5014a0996c735fe402772dc9955c185f9db.JPG', '', 'ARSENAL_QUEENS_PARK_RANGERS_01', 'England', '', 'LONDON', 'Arsenal''s Thomas Vermaelen  battles with QPR''s Jamie Mackie\r\nFA Barclays Premiership. Arsenal v QPR. The Emirates. 31.12.11\r\nPhoto By Karl Winter Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_a47fc5014a0996c735fe402772dc9955c185f9db.JPG', './Uploads/thumb//20120102/s_a47fc5014a0996c735fe402772dc9955c185f9db.JPG', 2654, 3000, 918, 'Karl Winter/Fotosports Int''l'),
(160, 136, 'xieyd', 1325443414, 0, '10a4708c1db21e677b1980dfa64035fb539d2ab3.JPG', '', './Uploads/images//20120102/10a4708c1db21e677b1980dfa64035fb539d2ab3.JPG', '', 'ARSENAL_QUEENS_PARK_RANGERS_04', 'England', '', 'LONDON', 'Arsenal''s Thomas Vermaelen  battles with QPR''s Jamie Mackie\r\nFA Barclays Premiership. Arsenal v QPR. The Emirates. 31.12.11\r\nPhoto By Karl Winter Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_10a4708c1db21e677b1980dfa64035fb539d2ab3.JPG', './Uploads/thumb//20120102/s_10a4708c1db21e677b1980dfa64035fb539d2ab3.JPG', 3000, 2299, 876, 'Karl Winter/Fotosports Int''l'),
(161, 136, 'xieyd', 1325443416, 0, 'bb652cc2f0a965e464d3d060094d0aa81d923dc1.JPG', '', './Uploads/images//20120102/bb652cc2f0a965e464d3d060094d0aa81d923dc1.JPG', '', 'ARSENAL_QUEENS_PARK_RANGERS_05', 'England', '', 'LONDON', 'Arsenal''s Mikel Arteta  battles with QPR''s Jay Bothroyd\r\nFA Barclays Premiership. Arsenal v QPR. The Emirates. 31.12.11\r\nPhoto By Karl Winter Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_bb652cc2f0a965e464d3d060094d0aa81d923dc1.JPG', './Uploads/thumb//20120102/s_bb652cc2f0a965e464d3d060094d0aa81d923dc1.JPG', 2033, 3000, 910, 'Karl Winter/Fotosports Int''l'),
(162, 136, 'xieyd', 1325443418, 0, 'c3c7163587f9afedd7f812f5b95006fd35e8c347.JPG', '', './Uploads/images//20120102/c3c7163587f9afedd7f812f5b95006fd35e8c347.JPG', '', 'SPT_BHAM_BLACKPOOL_005', 'England', '', 'Birmingham', 'St andrews Ground Birmingham City v Blackpool Championship 31/12/2011\r\nCurtis Davies (Birmingham)  scores  first goal with header  \r\nPhoto: Roger Parker Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_c3c7163587f9afedd7f812f5b95006fd35e8c347.JPG', './Uploads/thumb//20120102/s_c3c7163587f9afedd7f812f5b95006fd35e8c347.JPG', 3000, 1843, 883, 'roger parker'),
(163, 136, 'xieyd', 1325443420, 0, 'e86d13c1a957f57ca13e541fd2466995af9a83a2.JPG', 'SOCCER CLUB', './Uploads/images//20120102/e86d13c1a957f57ca13e541fd2466995af9a83a2.JPG', '', 'SPT_CHELSEA_ASTON_VILLA_006', 'England', '', 'London', 'Chelsea''s Didier Drogba jokes with Aston Villa''s Stephen Warnock pretending to ride him a like a horse after they tangle. Chelsea are level with Aston Villa 1:1\r\nChelsea 31/12/11\r\nChelsea V Aston Villa 31/12/11\r\nThe Premier League\r\nPhoto: Richard Washbroo', 0, 0, './Uploads/thumb//20120102/m_e86d13c1a957f57ca13e541fd2466995af9a83a2.JPG', './Uploads/thumb//20120102/s_e86d13c1a957f57ca13e541fd2466995af9a83a2.JPG', 3495, 2529, 2359, 'Richard Washbrooke'),
(164, 136, 'xieyd', 1325443422, 0, '19aab1b097e0bb61f6d59622e42f9647e71406de.JPG', '', './Uploads/images//20120102/19aab1b097e0bb61f6d59622e42f9647e71406de.JPG', '', 'SPT_BHAM_BLACKPOOL_002', 'England', '', 'Birmingham', 'St andrews Ground Birmingham City v Blackpool Championship 31/12/2011\r\nCurtis Davies (Birmingham)  celebrates scoring first goal with Steven Caldwell\r\nPhoto: Roger Parker Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_19aab1b097e0bb61f6d59622e42f9647e71406de.JPG', './Uploads/thumb//20120102/s_19aab1b097e0bb61f6d59622e42f9647e71406de.JPG', 2389, 3000, 1017, 'roger parker'),
(165, 136, 'xieyd', 1325443424, 0, '5873c396a099b70868366fa9320d742f3ef06ee5.JPG', '', './Uploads/images//20120102/5873c396a099b70868366fa9320d742f3ef06ee5.JPG', '', 'SPT_BHAM_BLACKPOOL_003', 'England', '', 'Birmingham', 'St andrews Ground Birmingham City v Blackpool Championship 31/12/2011\r\nCurtis Davies (Birmingham)  celebrates scoring first goal with Steven Caldwell\r\nPhoto: Roger Parker Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_5873c396a099b70868366fa9320d742f3ef06ee5.JPG', './Uploads/thumb//20120102/s_5873c396a099b70868366fa9320d742f3ef06ee5.JPG', 3000, 2314, 1043, 'roger parker'),
(166, 136, 'xieyd', 1325443426, 0, '038e9899c5f66eb0fdc1d3b31d7b529cf9fe78d8.JPG', '', './Uploads/images//20120102/038e9899c5f66eb0fdc1d3b31d7b529cf9fe78d8.JPG', '', 'SPT_BHAM_BLACKPOOL_004', 'England', '', 'Birmingham', 'St andrews Ground Birmingham City v Blackpool Championship 31/12/2011\r\nCurtis Davies (Birmingham)  scores first goal \r\nPhoto: Roger Parker Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_038e9899c5f66eb0fdc1d3b31d7b529cf9fe78d8.JPG', './Uploads/thumb//20120102/s_038e9899c5f66eb0fdc1d3b31d7b529cf9fe78d8.JPG', 3000, 1907, 1097, 'roger parker'),
(167, 136, 'xieyd', 1325443427, 0, '0a5c99076d31e301ee49cd34c4d0a116e9963dba.JPG', '', './Uploads/images//20120102/0a5c99076d31e301ee49cd34c4d0a116e9963dba.JPG', '', 'SPT_BHAM_BLACKPOOL_006', 'England', '', 'Birmingham', 'St andrews Ground Birmingham City v Blackpool Championship 31/12/2011\r\nNikola Zigic (Birmingham)  is foiled by Mark Howard   (Blackpool)  \r\nPhoto: Roger Parker Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_0a5c99076d31e301ee49cd34c4d0a116e9963dba.JPG', './Uploads/thumb//20120102/s_0a5c99076d31e301ee49cd34c4d0a116e9963dba.JPG', 3000, 2024, 1042, 'roger parker'),
(168, 136, 'xieyd', 1325443428, 0, 'b188cc27dc9ccdedd5de19047bec233e71ca1529.JPG', 'SOCCER CLUB', './Uploads/images//20120102/b188cc27dc9ccdedd5de19047bec233e71ca1529.JPG', '', 'SPT_CHELSEA_ASTON_VILLA_004', 'England', '', 'London', 'Chelsea''s Didier Drogba celebrates with the fans after he scores from the penalty spot to give Chelsea a 1:0 lead\r\nChelsea 31/12/11\r\nChelsea V Aston Villa 31/12/11\r\nThe Premier League\r\nPhoto: Richard Washbrooke Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_b188cc27dc9ccdedd5de19047bec233e71ca1529.JPG', './Uploads/thumb//20120102/s_b188cc27dc9ccdedd5de19047bec233e71ca1529.JPG', 1095, 1594, 1496, 'Richard Washbrooke'),
(169, 136, 'xieyd', 1325443430, 0, 'b085ad6b0cef2b4255a725abe3ccb08ee7518739.JPG', '', './Uploads/images//20120102/b085ad6b0cef2b4255a725abe3ccb08ee7518739.JPG', '', 'SPT_BHAM_BLACKPOOL_001', 'England', '', 'Birmingham', 'St andrews Ground Birmingham City v Blackpool Championship 31/12/2011\r\nCurtis Davies (Birmingham)  celebrates scoring first goal with Steven Caldwell\r\nPhoto: Roger Parker Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_b085ad6b0cef2b4255a725abe3ccb08ee7518739.JPG', './Uploads/thumb//20120102/s_b085ad6b0cef2b4255a725abe3ccb08ee7518739.JPG', 3000, 2557, 1120, 'roger parker'),
(170, 136, 'xieyd', 1325443432, 0, 'f761bcd09319dd9e955794bc61f95d72da4c89ef.JPG', 'SOCCER CLUB', './Uploads/images//20120102/f761bcd09319dd9e955794bc61f95d72da4c89ef.JPG', '', 'SPT_CHELSEA_ASTON_VILLA_005', 'England', '', 'London', 'Aston Villa''s Manager Alex McLeish tries to calm the palyers down after they score to level the game 1:1 through Aston Villa''s Stephen Ireland goal\r\nChelsea 31/12/11\r\nChelsea V Aston Villa 31/12/11\r\nThe Premier League\r\nPhoto: Richard Washbrooke Fotosports', 0, 0, './Uploads/thumb//20120102/m_f761bcd09319dd9e955794bc61f95d72da4c89ef.JPG', './Uploads/thumb//20120102/s_f761bcd09319dd9e955794bc61f95d72da4c89ef.JPG', 1728, 1355, 1784, 'Richard Washbrooke'),
(171, 136, 'xieyd', 1325443433, 0, 'fea87cde9f6fba896de1bac8ee7213fdd54afc3c.JPG', '', './Uploads/images//20120102/fea87cde9f6fba896de1bac8ee7213fdd54afc3c.JPG', '', 'SPT_BHAM_BLACKPOOL_006', 'England', '', 'Birmingham', 'St andrews Ground Birmingham City v Blackpool Championship 31/12/2011\r\nNikola Zigic (Birmingham)  is foiled by Mark Howard Alex Baptiste and Ian Evatt  (Blackpool)  \r\nPhoto: Roger Parker Fotosports International\r\n Photo via Newscom', 0, 0, './Uploads/thumb//20120102/m_fea87cde9f6fba896de1bac8ee7213fdd54afc3c.JPG', './Uploads/thumb//20120102/s_fea87cde9f6fba896de1bac8ee7213fdd54afc3c.JPG', 3000, 1990, 903, 'roger parker'),
(172, 136, 'xieyd', 1325443436, 0, 'e04d0573005c05ea72fb713405374e76b2a88440.JPG', 'SOCCER CLUB', './Uploads/images//20120102/e04d0573005c05ea72fb713405374e76b2a88440.JPG', '', 'SPT_CHELSEA_ASTON_VILLA_007', 'England', '', 'London', 'Chelsea''s Didier Drogba jokes with Aston Villa''s Stephen Warnock pretending to ride him a like a horse after they tangle. Chelsea are level with Aston Villa 1:1\r\nChelsea 31/12/11\r\nChelsea V Aston Villa 31/12/11\r\nThe Premier League\r\nPhoto: Richard Washbroo', 0, 0, './Uploads/thumb//20120102/m_e04d0573005c05ea72fb713405374e76b2a88440.JPG', './Uploads/thumb//20120102/s_e04d0573005c05ea72fb713405374e76b2a88440.JPG', 3479, 2615, 2397, 'Richard Washbrooke'),
(173, 137, 'selectersky', 1325443922, 1319373003, '237b4dbc3ebd44634ae7798e4a8fb3c788aae72b.JPG', '', './Uploads/images//20120102/237b4dbc3ebd44634ae7798e4a8fb3c788aae72b.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.1', 0, 0, './Uploads/thumb//20120102/m_237b4dbc3ebd44634ae7798e4a8fb3c788aae72b.JPG', './Uploads/thumb//20120102/s_237b4dbc3ebd44634ae7798e4a8fb3c788aae72b.JPG', 4256, 2832, 1843, 'Xie Youding'),
(174, 137, 'selectersky', 1325443924, 1319373005, '7179a6d2c1577f0298f2be24760750af89953747.JPG', '', './Uploads/images//20120102/7179a6d2c1577f0298f2be24760750af89953747.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.2', 0, 0, './Uploads/thumb//20120102/m_7179a6d2c1577f0298f2be24760750af89953747.JPG', './Uploads/thumb//20120102/s_7179a6d2c1577f0298f2be24760750af89953747.JPG', 4256, 2832, 2159, 'Xie Youding'),
(175, 137, 'selectersky', 1325443927, 1319373009, 'f782c5be7f2a8519ef133b335775e162bafbb904.JPG', '', './Uploads/images//20120102/f782c5be7f2a8519ef133b335775e162bafbb904.JPG', '', '运河燃放点的烟花', '', '', '', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.3', 0, 0, './Uploads/thumb//20120102/m_f782c5be7f2a8519ef133b335775e162bafbb904.JPG', './Uploads/thumb//20120102/s_f782c5be7f2a8519ef133b335775e162bafbb904.JPG', 4256, 2832, 1798, 'Xie Youding'),
(176, 138, 'xieyd', 1325445221, 1325173340, '3d3a696799ce1a063a3c81a0803ee1c09242aa07.JPG', 'FBC', './Uploads/images//20120102/3d3a696799ce1a063a3c81a0803ee1c09242aa07.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears running back Terrance Ganaway (24) celebrates after scoring a touchdown during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_3d3a696799ce1a063a3c81a0803ee1c09242aa07.JPG', './Uploads/thumb//20120102/s_3d3a696799ce1a063a3c81a0803ee1c09242aa07.JPG', 3500, 2450, 3459, 'John Albright / Icon SMI'),
(177, 138, 'xieyd', 1325445224, 1325173440, '95fcaed3647b6f34f1d8847bc73f0d027481c20a.JPG', 'FBC', './Uploads/images//20120102/95fcaed3647b6f34f1d8847bc73f0d027481c20a.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears wide receiver Tevin Reese (16) celebrates on the sideline during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_95fcaed3647b6f34f1d8847bc73f0d027481c20a.JPG', './Uploads/thumb//20120102/s_95fcaed3647b6f34f1d8847bc73f0d027481c20a.JPG', 3500, 2450, 2715, 'John Albright / Icon SMI'),
(178, 138, 'xieyd', 1325445227, 1325173509, '3f7650dd97a99f10c3af526c3eaf267144555ad9.JPG', 'FBC', './Uploads/images//20120102/3f7650dd97a99f10c3af526c3eaf267144555ad9.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: the Baylor Bears mascot during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_3f7650dd97a99f10c3af526c3eaf267144555ad9.JPG', './Uploads/thumb//20120102/s_3f7650dd97a99f10c3af526c3eaf267144555ad9.JPG', 3500, 2451, 2613, 'John Albright / Icon SMI'),
(179, 138, 'xieyd', 1325445230, 1325173831, '9c224d78a96b7f57ac0c093b9b7afbfba2f82708.JPG', 'FBC', './Uploads/images//20120102/9c224d78a96b7f57ac0c093b9b7afbfba2f82708.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears quarterback Robert Griffin III (10) talks to the press after the Bears won the  2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_9c224d78a96b7f57ac0c093b9b7afbfba2f82708.JPG', './Uploads/thumb//20120102/s_9c224d78a96b7f57ac0c093b9b7afbfba2f82708.JPG', 3500, 2451, 2460, 'John Albright / Icon SMI'),
(180, 138, 'xieyd', 1325445233, 1325174100, 'b5a34c47c647a7560db2eb10dbf14375d905893d.JPG', 'FBC', './Uploads/images//20120102/b5a34c47c647a7560db2eb10dbf14375d905893d.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears head coach Art Briles kisses Baylor Bears quarterback Robert Griffin III (10) after winning the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_b5a34c47c647a7560db2eb10dbf14375d905893d.JPG', './Uploads/thumb//20120102/s_b5a34c47c647a7560db2eb10dbf14375d905893d.JPG', 3500, 2450, 2478, 'John Albright / Icon SMI'),
(181, 138, 'xieyd', 1325445236, 1325174459, '3a39b331d0f60de084cfa4cb30e20eba6473f78e.JPG', 'FBC', './Uploads/images//20120102/3a39b331d0f60de084cfa4cb30e20eba6473f78e.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears players celebrate after winning the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_3a39b331d0f60de084cfa4cb30e20eba6473f78e.JPG', './Uploads/thumb//20120102/s_3a39b331d0f60de084cfa4cb30e20eba6473f78e.JPG', 3500, 2448, 2445, 'John Albright / Icon SMI'),
(182, 138, 'xieyd', 1325445239, 1325174478, 'c12eb70b693d1d7cdafdd20c2c0538e638f9bd26.JPG', 'FBC', './Uploads/images//20120102/c12eb70b693d1d7cdafdd20c2c0538e638f9bd26.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Fireworks go off as Baylor Bears players celebrate after winning the  2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_c12eb70b693d1d7cdafdd20c2c0538e638f9bd26.JPG', './Uploads/thumb//20120102/s_c12eb70b693d1d7cdafdd20c2c0538e638f9bd26.JPG', 3500, 2450, 3079, 'John Albright / Icon SMI'),
(183, 138, 'xieyd', 1325445242, 1325174481, '5e39617e432df5743e8c3b9838406ccff7e8cd82.JPG', 'FBC', './Uploads/images//20120102/5e39617e432df5743e8c3b9838406ccff7e8cd82.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Fireworks go off as Baylor Bears players celebrate after winning the  2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_5e39617e432df5743e8c3b9838406ccff7e8cd82.JPG', './Uploads/thumb//20120102/s_5e39617e432df5743e8c3b9838406ccff7e8cd82.JPG', 3500, 2449, 3225, 'John Albright / Icon SMI'),
(184, 138, 'xieyd', 1325445245, 1325174492, '4d9c41b0ad6f9e30b13aeb6e2719f054b6aaacbc.JPG', 'FBC', './Uploads/images//20120102/4d9c41b0ad6f9e30b13aeb6e2719f054b6aaacbc.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Fireworks go off as Baylor Bears players celebrate after winning the  2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_4d9c41b0ad6f9e30b13aeb6e2719f054b6aaacbc.JPG', './Uploads/thumb//20120102/s_4d9c41b0ad6f9e30b13aeb6e2719f054b6aaacbc.JPG', 3500, 2449, 2352, 'John Albright / Icon SMI'),
(185, 138, 'xieyd', 1325445248, 1325171991, '70b17e51d561f1bc015e6cc4b07b3e0ce4b5d249.JPG', 'FBC', './Uploads/images//20120102/70b17e51d561f1bc015e6cc4b07b3e0ce4b5d249.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears quarterback Robert Griffin III (10) during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_70b17e51d561f1bc015e6cc4b07b3e0ce4b5d249.JPG', './Uploads/thumb//20120102/s_70b17e51d561f1bc015e6cc4b07b3e0ce4b5d249.JPG', 3500, 2450, 2369, 'John Albright / Icon SMI'),
(186, 138, 'xieyd', 1325445250, 1325171994, '9e5f89d8ce34b31ddf140a0abb3700932b2250e5.JPG', 'FBC', './Uploads/images//20120102/9e5f89d8ce34b31ddf140a0abb3700932b2250e5.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears wide receiver Kendall Wright (1) dives for extra yards during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_9e5f89d8ce34b31ddf140a0abb3700932b2250e5.JPG', './Uploads/thumb//20120102/s_9e5f89d8ce34b31ddf140a0abb3700932b2250e5.JPG', 3500, 2450, 1945, 'John Albright / Icon SMI'),
(187, 138, 'xieyd', 1325445253, 1325172013, 'f6538d7be62fcca9244002c23aafe1569afeec13.JPG', 'FBC', './Uploads/images//20120102/f6538d7be62fcca9244002c23aafe1569afeec13.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears running back Terrance Ganaway (24) during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_f6538d7be62fcca9244002c23aafe1569afeec13.JPG', './Uploads/thumb//20120102/s_f6538d7be62fcca9244002c23aafe1569afeec13.JPG', 2450, 3500, 2137, 'John Albright / Icon SMI'),
(188, 138, 'xieyd', 1325445256, 1325172015, '222a8a60b3da791ef2a457a0d6ea651a1cdba5a7.JPG', 'FBC', './Uploads/images//20120102/222a8a60b3da791ef2a457a0d6ea651a1cdba5a7.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears running back Terrance Ganaway (24) is tackled by Washington Huskies defensive end Josh Shirley (22) and Washington Huskies safety Nate Fellner (29) during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies', 100, 0, './Uploads/thumb//20120102/m_222a8a60b3da791ef2a457a0d6ea651a1cdba5a7.JPG', './Uploads/thumb//20120102/s_222a8a60b3da791ef2a457a0d6ea651a1cdba5a7.JPG', 3500, 2449, 2208, 'John Albright / Icon SMI'),
(189, 138, 'xieyd', 1325445259, 1325172015, '42afd4543bddd0d2f238a857c04f5d2399150e58.JPG', 'FBC', './Uploads/images//20120102/42afd4543bddd0d2f238a857c04f5d2399150e58.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: Baylor Bears running back Terrance Ganaway (24) is tackled by Washington Huskies defensive end Josh Shirley (22) and Washington Huskies safety Nate Fellner (29) during the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies', 100, 0, './Uploads/thumb//20120102/m_42afd4543bddd0d2f238a857c04f5d2399150e58.JPG', './Uploads/thumb//20120102/s_42afd4543bddd0d2f238a857c04f5d2399150e58.JPG', 3500, 2449, 2271, 'John Albright / Icon SMI'),
(190, 138, 'xieyd', 1325445261, 1325173203, '779919f42d06ed1cedebb5da883a95d180ec8746.JPG', 'FBC', './Uploads/images//20120102/779919f42d06ed1cedebb5da883a95d180ec8746.JPG', '', '', 'USA', 'Texas', 'San Antonio', 'Dec 29 2011: \r\nduring the 2011 Valero Alamo Bowl between the Baylor Bears and Washington Huskies at the Alamodome in San Antonio Texas. \r\n Photo via Newscom', 100, 1, './Uploads/thumb//20120102/m_779919f42d06ed1cedebb5da883a95d180ec8746.JPG', './Uploads/thumb//20120102/s_779919f42d06ed1cedebb5da883a95d180ec8746.JPG', 3500, 2450, 3162, 'John Albright / Icon SMI'),
(191, 139, 'xieyd', 1325445920, 1325248718, 'e0a0306c44c4cadea13dad6a4403f23fd84ad5a8.JPG', 'HKC', './Uploads/images//20120102/e0a0306c44c4cadea13dad6a4403f23fd84ad5a8.JPG', '', '', 'USA', 'MN', 'St. Cloud', '30 December 2011: St. Cloud State defenseman Nick Jensen (14) against the Western Michigan Broncos at the National Hockey Center in St. Cloud, MN. Western Michigan defeated St. Cloud State 2-1 in overtime. Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_e0a0306c44c4cadea13dad6a4403f23fd84ad5a8.JPG', './Uploads/thumb//20120102/s_e0a0306c44c4cadea13dad6a4403f23fd84ad5a8.JPG', 2961, 1974, 2387, 'Brace Hemmelgarn/Icon SMI'),
(192, 139, 'xieyd', 1325445922, 1325248872, '0cb40958f76b672df47d6cd12dd5f750c806548f.JPG', 'HKC', './Uploads/images//20120102/0cb40958f76b672df47d6cd12dd5f750c806548f.JPG', '', '', 'USA', 'MN', 'St. Cloud', '30 December 2011: Western Michigan defenseman Matt Tennyson (7) hits St. Cloud State forward Brooks Bertsch (21) at the National Hockey Center in St. Cloud, MN. Western Michigan defeated St. Cloud State 2-1 in overtime. Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_0cb40958f76b672df47d6cd12dd5f750c806548f.JPG', './Uploads/thumb//20120102/s_0cb40958f76b672df47d6cd12dd5f750c806548f.JPG', 1462, 2193, 1547, 'Brace Hemmelgarn/Icon SMI'),
(193, 139, 'xieyd', 1325445925, 1325253708, 'b6960911c9443bf7e9bd9bcfba9e586ea2118558.JPG', 'HKC', './Uploads/images//20120102/b6960911c9443bf7e9bd9bcfba9e586ea2118558.JPG', '', '', 'USA', 'MN', 'St. Cloud', '30 December 2011: Western Michigan defenseman Danny DeKeyser (5) against the St. Cloud State Huskies at the National Hockey Center in St. Cloud, MN. Western Michigan defeated St. Cloud State 2-1 in overtime. Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_b6960911c9443bf7e9bd9bcfba9e586ea2118558.JPG', './Uploads/thumb//20120102/s_b6960911c9443bf7e9bd9bcfba9e586ea2118558.JPG', 3308, 2205, 3279, 'Brace Hemmelgarn/Icon SMI'),
(194, 139, 'xieyd', 1325445926, 1325251810, 'e420b54969ec1d79ebf5617630bb20511f44c97c.JPG', 'HKC', './Uploads/images//20120102/e420b54969ec1d79ebf5617630bb20511f44c97c.JPG', '', '', 'USA', 'MN', 'St. Cloud', '30 December 2011: Western Michigan defenseman Danny DeKeyser (5) against the St. Cloud State Huskies at the National Hockey Center in St. Cloud, MN. Western Michigan defeated St. Cloud State 2-1 in overtime. Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_e420b54969ec1d79ebf5617630bb20511f44c97c.JPG', './Uploads/thumb//20120102/s_e420b54969ec1d79ebf5617630bb20511f44c97c.JPG', 2562, 1708, 1480, 'Brace Hemmelgarn/Icon SMI'),
(195, 139, 'xieyd', 1325445928, 1325248175, '493314875503f91519d78b69eacf7c459d39b293.JPG', 'HKC', './Uploads/images//20120102/493314875503f91519d78b69eacf7c459d39b293.JPG', '', '', 'USA', 'MN', 'St. Cloud', '30 December 2011: Western Michigan defenseman Danny DeKeyser (5) against the St. Cloud State Huskies at the National Hockey Center in St. Cloud, MN. Western Michigan defeated St. Cloud State 2-1 in overtime. Photo via Newscom', 100, 0, './Uploads/thumb//20120102/m_493314875503f91519d78b69eacf7c459d39b293.JPG', './Uploads/thumb//20120102/s_493314875503f91519d78b69eacf7c459d39b293.JPG', 3380, 2253, 2099, 'Brace Hemmelgarn/Icon SMI'),
(196, 140, 'selectersky', 1325446943, 1319373001, 'f72c3f09432543fe869320e2ebfc06558f8cba38.JPG', '', './Uploads/images//20120102/f72c3f09432543fe869320e2ebfc06558f8cba38.JPG', '时效图片', '运河燃放点的烟花', '', '', '', 'utf-8', 0, 0, './Uploads/thumb//20120102/m_f72c3f09432543fe869320e2ebfc06558f8cba38.JPG', './Uploads/thumb//20120102/s_f72c3f09432543fe869320e2ebfc06558f8cba38.JPG', 4256, 2832, 1711, 'Xie Youding'),
(197, 140, 'selectersky', 1325446945, 1319373003, 'a436f3d1f5198f4a0b83594fade32d9e484bddb4.JPG', '', './Uploads/images//20120102/a436f3d1f5198f4a0b83594fade32d9e484bddb4.JPG', '时效图片', '运河燃放点的烟花', '', '', '', 'utf-8', 0, 0, './Uploads/thumb//20120102/m_a436f3d1f5198f4a0b83594fade32d9e484bddb4.JPG', './Uploads/thumb//20120102/s_a436f3d1f5198f4a0b83594fade32d9e484bddb4.JPG', 4256, 2832, 1843, 'Xie Youding'),
(198, 140, 'selectersky', 1325446948, 1319373005, '8b11be9603de3f855d85d5d296dc7f9896223dc8.JPG', '', './Uploads/images//20120102/8b11be9603de3f855d85d5d296dc7f9896223dc8.JPG', '时效图片', '运河燃放点的烟花', '', '', '', 'utf-8', 0, 0, './Uploads/thumb//20120102/m_8b11be9603de3f855d85d5d296dc7f9896223dc8.JPG', './Uploads/thumb//20120102/s_8b11be9603de3f855d85d5d296dc7f9896223dc8.JPG', 4256, 2832, 2159, 'Xie Youding'),
(199, 140, 'selectersky', 1325446950, 1319373009, 'a08f6e142598af716a1b5f564db6cddab5e23237.JPG', '', './Uploads/images//20120102/a08f6e142598af716a1b5f564db6cddab5e23237.JPG', '时效图片', '运河燃放点的烟花', '', '', '', 'utf-8', 0, 0, './Uploads/thumb//20120102/m_a08f6e142598af716a1b5f564db6cddab5e23237.JPG', './Uploads/thumb//20120102/s_a08f6e142598af716a1b5f564db6cddab5e23237.JPG', 4256, 2832, 1798, 'Xie Youding'),
(200, 141, 'selectersky', 1325447006, 1319373005, 'f01b6c130dd564f2be3d498e92bdbfc49078c8db.JPG', '', './Uploads/images//20120102/f01b6c130dd564f2be3d498e92bdbfc49078c8db.JPG', '时效图片', '运河燃放点的烟花', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_f01b6c130dd564f2be3d498e92bdbfc49078c8db.JPG', './Uploads/thumb//20120102/s_f01b6c130dd564f2be3d498e92bdbfc49078c8db.JPG', 4256, 2832, 2159, 'Xie Youding'),
(201, 141, 'selectersky', 1325447008, 1319373009, 'a18c30f221d25051bed34b6527eccd44f4df9b43.JPG', '', './Uploads/images//20120102/a18c30f221d25051bed34b6527eccd44f4df9b43.JPG', '时效图片', '运河燃放点的烟花', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_a18c30f221d25051bed34b6527eccd44f4df9b43.JPG', './Uploads/thumb//20120102/s_a18c30f221d25051bed34b6527eccd44f4df9b43.JPG', 4256, 2832, 1798, 'Xie Youding'),
(202, 142, 'selectersky', 1325511204, 1325511204, '76d78faba3cbfb14d0e0d343811ec40331295653.jpg', '', './Uploads/images//20120102/76d78faba3cbfb14d0e0d343811ec40331295653.jpg', '时效图片', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_76d78faba3cbfb14d0e0d343811ec40331295653.jpg', './Uploads/thumb//20120102/s_76d78faba3cbfb14d0e0d343811ec40331295653.jpg', 596, 375, 53, ''),
(203, 143, 'selectersky', 1325511261, 1325511261, 'e490e475b40f1921d9d690621da8210d1ebb011b.jpg', '', './Uploads/images//20120102/e490e475b40f1921d9d690621da8210d1ebb011b.jpg', '时效图片', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_e490e475b40f1921d9d690621da8210d1ebb011b.jpg', './Uploads/thumb//20120102/s_e490e475b40f1921d9d690621da8210d1ebb011b.jpg', 596, 375, 53, ''),
(204, 144, 'selectersky', 1325447529, 1325447529, '0ff825ef62909a7d7324bc06ede56ba127b0fdde.jpg', '', './Uploads/images//20120102/0ff825ef62909a7d7324bc06ede56ba127b0fdde.jpg', '时效图片', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_0ff825ef62909a7d7324bc06ede56ba127b0fdde.jpg', './Uploads/thumb//20120102/s_0ff825ef62909a7d7324bc06ede56ba127b0fdde.jpg', 596, 375, 53, ''),
(205, 0, 'selectersky', 1325447552, 0, '495fb9a88e3673ab65927c23f5e81b1dd51dd15c.jpg', '', './Uploads/images//20120102/495fb9a88e3673ab65927c23f5e81b1dd51dd15c.jpg', '时效图片', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_495fb9a88e3673ab65927c23f5e81b1dd51dd15c.jpg', './Uploads/thumb//20120102/s_495fb9a88e3673ab65927c23f5e81b1dd51dd15c.jpg', 596, 375, 53, ''),
(206, 146, 'selectersky', 1325511422, 1325511422, '883c87a58821c7173e969f67a389b4419459d31e.jpg', '', './Uploads/images//20120102/883c87a58821c7173e969f67a389b4419459d31e.jpg', '时效图片', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_883c87a58821c7173e969f67a389b4419459d31e.jpg', './Uploads/thumb//20120102/s_883c87a58821c7173e969f67a389b4419459d31e.jpg', 596, 375, 53, ''),
(207, 147, 'selectersky', 1325511519, 1319373001, 'bafddfec3cfe240c92b62f58b9c95fb46f87a27c.JPG', '', './Uploads/images//20120102/bafddfec3cfe240c92b62f58b9c95fb46f87a27c.JPG', '时效图片', '运河燃放点的烟花', '', '', '', '杭州西博烟花大会', 0, 0, './Uploads/thumb//20120102/m_bafddfec3cfe240c92b62f58b9c95fb46f87a27c.JPG', './Uploads/thumb//20120102/s_bafddfec3cfe240c92b62f58b9c95fb46f87a27c.JPG', 4256, 2832, 1711, 'Xie Youding');
INSERT INTO `pc_upload_images_detail` (`id`, `group_id`, `account`, `createtime`, `photo_time`, `filename`, `cate`, `url`, `type`, `title`, `country`, `province`, `city`, `remark`, `point`, `check_state`, `big_url`, `small_url`, `width`, `height`, `filesize`, `author`) VALUES
(208, 150, 'selectersky', 1325447855, 1319373001, '463fd3cfc7de963427c4e365dc610aeb0ae97816.JPG', '', './Uploads/images//20120102/463fd3cfc7de963427c4e365dc610aeb0ae97816.JPG', '时效图片', '运河燃放点的烟花', '', '', '', '杭州西博烟花大会111', 100, 0, './Uploads/thumb//20120102/m_463fd3cfc7de963427c4e365dc610aeb0ae97816.JPG', './Uploads/thumb//20120102/s_463fd3cfc7de963427c4e365dc610aeb0ae97816.JPG', 4256, 2832, 1711, 'Xie Youding'),
(209, 151, 'selectersky', 1325449387, 1319373005, '6b9c7feca230010b27171aa1cf2d3e401875f440.JPG', '', './Uploads/images//20120102/6b9c7feca230010b27171aa1cf2d3e401875f440.JPG', '时效图片', '运河燃放点的烟花', '', '', '', '杭州西博烟花大会111', 100, 0, './Uploads/thumb//20120102/m_6b9c7feca230010b27171aa1cf2d3e401875f440.JPG', './Uploads/thumb//20120102/s_6b9c7feca230010b27171aa1cf2d3e401875f440.JPG', 4256, 2832, 2159, 'Xie Youding'),
(210, 151, 'selectersky', 1325449389, 1319373009, 'e54a006b1bc5a700a56c071bbf2cb0ca7e8840fe.JPG', '', './Uploads/images//20120102/e54a006b1bc5a700a56c071bbf2cb0ca7e8840fe.JPG', '时效图片', '运河燃放点的烟花', '', '', '', '杭州西博烟花大会22222', 100, 0, './Uploads/thumb//20120102/m_e54a006b1bc5a700a56c071bbf2cb0ca7e8840fe.JPG', './Uploads/thumb//20120102/s_e54a006b1bc5a700a56c071bbf2cb0ca7e8840fe.JPG', 4256, 2832, 1798, 'Xie Youding'),
(211, 152, 'selectersky', 1325449601, 1319373005, 'd8a716a3df76c625c346bf57df43b8440147d18d.JPG', '', './Uploads/images//20120102/d8a716a3df76c625c346bf57df43b8440147d18d.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会1', 100, 0, './Uploads/thumb//20120102/m_d8a716a3df76c625c346bf57df43b8440147d18d.JPG', './Uploads/thumb//20120102/s_d8a716a3df76c625c346bf57df43b8440147d18d.JPG', 4256, 2832, 2159, 'Xie Youding'),
(212, 152, 'selectersky', 1325449604, 1319373009, 'bb0741760235e83c4014e77d6420b5a276d0da3a.JPG', '', './Uploads/images//20120102/bb0741760235e83c4014e77d6420b5a276d0da3a.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会2', 100, 0, './Uploads/thumb//20120102/m_bb0741760235e83c4014e77d6420b5a276d0da3a.JPG', './Uploads/thumb//20120102/s_bb0741760235e83c4014e77d6420b5a276d0da3a.JPG', 4256, 2832, 1798, 'Xie Youding'),
(213, 153, 'selectersky', 1325449847, 1319373005, 'e723328213a6c7b288ccc616843888582aa6ade6.JPG', '', './Uploads/images//20120102/e723328213a6c7b288ccc616843888582aa6ade6.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会1', 100, 0, './Uploads/thumb//20120102/m_e723328213a6c7b288ccc616843888582aa6ade6.JPG', './Uploads/thumb//20120102/s_e723328213a6c7b288ccc616843888582aa6ade6.JPG', 4256, 2832, 2159, 'Xie Youding'),
(214, 153, 'selectersky', 1325449849, 1319373009, '36c65f5ef0a0eab61994bca69fce0c24df77fe27.JPG', '', './Uploads/images//20120102/36c65f5ef0a0eab61994bca69fce0c24df77fe27.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会2', 100, 0, './Uploads/thumb//20120102/m_36c65f5ef0a0eab61994bca69fce0c24df77fe27.JPG', './Uploads/thumb//20120102/s_36c65f5ef0a0eab61994bca69fce0c24df77fe27.JPG', 4256, 2832, 1798, 'Xie Youding'),
(215, 154, 'selectersky', 1325449980, 1319373003, 'bcbfe521f2633b8c7966184e58517a0f3f6b458f.JPG', '', './Uploads/images//20120102/bcbfe521f2633b8c7966184e58517a0f3f6b458f.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会3', 100, 0, './Uploads/thumb//20120102/m_bcbfe521f2633b8c7966184e58517a0f3f6b458f.JPG', './Uploads/thumb//20120102/s_bcbfe521f2633b8c7966184e58517a0f3f6b458f.JPG', 4256, 2832, 1843, 'Xie Youding'),
(216, 154, 'selectersky', 1325449983, 1319373005, '2d71fdfcb94b3893752a2a74f137ec33269b2b9f.JPG', '', './Uploads/images//20120102/2d71fdfcb94b3893752a2a74f137ec33269b2b9f.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会4', 100, 0, './Uploads/thumb//20120102/m_2d71fdfcb94b3893752a2a74f137ec33269b2b9f.JPG', './Uploads/thumb//20120102/s_2d71fdfcb94b3893752a2a74f137ec33269b2b9f.JPG', 4256, 2832, 2159, 'Xie Youding'),
(217, 156, 'selectersky', 1325514030, 1319373005, '886a1c99773bfc30bcd9d6d24bb6b76048032caa.JPG', '', './Uploads/images//20120102/886a1c99773bfc30bcd9d6d24bb6b76048032caa.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_886a1c99773bfc30bcd9d6d24bb6b76048032caa.JPG', './Uploads/thumb//20120102/s_886a1c99773bfc30bcd9d6d24bb6b76048032caa.JPG', 4256, 2832, 2159, 'Xie Youding'),
(218, 156, 'selectersky', 1325514039, 1319373009, '5ebcdda94bbb50544fcf8d90bcc0f07928b63e10.JPG', '', './Uploads/images//20120102/5ebcdda94bbb50544fcf8d90bcc0f07928b63e10.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_5ebcdda94bbb50544fcf8d90bcc0f07928b63e10.JPG', './Uploads/thumb//20120102/s_5ebcdda94bbb50544fcf8d90bcc0f07928b63e10.JPG', 4256, 2832, 1798, 'Xie Youding'),
(219, 157, 'selectersky', 1325451439, 1319373005, '2866eee0963197bc1027d3251de34d3ce099d8ef.JPG', '', './Uploads/images//20120102/2866eee0963197bc1027d3251de34d3ce099d8ef.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会11', 100, 0, './Uploads/thumb//20120102/m_2866eee0963197bc1027d3251de34d3ce099d8ef.JPG', './Uploads/thumb//20120102/s_2866eee0963197bc1027d3251de34d3ce099d8ef.JPG', 4256, 2832, 2159, 'Xie Youding'),
(220, 157, 'selectersky', 1325451441, 1319373009, '289da3c474af4d7122ba8a7ff7fe11e34b3505ce.JPG', '', './Uploads/images//20120102/289da3c474af4d7122ba8a7ff7fe11e34b3505ce.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会22', 100, 0, './Uploads/thumb//20120102/m_289da3c474af4d7122ba8a7ff7fe11e34b3505ce.JPG', './Uploads/thumb//20120102/s_289da3c474af4d7122ba8a7ff7fe11e34b3505ce.JPG', 4256, 2832, 1798, 'Xie Youding'),
(221, 158, 'xieyd', 1325451492, 1324360469, '6617c44ab968d1f0e7add04f8905218082c0129e.jpg', '', './Uploads/images//20120102/6617c44ab968d1f0e7add04f8905218082c0129e.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_6617c44ab968d1f0e7add04f8905218082c0129e.jpg', './Uploads/thumb//20120102/s_6617c44ab968d1f0e7add04f8905218082c0129e.jpg', 2787, 2182, 941, 'Dennis Van Tine'),
(222, 158, 'xieyd', 1325451493, 1324360336, 'b372073fe8c5b4102bc476beac331db14492b18d.jpg', '', './Uploads/images//20120102/b372073fe8c5b4102bc476beac331db14492b18d.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_b372073fe8c5b4102bc476beac331db14492b18d.jpg', './Uploads/thumb//20120102/s_b372073fe8c5b4102bc476beac331db14492b18d.jpg', 1804, 2378, 651, 'Dennis Van Tine'),
(223, 158, 'xieyd', 1325451495, 1324359119, 'd78cf9add06ab367f23cf2bb2c2f3bc5cebffd79.jpg', '', './Uploads/images//20120102/d78cf9add06ab367f23cf2bb2c2f3bc5cebffd79.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_d78cf9add06ab367f23cf2bb2c2f3bc5cebffd79.jpg', './Uploads/thumb//20120102/s_d78cf9add06ab367f23cf2bb2c2f3bc5cebffd79.jpg', 2046, 3262, 717, 'Dennis Van Tine'),
(224, 158, 'xieyd', 1325451497, 1324358922, 'c7f0102367471cbd460e431671b28c61b38d3768.jpg', '', './Uploads/images//20120102/c7f0102367471cbd460e431671b28c61b38d3768.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_c7f0102367471cbd460e431671b28c61b38d3768.jpg', './Uploads/thumb//20120102/s_c7f0102367471cbd460e431671b28c61b38d3768.jpg', 1664, 2498, 541, 'Dennis Van Tine'),
(225, 158, 'xieyd', 1325451498, 1324360495, '8258425257c2088c0ff73519b80aa8391223b732.jpg', '', './Uploads/images//20120102/8258425257c2088c0ff73519b80aa8391223b732.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_8258425257c2088c0ff73519b80aa8391223b732.jpg', './Uploads/thumb//20120102/s_8258425257c2088c0ff73519b80aa8391223b732.jpg', 2232, 1559, 341, 'Dennis Van Tine'),
(226, 158, 'xieyd', 1325451500, 1324359915, 'ca6f0cd71bda01f7a50ebefbe7c8bf29785f3f15.jpg', '', './Uploads/images//20120102/ca6f0cd71bda01f7a50ebefbe7c8bf29785f3f15.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_ca6f0cd71bda01f7a50ebefbe7c8bf29785f3f15.jpg', './Uploads/thumb//20120102/s_ca6f0cd71bda01f7a50ebefbe7c8bf29785f3f15.jpg', 2167, 3280, 711, 'Dennis Van Tine'),
(227, 158, 'xieyd', 1325451501, 1324360730, '6b33c5ca7e609a5c889de6b420286e7dc6fd3578.jpg', '', './Uploads/images//20120102/6b33c5ca7e609a5c889de6b420286e7dc6fd3578.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_6b33c5ca7e609a5c889de6b420286e7dc6fd3578.jpg', './Uploads/thumb//20120102/s_6b33c5ca7e609a5c889de6b420286e7dc6fd3578.jpg', 2764, 1859, 376, 'Dennis Van Tine'),
(228, 158, 'xieyd', 1325451502, 1324360633, '0078fd589dbd07dc9e897719f2ff23378fb1e5f3.jpg', '', './Uploads/images//20120102/0078fd589dbd07dc9e897719f2ff23378fb1e5f3.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_0078fd589dbd07dc9e897719f2ff23378fb1e5f3.jpg', './Uploads/thumb//20120102/s_0078fd589dbd07dc9e897719f2ff23378fb1e5f3.jpg', 1988, 1407, 338, 'Dennis Van Tine'),
(229, 158, 'xieyd', 1325451504, 1324360730, 'd0e0cdb9db740888608c89e5b2b6726bae5b2fa7.jpg', '', './Uploads/images//20120102/d0e0cdb9db740888608c89e5b2b6726bae5b2fa7.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_d0e0cdb9db740888608c89e5b2b6726bae5b2fa7.jpg', './Uploads/thumb//20120102/s_d0e0cdb9db740888608c89e5b2b6726bae5b2fa7.jpg', 3039, 2179, 487, 'Dennis Van Tine'),
(230, 158, 'xieyd', 1325451506, 1324358956, 'afc516ca926e5f7bf9043d37b9e71e50ab8f74a2.jpg', '', './Uploads/images//20120102/afc516ca926e5f7bf9043d37b9e71e50ab8f74a2.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_afc516ca926e5f7bf9043d37b9e71e50ab8f74a2.jpg', './Uploads/thumb//20120102/s_afc516ca926e5f7bf9043d37b9e71e50ab8f74a2.jpg', 2871, 2210, 585, 'Dennis Van Tine'),
(231, 158, 'xieyd', 1325451507, 1324359095, '46654a80dae57faa299d552db4572cd09544043f.jpg', '', './Uploads/images//20120102/46654a80dae57faa299d552db4572cd09544043f.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_46654a80dae57faa299d552db4572cd09544043f.jpg', './Uploads/thumb//20120102/s_46654a80dae57faa299d552db4572cd09544043f.jpg', 1156, 1541, 332, 'Dennis Van Tine'),
(232, 158, 'xieyd', 1325451508, 1324359915, '6b791d0000f53586c50c40e1e4a07591ea40526c.jpg', '', './Uploads/images//20120102/6b791d0000f53586c50c40e1e4a07591ea40526c.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_6b791d0000f53586c50c40e1e4a07591ea40526c.jpg', './Uploads/thumb//20120102/s_6b791d0000f53586c50c40e1e4a07591ea40526c.jpg', 2202, 3058, 647, 'Dennis Van Tine'),
(233, 158, 'xieyd', 1325451510, 1324358858, 'be212e6d7bb0a32ccfe443a363f20c5054cac52a.jpg', '', './Uploads/images//20120102/be212e6d7bb0a32ccfe443a363f20c5054cac52a.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_be212e6d7bb0a32ccfe443a363f20c5054cac52a.jpg', './Uploads/thumb//20120102/s_be212e6d7bb0a32ccfe443a363f20c5054cac52a.jpg', 2931, 2075, 507, 'Dennis Van Tine'),
(234, 158, 'xieyd', 1325451512, 1324359132, 'fd6ec568cfa3ab13a44945c45bb343c4a14a77c0.jpg', '', './Uploads/images//20120102/fd6ec568cfa3ab13a44945c45bb343c4a14a77c0.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_fd6ec568cfa3ab13a44945c45bb343c4a14a77c0.jpg', './Uploads/thumb//20120102/s_fd6ec568cfa3ab13a44945c45bb343c4a14a77c0.jpg', 2310, 3544, 802, 'Dennis Van Tine'),
(235, 158, 'xieyd', 1325451514, 1324359296, 'e86ab6704976cd1fcc3732ad489e6d6a4770b228.jpg', '', './Uploads/images//20120102/e86ab6704976cd1fcc3732ad489e6d6a4770b228.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_e86ab6704976cd1fcc3732ad489e6d6a4770b228.jpg', './Uploads/thumb//20120102/s_e86ab6704976cd1fcc3732ad489e6d6a4770b228.jpg', 2020, 2434, 432, 'Dennis Van Tine'),
(236, 158, 'xieyd', 1325451515, 1324360950, '06813715f2cbefcc575c4db2e6d32276487b96be.jpg', '', './Uploads/images//20120102/06813715f2cbefcc575c4db2e6d32276487b96be.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_06813715f2cbefcc575c4db2e6d32276487b96be.jpg', './Uploads/thumb//20120102/s_06813715f2cbefcc575c4db2e6d32276487b96be.jpg', 1880, 2126, 485, 'Dennis Van Tine'),
(237, 158, 'xieyd', 1325451516, 1324359083, 'd344ae6537135c26f15b8cfc64f1aaf929c72ee1.jpg', '', './Uploads/images//20120102/d344ae6537135c26f15b8cfc64f1aaf929c72ee1.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_d344ae6537135c26f15b8cfc64f1aaf929c72ee1.jpg', './Uploads/thumb//20120102/s_d344ae6537135c26f15b8cfc64f1aaf929c72ee1.jpg', 1456, 2362, 319, 'Dennis Van Tine'),
(238, 158, 'xieyd', 1325451517, 1324360480, 'ef9f7bde1df24f1c957dfb61b8db8197d32fc8ed.jpg', '', './Uploads/images//20120102/ef9f7bde1df24f1c957dfb61b8db8197d32fc8ed.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_ef9f7bde1df24f1c957dfb61b8db8197d32fc8ed.jpg', './Uploads/thumb//20120102/s_ef9f7bde1df24f1c957dfb61b8db8197d32fc8ed.jpg', 1388, 1854, 335, 'Dennis Van Tine'),
(239, 160, 'xieyd', 1325451598, 1324360469, '2118be77004ae3bbfc7fc1d210c1f584afbacdf0.jpg', '', './Uploads/images//20120102/2118be77004ae3bbfc7fc1d210c1f584afbacdf0.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_2118be77004ae3bbfc7fc1d210c1f584afbacdf0.jpg', './Uploads/thumb//20120102/s_2118be77004ae3bbfc7fc1d210c1f584afbacdf0.jpg', 2787, 2182, 941, 'Dennis Van Tine'),
(240, 160, 'xieyd', 1325451600, 1324360336, '37009961e37cd0d1ad54d8953f655f937eb2c6d9.jpg', '', './Uploads/images//20120102/37009961e37cd0d1ad54d8953f655f937eb2c6d9.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_37009961e37cd0d1ad54d8953f655f937eb2c6d9.jpg', './Uploads/thumb//20120102/s_37009961e37cd0d1ad54d8953f655f937eb2c6d9.jpg', 1804, 2378, 651, 'Dennis Van Tine'),
(241, 160, 'xieyd', 1325451602, 1324359119, '66e4381f763cb59ff11ba98918cc788f3f9d1616.jpg', '', './Uploads/images//20120102/66e4381f763cb59ff11ba98918cc788f3f9d1616.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_66e4381f763cb59ff11ba98918cc788f3f9d1616.jpg', './Uploads/thumb//20120102/s_66e4381f763cb59ff11ba98918cc788f3f9d1616.jpg', 2046, 3262, 717, 'Dennis Van Tine'),
(242, 160, 'xieyd', 1325451603, 1324358922, 'a3a59aea79090753ff8b6ed862d8197ea8265267.jpg', '', './Uploads/images//20120102/a3a59aea79090753ff8b6ed862d8197ea8265267.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_a3a59aea79090753ff8b6ed862d8197ea8265267.jpg', './Uploads/thumb//20120102/s_a3a59aea79090753ff8b6ed862d8197ea8265267.jpg', 1664, 2498, 541, 'Dennis Van Tine'),
(243, 160, 'xieyd', 1325451604, 1324360495, 'e605cfeb7e4aed2acdffc7d83bd7692efbaf6f56.jpg', '', './Uploads/images//20120102/e605cfeb7e4aed2acdffc7d83bd7692efbaf6f56.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_e605cfeb7e4aed2acdffc7d83bd7692efbaf6f56.jpg', './Uploads/thumb//20120102/s_e605cfeb7e4aed2acdffc7d83bd7692efbaf6f56.jpg', 2232, 1559, 341, 'Dennis Van Tine'),
(244, 160, 'xieyd', 1325451606, 1324359915, 'f9602c0be9590953c298dc9290498958f654ff7e.jpg', '', './Uploads/images//20120102/f9602c0be9590953c298dc9290498958f654ff7e.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_f9602c0be9590953c298dc9290498958f654ff7e.jpg', './Uploads/thumb//20120102/s_f9602c0be9590953c298dc9290498958f654ff7e.jpg', 2167, 3280, 711, 'Dennis Van Tine'),
(245, 160, 'xieyd', 1325451608, 1324360730, 'fdad66e8120c55d0ae319af410f914de04e0682d.jpg', '', './Uploads/images//20120102/fdad66e8120c55d0ae319af410f914de04e0682d.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_fdad66e8120c55d0ae319af410f914de04e0682d.jpg', './Uploads/thumb//20120102/s_fdad66e8120c55d0ae319af410f914de04e0682d.jpg', 2764, 1859, 376, 'Dennis Van Tine'),
(246, 160, 'xieyd', 1325451609, 1324360633, 'c9e78f35698f966d9732c3a6a227b3d61e7815de.jpg', '', './Uploads/images//20120102/c9e78f35698f966d9732c3a6a227b3d61e7815de.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_c9e78f35698f966d9732c3a6a227b3d61e7815de.jpg', './Uploads/thumb//20120102/s_c9e78f35698f966d9732c3a6a227b3d61e7815de.jpg', 1988, 1407, 338, 'Dennis Van Tine'),
(247, 160, 'xieyd', 1325451610, 1324360730, '74bf0fac462a52f3cc68a853cb3553e6df76d9cd.jpg', '', './Uploads/images//20120102/74bf0fac462a52f3cc68a853cb3553e6df76d9cd.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_74bf0fac462a52f3cc68a853cb3553e6df76d9cd.jpg', './Uploads/thumb//20120102/s_74bf0fac462a52f3cc68a853cb3553e6df76d9cd.jpg', 3039, 2179, 487, 'Dennis Van Tine'),
(248, 160, 'xieyd', 1325451612, 1324358956, 'd9e2f9d65bda1a97098e705922438ff46fcd4198.jpg', '', './Uploads/images//20120102/d9e2f9d65bda1a97098e705922438ff46fcd4198.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_d9e2f9d65bda1a97098e705922438ff46fcd4198.jpg', './Uploads/thumb//20120102/s_d9e2f9d65bda1a97098e705922438ff46fcd4198.jpg', 2871, 2210, 585, 'Dennis Van Tine'),
(249, 160, 'xieyd', 1325451613, 1324359095, '11c797d971cf3722f8b4ef033780bc7f899e52d1.jpg', '', './Uploads/images//20120102/11c797d971cf3722f8b4ef033780bc7f899e52d1.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_11c797d971cf3722f8b4ef033780bc7f899e52d1.jpg', './Uploads/thumb//20120102/s_11c797d971cf3722f8b4ef033780bc7f899e52d1.jpg', 1156, 1541, 332, 'Dennis Van Tine'),
(250, 160, 'xieyd', 1325451615, 1324359915, '0995bf4e562c985f6a30fc092fbc1b15c8ce98cb.jpg', '', './Uploads/images//20120102/0995bf4e562c985f6a30fc092fbc1b15c8ce98cb.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_0995bf4e562c985f6a30fc092fbc1b15c8ce98cb.jpg', './Uploads/thumb//20120102/s_0995bf4e562c985f6a30fc092fbc1b15c8ce98cb.jpg', 2202, 3058, 647, 'Dennis Van Tine'),
(251, 160, 'xieyd', 1325451616, 1324358858, '2bc297f0eacf4b5dc3f3993c5cb91c6ae2c6840e.jpg', '', './Uploads/images//20120102/2bc297f0eacf4b5dc3f3993c5cb91c6ae2c6840e.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_2bc297f0eacf4b5dc3f3993c5cb91c6ae2c6840e.jpg', './Uploads/thumb//20120102/s_2bc297f0eacf4b5dc3f3993c5cb91c6ae2c6840e.jpg', 2931, 2075, 507, 'Dennis Van Tine'),
(252, 160, 'xieyd', 1325451618, 1324359132, 'f6646b189d73a3675a448fc1fe3329535e399d75.jpg', '', './Uploads/images//20120102/f6646b189d73a3675a448fc1fe3329535e399d75.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_f6646b189d73a3675a448fc1fe3329535e399d75.jpg', './Uploads/thumb//20120102/s_f6646b189d73a3675a448fc1fe3329535e399d75.jpg', 2310, 3544, 802, 'Dennis Van Tine'),
(253, 160, 'xieyd', 1325451620, 1324359296, '2158aeb1400dd49c9d3aa12fa78f18f5080fa6e2.jpg', '', './Uploads/images//20120102/2158aeb1400dd49c9d3aa12fa78f18f5080fa6e2.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_2158aeb1400dd49c9d3aa12fa78f18f5080fa6e2.jpg', './Uploads/thumb//20120102/s_2158aeb1400dd49c9d3aa12fa78f18f5080fa6e2.jpg', 2020, 2434, 432, 'Dennis Van Tine'),
(254, 160, 'xieyd', 1325451621, 1324360950, 'c189d3f5cf3b8cd5d402dcd41bed35e2082f1154.jpg', '', './Uploads/images//20120102/c189d3f5cf3b8cd5d402dcd41bed35e2082f1154.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_c189d3f5cf3b8cd5d402dcd41bed35e2082f1154.jpg', './Uploads/thumb//20120102/s_c189d3f5cf3b8cd5d402dcd41bed35e2082f1154.jpg', 1880, 2126, 485, 'Dennis Van Tine'),
(255, 160, 'xieyd', 1325451623, 1324359083, 'cbf194de0486be6dcf070fc3e8bbdf40bde24249.jpg', '', './Uploads/images//20120102/cbf194de0486be6dcf070fc3e8bbdf40bde24249.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_cbf194de0486be6dcf070fc3e8bbdf40bde24249.jpg', './Uploads/thumb//20120102/s_cbf194de0486be6dcf070fc3e8bbdf40bde24249.jpg', 1456, 2362, 319, 'Dennis Van Tine'),
(256, 160, 'xieyd', 1325451623, 1324360480, 'f7cb3e50f838745faaa66e75e774eeb55fa98f68.jpg', '', './Uploads/images//20120102/f7cb3e50f838745faaa66e75e774eeb55fa98f68.jpg', '时效图片', '', 'USA', '', 'New York', 'City', 0, 0, './Uploads/thumb//20120102/m_f7cb3e50f838745faaa66e75e774eeb55fa98f68.jpg', './Uploads/thumb//20120102/s_f7cb3e50f838745faaa66e75e774eeb55fa98f68.jpg', 1388, 1854, 335, 'Dennis Van Tine'),
(257, 161, 'xieyd', 1325451736, 1324375937, '6fd50ff76181a63fd2bc34e5c5eccdf0ee362aa5.jpg', '', './Uploads/images//20120102/6fd50ff76181a63fd2bc34e5c5eccdf0ee362aa5.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_6fd50ff76181a63fd2bc34e5c5eccdf0ee362aa5.jpg', './Uploads/thumb//20120102/s_6fd50ff76181a63fd2bc34e5c5eccdf0ee362aa5.jpg', 3008, 2000, 3863, 'Apaydin Alain'),
(258, 161, 'xieyd', 1325451738, 1324377914, 'f9b1c2d73e754354a918ed56d12f293035eed383.jpg', '', './Uploads/images//20120102/f9b1c2d73e754354a918ed56d12f293035eed383.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_f9b1c2d73e754354a918ed56d12f293035eed383.jpg', './Uploads/thumb//20120102/s_f9b1c2d73e754354a918ed56d12f293035eed383.jpg', 3008, 2000, 5168, 'Apaydin Alain'),
(259, 161, 'xieyd', 1325451740, 1324376960, '23903eccd4bc4da47e8d7d90ae4ffbc335c30d4b.jpg', '', './Uploads/images//20120102/23903eccd4bc4da47e8d7d90ae4ffbc335c30d4b.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_23903eccd4bc4da47e8d7d90ae4ffbc335c30d4b.jpg', './Uploads/thumb//20120102/s_23903eccd4bc4da47e8d7d90ae4ffbc335c30d4b.jpg', 2000, 3008, 3323, 'Apaydin Alain'),
(260, 161, 'xieyd', 1325451743, 1324375981, '0ffd42548f2170bb4ca47057ab09ac6563dace76.jpg', '', './Uploads/images//20120102/0ffd42548f2170bb4ca47057ab09ac6563dace76.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_0ffd42548f2170bb4ca47057ab09ac6563dace76.jpg', './Uploads/thumb//20120102/s_0ffd42548f2170bb4ca47057ab09ac6563dace76.jpg', 3008, 2000, 3990, 'Apaydin Alain'),
(261, 161, 'xieyd', 1325451745, 1324377111, '82190579100bbc57e04b58a527ecf4ea35ae80e0.jpg', '', './Uploads/images//20120102/82190579100bbc57e04b58a527ecf4ea35ae80e0.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_82190579100bbc57e04b58a527ecf4ea35ae80e0.jpg', './Uploads/thumb//20120102/s_82190579100bbc57e04b58a527ecf4ea35ae80e0.jpg', 3008, 2000, 4471, 'Apaydin Alain'),
(262, 161, 'xieyd', 1325451748, 1324377827, '8183c7fc5eaf7a51fddb033b8e8a5a8f7b4415ee.jpg', '', './Uploads/images//20120102/8183c7fc5eaf7a51fddb033b8e8a5a8f7b4415ee.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_8183c7fc5eaf7a51fddb033b8e8a5a8f7b4415ee.jpg', './Uploads/thumb//20120102/s_8183c7fc5eaf7a51fddb033b8e8a5a8f7b4415ee.jpg', 2000, 3008, 4283, 'Apaydin Alain'),
(263, 161, 'xieyd', 1325451750, 1324378037, '3a15fa9531f47d213973b19c2a0696e6f85a7447.jpg', '', './Uploads/images//20120102/3a15fa9531f47d213973b19c2a0696e6f85a7447.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_3a15fa9531f47d213973b19c2a0696e6f85a7447.jpg', './Uploads/thumb//20120102/s_3a15fa9531f47d213973b19c2a0696e6f85a7447.jpg', 2000, 3008, 4586, 'Apaydin Alain'),
(264, 161, 'xieyd', 1325451753, 1324377870, '23fd2470ee77fe12d901359d94cdd533daf23f28.jpg', '', './Uploads/images//20120102/23fd2470ee77fe12d901359d94cdd533daf23f28.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_23fd2470ee77fe12d901359d94cdd533daf23f28.jpg', './Uploads/thumb//20120102/s_23fd2470ee77fe12d901359d94cdd533daf23f28.jpg', 3008, 2000, 5658, 'Apaydin Alain'),
(265, 161, 'xieyd', 1325451755, 1324375967, '5c7274371362d44f24b200529df87cd2f84db31d.jpg', '', './Uploads/images//20120102/5c7274371362d44f24b200529df87cd2f84db31d.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_5c7274371362d44f24b200529df87cd2f84db31d.jpg', './Uploads/thumb//20120102/s_5c7274371362d44f24b200529df87cd2f84db31d.jpg', 2000, 3008, 4109, 'Apaydin Alain'),
(266, 161, 'xieyd', 1325451758, 1324378093, '441729bb77b17753b684ceda8e1e3428c6597a69.jpg', '', './Uploads/images//20120102/441729bb77b17753b684ceda8e1e3428c6597a69.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_441729bb77b17753b684ceda8e1e3428c6597a69.jpg', './Uploads/thumb//20120102/s_441729bb77b17753b684ceda8e1e3428c6597a69.jpg', 3008, 2000, 5087, 'Apaydin Alain'),
(267, 161, 'xieyd', 1325451760, 1324376759, '39b949827e08ae14f51493d062cfc69cb957e3c6.jpg', '', './Uploads/images//20120102/39b949827e08ae14f51493d062cfc69cb957e3c6.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_39b949827e08ae14f51493d062cfc69cb957e3c6.jpg', './Uploads/thumb//20120102/s_39b949827e08ae14f51493d062cfc69cb957e3c6.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(268, 161, 'xieyd', 1325451762, 1324376153, 'ed9994ea2f9b2826eaa28e9e960f66609b69ebb2.jpg', '', './Uploads/images//20120102/ed9994ea2f9b2826eaa28e9e960f66609b69ebb2.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_ed9994ea2f9b2826eaa28e9e960f66609b69ebb2.jpg', './Uploads/thumb//20120102/s_ed9994ea2f9b2826eaa28e9e960f66609b69ebb2.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(269, 161, 'xieyd', 1325451765, 1324376459, '720f0809c895d9f3d81826321da9b9823d615406.jpg', '', './Uploads/images//20120102/720f0809c895d9f3d81826321da9b9823d615406.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_720f0809c895d9f3d81826321da9b9823d615406.jpg', './Uploads/thumb//20120102/s_720f0809c895d9f3d81826321da9b9823d615406.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(270, 162, 'selectersky', 1325515823, 1324376459, 'c2a59a1a2ee811d1be687d3e3f5b4ebe81596c90.jpg', '', './Uploads/images//20120102/c2a59a1a2ee811d1be687d3e3f5b4ebe81596c90.jpg', '时效图片', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_c2a59a1a2ee811d1be687d3e3f5b4ebe81596c90.jpg', './Uploads/thumb//20120102/s_c2a59a1a2ee811d1be687d3e3f5b4ebe81596c90.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(271, 0, 'selectersky', 1325515897, 1324376459, '197ffa6a181695dd141784cce26ec540b9ed43cd.jpg', '', './Uploads/images//20120102/197ffa6a181695dd141784cce26ec540b9ed43cd.jpg', '时效图片', '', '', '', '', '', 0, 0, './Uploads/thumb//20120102/m_197ffa6a181695dd141784cce26ec540b9ed43cd.jpg', './Uploads/thumb//20120102/s_197ffa6a181695dd141784cce26ec540b9ed43cd.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(272, 164, 'xieyd', 1325452544, 1324375233, '5784ee2433a2c8a42d5337fb1f294467fdf68ac8.JPG', '', './Uploads/images//20120102/5784ee2433a2c8a42d5337fb1f294467fdf68ac8.JPG', '时效图片', '', 'France', '75', 'PARIS XIV', 'EVA JOLY', 100, 0, './Uploads/thumb//20120102/m_5784ee2433a2c8a42d5337fb1f294467fdf68ac8.JPG', './Uploads/thumb//20120102/s_5784ee2433a2c8a42d5337fb1f294467fdf68ac8.JPG', 1734, 2600, 805, 'MARC MENOU'),
(273, 164, 'xieyd', 1325452546, 1324375370, '7d8473068247d259ab285ed6cca9b3e502dcb423.JPG', '', './Uploads/images//20120102/7d8473068247d259ab285ed6cca9b3e502dcb423.JPG', '时效图片', '', 'France', '75', 'PARIS XIV', 'EVA JOLY', 100, 0, './Uploads/thumb//20120102/m_7d8473068247d259ab285ed6cca9b3e502dcb423.JPG', './Uploads/thumb//20120102/s_7d8473068247d259ab285ed6cca9b3e502dcb423.JPG', 1734, 2600, 739, 'MARC MENOU'),
(274, 164, 'xieyd', 1325452547, 1324375593, '5c96ffb0219aa43bd89fbd83092dd4f4bf366b8b.JPG', '', './Uploads/images//20120102/5c96ffb0219aa43bd89fbd83092dd4f4bf366b8b.JPG', '时效图片', '', 'France', '75', 'PARIS XIV', 'EVA JOLY', 100, 0, './Uploads/thumb//20120102/m_5c96ffb0219aa43bd89fbd83092dd4f4bf366b8b.JPG', './Uploads/thumb//20120102/s_5c96ffb0219aa43bd89fbd83092dd4f4bf366b8b.JPG', 2600, 1734, 823, 'MARC MENOU'),
(275, 164, 'xieyd', 1325452548, 1324376353, '659b5176f6adbc9b437df806a7884f710a08e3a3.JPG', '', './Uploads/images//20120102/659b5176f6adbc9b437df806a7884f710a08e3a3.JPG', '时效图片', '', 'France', '75', 'PARIS XIV', 'EVA JOLY', 100, 0, './Uploads/thumb//20120102/m_659b5176f6adbc9b437df806a7884f710a08e3a3.JPG', './Uploads/thumb//20120102/s_659b5176f6adbc9b437df806a7884f710a08e3a3.JPG', 2600, 1734, 708, 'MARC MENOU'),
(276, 165, 'xieyd', 1325452638, 1324358772, 'b61475243f0582b5dc7bc837db363b58dd027602.JPG', '', './Uploads/images//20120102/b61475243f0582b5dc7bc837db363b58dd027602.JPG', '时效图片', '', 'France', '', 'Paris', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_b61475243f0582b5dc7bc837db363b58dd027602.JPG', './Uploads/thumb//20120102/s_b61475243f0582b5dc7bc837db363b58dd027602.JPG', 4256, 2832, 1327, 'Christophe Morin'),
(277, 165, 'xieyd', 1325452640, 1324354793, '42d95202547a2595282da2039ad57b1797025ae9.JPG', '', './Uploads/images//20120102/42d95202547a2595282da2039ad57b1797025ae9.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_42d95202547a2595282da2039ad57b1797025ae9.JPG', './Uploads/thumb//20120102/s_42d95202547a2595282da2039ad57b1797025ae9.JPG', 3822, 2645, 1095, 'Fabrice HEBRARD'),
(278, 165, 'xieyd', 1325452642, 1324358501, 'c8613f53cd180f06c78ad1a7283f3f0dd6faf344.JPG', '', './Uploads/images//20120102/c8613f53cd180f06c78ad1a7283f3f0dd6faf344.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_c8613f53cd180f06c78ad1a7283f3f0dd6faf344.JPG', './Uploads/thumb//20120102/s_c8613f53cd180f06c78ad1a7283f3f0dd6faf344.JPG', 4200, 2440, 929, 'Fabrice HEBRARD'),
(279, 165, 'xieyd', 1325452644, 1324358410, '2435ed68409c5092952226bab1aa9ea7fb33ffcf.JPG', '', './Uploads/images//20120102/2435ed68409c5092952226bab1aa9ea7fb33ffcf.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_2435ed68409c5092952226bab1aa9ea7fb33ffcf.JPG', './Uploads/thumb//20120102/s_2435ed68409c5092952226bab1aa9ea7fb33ffcf.JPG', 3454, 2211, 855, 'Fabrice HEBRARD'),
(280, 165, 'xieyd', 1325452646, 1324358495, 'be5373e5d46e677d3d39ef3a3dd859647abcbaac.JPG', '', './Uploads/images//20120102/be5373e5d46e677d3d39ef3a3dd859647abcbaac.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_be5373e5d46e677d3d39ef3a3dd859647abcbaac.JPG', './Uploads/thumb//20120102/s_be5373e5d46e677d3d39ef3a3dd859647abcbaac.JPG', 4300, 2868, 1571, 'Fabrice HEBRARD'),
(281, 165, 'xieyd', 1325452648, 1324352488, 'e97e50b70abf1adf335fa774ebca58cd152097df.JPG', '', './Uploads/images//20120102/e97e50b70abf1adf335fa774ebca58cd152097df.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_e97e50b70abf1adf335fa774ebca58cd152097df.JPG', './Uploads/thumb//20120102/s_e97e50b70abf1adf335fa774ebca58cd152097df.JPG', 3447, 2395, 1058, 'Fabrice HEBRARD'),
(282, 165, 'xieyd', 1325452651, 1324354597, '902c622d55709951a7495d4d5cfbe19b85d8c5d3.JPG', '', './Uploads/images//20120102/902c622d55709951a7495d4d5cfbe19b85d8c5d3.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_902c622d55709951a7495d4d5cfbe19b85d8c5d3.JPG', './Uploads/thumb//20120102/s_902c622d55709951a7495d4d5cfbe19b85d8c5d3.JPG', 4300, 2432, 1252, 'Fabrice HEBRARD'),
(283, 165, 'xieyd', 1325452653, 1325452653, '32e9c07d78571b0a67d0eeb13bba119ae3967adb.JPG', '', './Uploads/images//20120102/32e9c07d78571b0a67d0eeb13bba119ae3967adb.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_32e9c07d78571b0a67d0eeb13bba119ae3967adb.JPG', './Uploads/thumb//20120102/s_32e9c07d78571b0a67d0eeb13bba119ae3967adb.JPG', 4300, 2868, 1106, 'Fabrice HEBRARD'),
(284, 165, 'xieyd', 1325452655, 1325452655, 'd11f826f5408d55871f02600bb8ac8915b7b1df6.JPG', '', './Uploads/images//20120102/d11f826f5408d55871f02600bb8ac8915b7b1df6.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_d11f826f5408d55871f02600bb8ac8915b7b1df6.JPG', './Uploads/thumb//20120102/s_d11f826f5408d55871f02600bb8ac8915b7b1df6.JPG', 4300, 2868, 1297, 'Fabrice HEBRARD'),
(285, 165, 'xieyd', 1325452658, 1324353636, '8de93c813f01d1e2317aec45537844d53d9d9716.JPG', '', './Uploads/images//20120102/8de93c813f01d1e2317aec45537844d53d9d9716.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_8de93c813f01d1e2317aec45537844d53d9d9716.JPG', './Uploads/thumb//20120102/s_8de93c813f01d1e2317aec45537844d53d9d9716.JPG', 4300, 2868, 1205, 'Fabrice HEBRARD'),
(286, 165, 'xieyd', 1325452660, 1324359571, '3a963cac0dc54d0fcce585f46151dbc8d1939014.JPG', '', './Uploads/images//20120102/3a963cac0dc54d0fcce585f46151dbc8d1939014.JPG', '时效图片', '', 'France', '', 'LES VANS', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_3a963cac0dc54d0fcce585f46151dbc8d1939014.JPG', './Uploads/thumb//20120102/s_3a963cac0dc54d0fcce585f46151dbc8d1939014.JPG', 3426, 2766, 1286, 'Fabrice HEBRARD'),
(287, 165, 'xieyd', 1325452662, 1324358058, 'c8e7fdd238dfc76cd8be2b4c3eb34def53f5ed99.JPG', '', './Uploads/images//20120102/c8e7fdd238dfc76cd8be2b4c3eb34def53f5ed99.JPG', '时效图片', '', 'France', '', 'Paris', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_c8e7fdd238dfc76cd8be2b4c3eb34def53f5ed99.JPG', './Uploads/thumb//20120102/s_c8e7fdd238dfc76cd8be2b4c3eb34def53f5ed99.JPG', 4256, 2832, 1141, 'Christophe Morin'),
(288, 165, 'xieyd', 1325452664, 1324357588, 'fdaa15d60862a6c7c35cf917be1f5a8f897780c0.JPG', '', './Uploads/images//20120102/fdaa15d60862a6c7c35cf917be1f5a8f897780c0.JPG', '时效图片', '', 'France', '', 'Paris', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_fdaa15d60862a6c7c35cf917be1f5a8f897780c0.JPG', './Uploads/thumb//20120102/s_fdaa15d60862a6c7c35cf917be1f5a8f897780c0.JPG', 2231, 3352, 515, 'Christophe Morin'),
(289, 165, 'xieyd', 1325452666, 1324357501, '3d0cea08564a27b18623b7bcd41b33e2ac52d0c2.JPG', '', './Uploads/images//20120102/3d0cea08564a27b18623b7bcd41b33e2ac52d0c2.JPG', '时效图片', '', 'France', '', 'Paris', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_3d0cea08564a27b18623b7bcd41b33e2ac52d0c2.JPG', './Uploads/thumb//20120102/s_3d0cea08564a27b18623b7bcd41b33e2ac52d0c2.JPG', 4256, 2832, 1468, 'Christophe Morin'),
(290, 165, 'xieyd', 1325452669, 1324358774, '5effaf0ecd603f09dbe913aaeb27f8a989b903f0.JPG', '', './Uploads/images//20120102/5effaf0ecd603f09dbe913aaeb27f8a989b903f0.JPG', '时效图片', '', 'France', '', 'Paris', 'VISITE PRESIDENTIELLE AUX VANS', 100, 0, './Uploads/thumb//20120102/m_5effaf0ecd603f09dbe913aaeb27f8a989b903f0.JPG', './Uploads/thumb//20120102/s_5effaf0ecd603f09dbe913aaeb27f8a989b903f0.JPG', 4256, 2832, 1368, 'Christophe Morin'),
(291, 166, 'selectersky', 1325516661, 1319373003, '33220fb74fb56ef434088b3224774e629effda70.JPG', '', './Uploads/images//20120102/33220fb74fb56ef434088b3224774e629effda70.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 0, 0, './Uploads/thumb//20120102/m_33220fb74fb56ef434088b3224774e629effda70.JPG', './Uploads/thumb//20120102/s_33220fb74fb56ef434088b3224774e629effda70.JPG', 4256, 2832, 1843, 'Xie Youding'),
(292, 166, 'selectersky', 1325516670, 1319373005, '044f54cc63261e9a4132808a0609763b0c719c32.JPG', '', './Uploads/images//20120102/044f54cc63261e9a4132808a0609763b0c719c32.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 0, 0, './Uploads/thumb//20120102/m_044f54cc63261e9a4132808a0609763b0c719c32.JPG', './Uploads/thumb//20120102/s_044f54cc63261e9a4132808a0609763b0c719c32.JPG', 4256, 2832, 2159, 'Xie Youding'),
(293, 166, 'selectersky', 1325516679, 1319373009, '000b165202588c0ba2644df87a834e721d9c78cd.JPG', '', './Uploads/images//20120102/000b165202588c0ba2644df87a834e721d9c78cd.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 0, 0, './Uploads/thumb//20120102/m_000b165202588c0ba2644df87a834e721d9c78cd.JPG', './Uploads/thumb//20120102/s_000b165202588c0ba2644df87a834e721d9c78cd.JPG', 4256, 2832, 1798, 'Xie Youding'),
(294, 167, 'selectersky', 1325516958, 1319373005, '6699669df0329a8fdaf281f38b961905c405fd89.JPG', '', './Uploads/images//20120102/6699669df0329a8fdaf281f38b961905c405fd89.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_6699669df0329a8fdaf281f38b961905c405fd89.JPG', './Uploads/thumb//20120102/s_6699669df0329a8fdaf281f38b961905c405fd89.JPG', 4256, 2832, 2159, 'Xie Youding'),
(295, 167, 'selectersky', 1325516967, 1319373009, '1e72d25a6aa567da5a9c10637276f08a0c6591d4.JPG', '', './Uploads/images//20120102/1e72d25a6aa567da5a9c10637276f08a0c6591d4.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_1e72d25a6aa567da5a9c10637276f08a0c6591d4.JPG', './Uploads/thumb//20120102/s_1e72d25a6aa567da5a9c10637276f08a0c6591d4.JPG', 4256, 2832, 1798, 'Xie Youding'),
(296, 168, 'selectersky', 1325453193, 1324378093, '27c9e3f8e28629484c783ae93729fe050bfd622e.jpg', '', './Uploads/images//20120102/27c9e3f8e28629484c783ae93729fe050bfd622e.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_27c9e3f8e28629484c783ae93729fe050bfd622e.jpg', './Uploads/thumb//20120102/s_27c9e3f8e28629484c783ae93729fe050bfd622e.jpg', 3008, 2000, 5087, 'Apaydin Alain'),
(297, 168, 'selectersky', 1325453195, 1324376759, '968540711e1a3d2c4c92ad7cbb1cc2fb544a5079.jpg', '', './Uploads/images//20120102/968540711e1a3d2c4c92ad7cbb1cc2fb544a5079.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_968540711e1a3d2c4c92ad7cbb1cc2fb544a5079.jpg', './Uploads/thumb//20120102/s_968540711e1a3d2c4c92ad7cbb1cc2fb544a5079.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(298, 168, 'selectersky', 1325453197, 1324376153, '72d49583dd0d717a38b717c9a62047c5249c5f99.jpg', '', './Uploads/images//20120102/72d49583dd0d717a38b717c9a62047c5249c5f99.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_72d49583dd0d717a38b717c9a62047c5249c5f99.jpg', './Uploads/thumb//20120102/s_72d49583dd0d717a38b717c9a62047c5249c5f99.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(299, 168, 'selectersky', 1325453199, 1324376459, 'ec717442db294966f754fed64a1fe2f0cec403cf.jpg', '', './Uploads/images//20120102/ec717442db294966f754fed64a1fe2f0cec403cf.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_ec717442db294966f754fed64a1fe2f0cec403cf.jpg', './Uploads/thumb//20120102/s_ec717442db294966f754fed64a1fe2f0cec403cf.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(300, 169, 'selectersky', 1325453378, 1324376759, '4b2629b4eff9f98469d88ff7608ae29b46a0de9d.jpg', '', './Uploads/images//20120102/4b2629b4eff9f98469d88ff7608ae29b46a0de9d.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_4b2629b4eff9f98469d88ff7608ae29b46a0de9d.jpg', './Uploads/thumb//20120102/s_4b2629b4eff9f98469d88ff7608ae29b46a0de9d.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(301, 169, 'selectersky', 1325453380, 1324376153, '1a398485ae584fba5c2409ead8466c0ac223aa81.jpg', '', './Uploads/images//20120102/1a398485ae584fba5c2409ead8466c0ac223aa81.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_1a398485ae584fba5c2409ead8466c0ac223aa81.jpg', './Uploads/thumb//20120102/s_1a398485ae584fba5c2409ead8466c0ac223aa81.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(302, 169, 'selectersky', 1325453382, 1324376459, '9dddc5deffd718a7b88576b96d00e5ff0e274468.jpg', '', './Uploads/images//20120102/9dddc5deffd718a7b88576b96d00e5ff0e274468.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_9dddc5deffd718a7b88576b96d00e5ff0e274468.jpg', './Uploads/thumb//20120102/s_9dddc5deffd718a7b88576b96d00e5ff0e274468.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(303, 170, 'selectersky', 1325517227, 1319373005, 'b6aa2d9da096d123e32a6db52b51406176ff6e75.JPG', '', './Uploads/images//20120102/b6aa2d9da096d123e32a6db52b51406176ff6e75.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_b6aa2d9da096d123e32a6db52b51406176ff6e75.JPG', './Uploads/thumb//20120102/s_b6aa2d9da096d123e32a6db52b51406176ff6e75.JPG', 4256, 2832, 2159, 'Xie Youding'),
(304, 170, 'selectersky', 1325517236, 1319373009, '32a8848b198c526f960f6a158581e7d2bb397aae.JPG', '', './Uploads/images//20120102/32a8848b198c526f960f6a158581e7d2bb397aae.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_32a8848b198c526f960f6a158581e7d2bb397aae.JPG', './Uploads/thumb//20120102/s_32a8848b198c526f960f6a158581e7d2bb397aae.JPG', 4256, 2832, 1798, 'Xie Youding'),
(305, 171, 'selectersky', 1325453470, 1319373005, 'e31c6c2bafe3b347264d1dfb11b8b61a5787b3c1.JPG', '', './Uploads/images//20120102/e31c6c2bafe3b347264d1dfb11b8b61a5787b3c1.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_e31c6c2bafe3b347264d1dfb11b8b61a5787b3c1.JPG', './Uploads/thumb//20120102/s_e31c6c2bafe3b347264d1dfb11b8b61a5787b3c1.JPG', 4256, 2832, 2159, 'Xie Youding'),
(306, 171, 'selectersky', 1325453472, 1319373009, '1336a21f6c76e582ac4ab4ec52d26ebea95b7bd7.JPG', '', './Uploads/images//20120102/1336a21f6c76e582ac4ab4ec52d26ebea95b7bd7.JPG', '时效图片', '', '', '', '', '杭州西博烟花大会', 100, 0, './Uploads/thumb//20120102/m_1336a21f6c76e582ac4ab4ec52d26ebea95b7bd7.JPG', './Uploads/thumb//20120102/s_1336a21f6c76e582ac4ab4ec52d26ebea95b7bd7.JPG', 4256, 2832, 1798, 'Xie Youding'),
(307, 172, 'selectersky', 1325453682, 1324378037, '7979cb6fc1609813f92def53dfb125c0f3662669.jpg', '', './Uploads/images//20120102/7979cb6fc1609813f92def53dfb125c0f3662669.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_7979cb6fc1609813f92def53dfb125c0f3662669.jpg', './Uploads/thumb//20120102/s_7979cb6fc1609813f92def53dfb125c0f3662669.jpg', 2000, 3008, 4586, 'Apaydin Alain'),
(308, 172, 'selectersky', 1325453685, 1324377870, '0e0da25b80cee1612bee50d2e14f92d38989abbc.jpg', '', './Uploads/images//20120102/0e0da25b80cee1612bee50d2e14f92d38989abbc.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_0e0da25b80cee1612bee50d2e14f92d38989abbc.jpg', './Uploads/thumb//20120102/s_0e0da25b80cee1612bee50d2e14f92d38989abbc.jpg', 3008, 2000, 5658, 'Apaydin Alain'),
(309, 172, 'selectersky', 1325453687, 1324375967, 'db4277b0c81f5a479f6488cf98f1d67cf2d7479d.jpg', '', './Uploads/images//20120102/db4277b0c81f5a479f6488cf98f1d67cf2d7479d.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_db4277b0c81f5a479f6488cf98f1d67cf2d7479d.jpg', './Uploads/thumb//20120102/s_db4277b0c81f5a479f6488cf98f1d67cf2d7479d.jpg', 2000, 3008, 4109, 'Apaydin Alain'),
(310, 172, 'selectersky', 1325453690, 1324378093, '46d59109591ae4c6197111fa6fb88309cb1dd9dd.jpg', '', './Uploads/images//20120102/46d59109591ae4c6197111fa6fb88309cb1dd9dd.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_46d59109591ae4c6197111fa6fb88309cb1dd9dd.jpg', './Uploads/thumb//20120102/s_46d59109591ae4c6197111fa6fb88309cb1dd9dd.jpg', 3008, 2000, 5087, 'Apaydin Alain'),
(311, 172, 'selectersky', 1325453692, 1324376759, '1c45bfda76960a950afb612254e2a5199a6e6eb6.jpg', '', './Uploads/images//20120102/1c45bfda76960a950afb612254e2a5199a6e6eb6.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_1c45bfda76960a950afb612254e2a5199a6e6eb6.jpg', './Uploads/thumb//20120102/s_1c45bfda76960a950afb612254e2a5199a6e6eb6.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(312, 172, 'selectersky', 1325453694, 1324376153, '0b974952b630dd59986aa41ce059306e9fca04f5.jpg', '', './Uploads/images//20120102/0b974952b630dd59986aa41ce059306e9fca04f5.jpg', '时效图片', '', 'France', '', 'Paris', 'Eiffel Tower Skating Rink - Paris', 100, 0, './Uploads/thumb//20120102/m_0b974952b630dd59986aa41ce059306e9fca04f5.jpg', './Uploads/thumb//20120102/s_0b974952b630dd59986aa41ce059306e9fca04f5.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(313, 172, 'selectersky', 1325453696, 1324376459, 'f11236bb25f3a1f5e106bdce2a0935bae2138ed8.jpg', '', './Uploads/images//20120102/f11236bb25f3a1f5e106bdce2a0935bae2138ed8.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_f11236bb25f3a1f5e106bdce2a0935bae2138ed8.jpg', './Uploads/thumb//20120102/s_f11236bb25f3a1f5e106bdce2a0935bae2138ed8.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(314, 173, 'xieyd', 1325453913, 1324358844, 'c5df881b262cc873cb18f0be1adddfaefd6b0dbc.jpg', '', './Uploads/images//20120102/c5df881b262cc873cb18f0be1adddfaefd6b0dbc.jpg', '时效图片', '', 'United States', 'CA', 'Los Angeles', 'Harrison Ford Hanging Out - LA', 100, 0, './Uploads/thumb//20120102/m_c5df881b262cc873cb18f0be1adddfaefd6b0dbc.jpg', './Uploads/thumb//20120102/s_c5df881b262cc873cb18f0be1adddfaefd6b0dbc.jpg', 2360, 3540, 2290, 'Madison Frederic'),
(315, 173, 'xieyd', 1325453916, 1324358847, '69b05221da3e2b20aee1daf4605dbcd88f1a7a29.jpg', '', './Uploads/images//20120102/69b05221da3e2b20aee1daf4605dbcd88f1a7a29.jpg', '时效图片', '', 'United States', 'CA', 'Los Angeles', 'Harrison Ford Hanging Out - LA', 100, 0, './Uploads/thumb//20120102/m_69b05221da3e2b20aee1daf4605dbcd88f1a7a29.jpg', './Uploads/thumb//20120102/s_69b05221da3e2b20aee1daf4605dbcd88f1a7a29.jpg', 2543, 3815, 2632, 'Madison Frederic'),
(316, 173, 'xieyd', 1325453918, 1324358848, '812cdd307e7406651ac4b892e014fc39b9bc843e.jpg', '', './Uploads/images//20120102/812cdd307e7406651ac4b892e014fc39b9bc843e.jpg', '时效图片', '', 'United States', 'CA', 'Los Angeles', 'Harrison Ford Hanging Out - LA', 100, 0, './Uploads/thumb//20120102/m_812cdd307e7406651ac4b892e014fc39b9bc843e.jpg', './Uploads/thumb//20120102/s_812cdd307e7406651ac4b892e014fc39b9bc843e.jpg', 2387, 3581, 2048, 'Madison Frederic'),
(317, 173, 'xieyd', 1325453921, 1324358851, '253c6858f194ef0335eb7f1c4a5301db73621f1a.jpg', '', './Uploads/images//20120102/253c6858f194ef0335eb7f1c4a5301db73621f1a.jpg', '时效图片', '', 'United States', 'CA', 'Los Angeles', 'Harrison Ford Hanging Out - LA', 100, 0, './Uploads/thumb//20120102/m_253c6858f194ef0335eb7f1c4a5301db73621f1a.jpg', './Uploads/thumb//20120102/s_253c6858f194ef0335eb7f1c4a5301db73621f1a.jpg', 2585, 3878, 2954, 'Madison Frederic'),
(318, 175, 'selectersky', 1325518352, 1324378093, '9317b4a1e00ff2e74c5fd2fb38f995590ae20657.jpg', '', './Uploads/images//20120102/9317b4a1e00ff2e74c5fd2fb38f995590ae20657.jpg', '时效图片', '', 'France', '', 'Paris', '302236_010', 100, 0, './Uploads/thumb//20120102/m_9317b4a1e00ff2e74c5fd2fb38f995590ae20657.jpg', './Uploads/thumb//20120102/s_9317b4a1e00ff2e74c5fd2fb38f995590ae20657.jpg', 3008, 2000, 5087, 'Apaydin Alain'),
(319, 175, 'selectersky', 1325518357, 1324376759, '800b9a97b2cf77fdd6291e079c468d90b5f53e09.jpg', '', './Uploads/images//20120102/800b9a97b2cf77fdd6291e079c468d90b5f53e09.jpg', '时效图片', '', 'France', '', 'Paris', '302236_011', 100, 0, './Uploads/thumb//20120102/m_800b9a97b2cf77fdd6291e079c468d90b5f53e09.jpg', './Uploads/thumb//20120102/s_800b9a97b2cf77fdd6291e079c468d90b5f53e09.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(320, 175, 'selectersky', 1325518363, 1324376153, 'e464778f8803cbbf05639f1b5d8b794c5328168d.jpg', '', './Uploads/images//20120102/e464778f8803cbbf05639f1b5d8b794c5328168d.jpg', '时效图片', '', 'France', '', 'Paris', '302236_012', 100, 0, './Uploads/thumb//20120102/m_e464778f8803cbbf05639f1b5d8b794c5328168d.jpg', './Uploads/thumb//20120102/s_e464778f8803cbbf05639f1b5d8b794c5328168d.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(321, 175, 'selectersky', 1325518369, 1324376459, '2b510e051084d47fd7be52207197d022ca5ece6a.jpg', '', './Uploads/images//20120102/2b510e051084d47fd7be52207197d022ca5ece6a.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_2b510e051084d47fd7be52207197d022ca5ece6a.jpg', './Uploads/thumb//20120102/s_2b510e051084d47fd7be52207197d022ca5ece6a.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(322, 176, 'selectersky', 1325518673, 1324376759, 'd1dffeb4d0cdc138b169289975d642d2a979e4e7.jpg', '', './Uploads/images//20120102/d1dffeb4d0cdc138b169289975d642d2a979e4e7.jpg', '时效图片', '', 'France', '', 'Paris', '302236_011', 100, 0, './Uploads/thumb//20120102/m_d1dffeb4d0cdc138b169289975d642d2a979e4e7.jpg', './Uploads/thumb//20120102/s_d1dffeb4d0cdc138b169289975d642d2a979e4e7.jpg', 3008, 2000, 4082, 'Apaydin Alain');
INSERT INTO `pc_upload_images_detail` (`id`, `group_id`, `account`, `createtime`, `photo_time`, `filename`, `cate`, `url`, `type`, `title`, `country`, `province`, `city`, `remark`, `point`, `check_state`, `big_url`, `small_url`, `width`, `height`, `filesize`, `author`) VALUES
(323, 176, 'selectersky', 1325518678, 1324376153, 'f6c57d4b8fc68d3f990b2c1c1bbe543ab064a783.jpg', '', './Uploads/images//20120102/f6c57d4b8fc68d3f990b2c1c1bbe543ab064a783.jpg', '时效图片', '', 'France', '', 'Paris', '302236_012', 100, 0, './Uploads/thumb//20120102/m_f6c57d4b8fc68d3f990b2c1c1bbe543ab064a783.jpg', './Uploads/thumb//20120102/s_f6c57d4b8fc68d3f990b2c1c1bbe543ab064a783.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(324, 176, 'selectersky', 1325518685, 1324376459, 'eeb0b589232f41e804f7cad5c69b900d76040bb7.jpg', '', './Uploads/images//20120102/eeb0b589232f41e804f7cad5c69b900d76040bb7.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_eeb0b589232f41e804f7cad5c69b900d76040bb7.jpg', './Uploads/thumb//20120102/s_eeb0b589232f41e804f7cad5c69b900d76040bb7.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(325, 177, 'selectersky', 1325518751, 1324376759, 'b4f6efb46f8fe475e276e071426e20571feca479.jpg', '', './Uploads/images//20120102/b4f6efb46f8fe475e276e071426e20571feca479.jpg', '时效图片', '', 'France', '', 'Paris', '302236_011', 100, 0, './Uploads/thumb//20120102/m_b4f6efb46f8fe475e276e071426e20571feca479.jpg', './Uploads/thumb//20120102/s_b4f6efb46f8fe475e276e071426e20571feca479.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(326, 177, 'selectersky', 1325518763, 1324376459, '1aac668dfd9ba386d48c317aff0403cb0be4debc.jpg', '', './Uploads/images//20120102/1aac668dfd9ba386d48c317aff0403cb0be4debc.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_1aac668dfd9ba386d48c317aff0403cb0be4debc.jpg', './Uploads/thumb//20120102/s_1aac668dfd9ba386d48c317aff0403cb0be4debc.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(327, 177, 'selectersky', 1325519015, 1324378093, '16b0d82c32b5845d191076b11a67106750b15af7.jpg', '', './Uploads/images//20120102/16b0d82c32b5845d191076b11a67106750b15af7.jpg', '时效图片', '', 'France', '', 'Paris', '302236_010', 100, 0, './Uploads/thumb//20120102/m_16b0d82c32b5845d191076b11a67106750b15af7.jpg', './Uploads/thumb//20120102/s_16b0d82c32b5845d191076b11a67106750b15af7.jpg', 3008, 2000, 5087, 'Apaydin Alain'),
(328, 177, 'selectersky', 1325519033, 1324376459, 'b2d914d257499ad3e619ce8d4a59861624374671.jpg', '', './Uploads/images//20120102/b2d914d257499ad3e619ce8d4a59861624374671.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_b2d914d257499ad3e619ce8d4a59861624374671.jpg', './Uploads/thumb//20120102/s_b2d914d257499ad3e619ce8d4a59861624374671.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(329, 178, 'selectersky', 1325519184, 1324376759, 'b029eb68dae0e5e76cac9b1051e9c81e290bf7f7.jpg', '', './Uploads/images//20120102/b029eb68dae0e5e76cac9b1051e9c81e290bf7f7.jpg', '时效图片', '', 'France', '', 'Paris', '302236_011', 100, 0, './Uploads/thumb//20120102/m_b029eb68dae0e5e76cac9b1051e9c81e290bf7f7.jpg', './Uploads/thumb//20120102/s_b029eb68dae0e5e76cac9b1051e9c81e290bf7f7.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(330, 178, 'selectersky', 1325519189, 1324376153, '94fcce087fb4e801a98edcd9ac6b5be33516f7b9.jpg', '', './Uploads/images//20120102/94fcce087fb4e801a98edcd9ac6b5be33516f7b9.jpg', '时效图片', '', 'France', '', 'Paris', '302236_012', 100, 0, './Uploads/thumb//20120102/m_94fcce087fb4e801a98edcd9ac6b5be33516f7b9.jpg', './Uploads/thumb//20120102/s_94fcce087fb4e801a98edcd9ac6b5be33516f7b9.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(331, 178, 'selectersky', 1325519196, 1324376459, 'a046c4bff0296972d9047c5d4ecb11ab6eaebac5.jpg', '', './Uploads/images//20120102/a046c4bff0296972d9047c5d4ecb11ab6eaebac5.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_a046c4bff0296972d9047c5d4ecb11ab6eaebac5.jpg', './Uploads/thumb//20120102/s_a046c4bff0296972d9047c5d4ecb11ab6eaebac5.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(332, 179, 'selectersky', 1325519318, 1324378093, '96027b933be930ad48d5b2a8884e12c30893385d.jpg', '', './Uploads/images//20120102/96027b933be930ad48d5b2a8884e12c30893385d.jpg', '时效图片', '', 'France', '', 'Paris', '302236_010', 100, 0, './Uploads/thumb//20120102/m_96027b933be930ad48d5b2a8884e12c30893385d.jpg', './Uploads/thumb//20120102/s_96027b933be930ad48d5b2a8884e12c30893385d.jpg', 3008, 2000, 5087, 'Apaydin Alain'),
(333, 179, 'selectersky', 1325519324, 1324376759, '7fb6134234ccee8e5f2d019ef5b6101e7b5dfa31.jpg', '', './Uploads/images//20120102/7fb6134234ccee8e5f2d019ef5b6101e7b5dfa31.jpg', '时效图片', '', 'France', '', 'Paris', '302236_011', 100, 0, './Uploads/thumb//20120102/m_7fb6134234ccee8e5f2d019ef5b6101e7b5dfa31.jpg', './Uploads/thumb//20120102/s_7fb6134234ccee8e5f2d019ef5b6101e7b5dfa31.jpg', 3008, 2000, 4082, 'Apaydin Alain'),
(334, 179, 'selectersky', 1325519330, 1324376153, '46e51c24318fbf24be2761d767071574af2b8500.jpg', '', './Uploads/images//20120102/46e51c24318fbf24be2761d767071574af2b8500.jpg', '时效图片', '', 'France', '', 'Paris', '302236_012', 100, 0, './Uploads/thumb//20120102/m_46e51c24318fbf24be2761d767071574af2b8500.jpg', './Uploads/thumb//20120102/s_46e51c24318fbf24be2761d767071574af2b8500.jpg', 3008, 2000, 4177, 'Apaydin Alain'),
(335, 179, 'selectersky', 1325519337, 1324376459, '0cb333493002b395ce5d57aa3e0b3937f3cb6e07.jpg', '', './Uploads/images//20120102/0cb333493002b395ce5d57aa3e0b3937f3cb6e07.jpg', '时效图片', '', '', '', '', '', 100, 0, './Uploads/thumb//20120102/m_0cb333493002b395ce5d57aa3e0b3937f3cb6e07.jpg', './Uploads/thumb//20120102/s_0cb333493002b395ce5d57aa3e0b3937f3cb6e07.jpg', 2000, 3008, 3785, 'Apaydin Alain'),
(336, 180, 'xieyd', 1325455712, 1325255317, '8b8d40575103246828c2b95a0e159c31a8036885.JPG', 'HKN', './Uploads/images//20120102/8b8d40575103246828c2b95a0e159c31a8036885.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 0, 0, './Uploads/thumb//20120102/m_8b8d40575103246828c2b95a0e159c31a8036885.JPG', './Uploads/thumb//20120102/s_8b8d40575103246828c2b95a0e159c31a8036885.JPG', 4256, 2832, 2566, 'Mark Goldman/Icon SMI'),
(337, 180, 'xieyd', 1325455714, 1325248872, '6a80573f78e6138be6db4dfc93c15f258a222dd5.JPG', 'HKC', './Uploads/images//20120102/6a80573f78e6138be6db4dfc93c15f258a222dd5.JPG', '时效图片', '', 'USA', 'MN', 'St. Cloud', 'NCAA HOCKEY: DEC 30 Western Michigan at St. Cloud State', 0, 0, './Uploads/thumb//20120102/m_6a80573f78e6138be6db4dfc93c15f258a222dd5.JPG', './Uploads/thumb//20120102/s_6a80573f78e6138be6db4dfc93c15f258a222dd5.JPG', 1462, 2193, 1547, 'Brace Hemmelgarn/Icon SMI'),
(338, 180, 'xieyd', 1325455716, 1325253708, '80c4cba1d3ed1dccd96fb98ddb8fe4dd99fd151f.JPG', 'HKC', './Uploads/images//20120102/80c4cba1d3ed1dccd96fb98ddb8fe4dd99fd151f.JPG', '时效图片', '', 'USA', 'MN', 'St. Cloud', 'NCAA HOCKEY: DEC 30 Western Michigan at St. Cloud State', 0, 0, './Uploads/thumb//20120102/m_80c4cba1d3ed1dccd96fb98ddb8fe4dd99fd151f.JPG', './Uploads/thumb//20120102/s_80c4cba1d3ed1dccd96fb98ddb8fe4dd99fd151f.JPG', 3308, 2205, 3279, 'Brace Hemmelgarn/Icon SMI'),
(339, 180, 'xieyd', 1325455717, 1325251810, '2f4f9d3df4efc59c95b813f29906f3841c4b01a0.JPG', 'HKC', './Uploads/images//20120102/2f4f9d3df4efc59c95b813f29906f3841c4b01a0.JPG', '时效图片', '', 'USA', 'MN', 'St. Cloud', 'NCAA HOCKEY: DEC 30 Western Michigan at St. Cloud State', 0, 0, './Uploads/thumb//20120102/m_2f4f9d3df4efc59c95b813f29906f3841c4b01a0.JPG', './Uploads/thumb//20120102/s_2f4f9d3df4efc59c95b813f29906f3841c4b01a0.JPG', 2562, 1708, 1480, 'Brace Hemmelgarn/Icon SMI'),
(340, 180, 'xieyd', 1325455719, 1325248175, 'da791da4d89b00a6d467ffc97dab4b775fbb2389.JPG', 'HKC', './Uploads/images//20120102/da791da4d89b00a6d467ffc97dab4b775fbb2389.JPG', '时效图片', '', 'USA', 'MN', 'St. Cloud', 'NCAA HOCKEY: DEC 30 Western Michigan at St. Cloud State', 0, 0, './Uploads/thumb//20120102/m_da791da4d89b00a6d467ffc97dab4b775fbb2389.JPG', './Uploads/thumb//20120102/s_da791da4d89b00a6d467ffc97dab4b775fbb2389.JPG', 3380, 2253, 2099, 'Brace Hemmelgarn/Icon SMI'),
(341, 180, 'xieyd', 1325455721, 1325248718, '5eebf8225485f0a0b6a0ccafaf5c7eca70f2c480.JPG', 'HKC', './Uploads/images//20120102/5eebf8225485f0a0b6a0ccafaf5c7eca70f2c480.JPG', '时效图片', '', 'USA', 'MN', 'St. Cloud', 'NCAA HOCKEY: DEC 30 Western Michigan at St. Cloud State', 0, 0, './Uploads/thumb//20120102/m_5eebf8225485f0a0b6a0ccafaf5c7eca70f2c480.JPG', './Uploads/thumb//20120102/s_5eebf8225485f0a0b6a0ccafaf5c7eca70f2c480.JPG', 2961, 1974, 2387, 'Brace Hemmelgarn/Icon SMI'),
(342, 180, 'xieyd', 1325455724, 1325254864, 'e67b0fc30a9b428b5b6944fa0d01e30d5884db85.JPG', 'HKN', './Uploads/images//20120102/e67b0fc30a9b428b5b6944fa0d01e30d5884db85.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 0, 0, './Uploads/thumb//20120102/m_e67b0fc30a9b428b5b6944fa0d01e30d5884db85.JPG', './Uploads/thumb//20120102/s_e67b0fc30a9b428b5b6944fa0d01e30d5884db85.JPG', 4256, 2832, 2056, 'Mark Goldman/Icon SMI'),
(343, 180, 'xieyd', 1325455727, 1325254870, '63cd7c81bfdb9fd3e2e79946192433d9d0d1b745.JPG', 'HKN', './Uploads/images//20120102/63cd7c81bfdb9fd3e2e79946192433d9d0d1b745.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 0, 0, './Uploads/thumb//20120102/m_63cd7c81bfdb9fd3e2e79946192433d9d0d1b745.JPG', './Uploads/thumb//20120102/s_63cd7c81bfdb9fd3e2e79946192433d9d0d1b745.JPG', 4256, 2832, 2352, 'Mark Goldman/Icon SMI'),
(344, 181, 'xieyd', 1325456045, 1325247125, '389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', 'HKN', './Uploads/images//20120102/389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 100, 1, './Uploads/thumb//20120102/m_389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', './Uploads/thumb//20120102/s_389580d9988b92af4683e665d8a95cf7b5f02e5d.JPG', 4256, 2832, 1684, 'Mark Goldman/Icon SMI'),
(345, 181, 'xieyd', 1325456047, 1325247708, '9dc5824f73533fcf51aae48fb16da37bbaef7e15.JPG', 'HKN', './Uploads/images//20120102/9dc5824f73533fcf51aae48fb16da37bbaef7e15.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 100, 1, './Uploads/thumb//20120102/m_9dc5824f73533fcf51aae48fb16da37bbaef7e15.JPG', './Uploads/thumb//20120102/s_9dc5824f73533fcf51aae48fb16da37bbaef7e15.JPG', 4256, 2832, 1531, 'Mark Goldman/Icon SMI'),
(346, 181, 'xieyd', 1325456050, 1325247821, 'e96980178617fa1fdd35d3897dd210089d9bbb60.JPG', 'HKN', './Uploads/images//20120102/e96980178617fa1fdd35d3897dd210089d9bbb60.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 100, 1, './Uploads/thumb//20120102/m_e96980178617fa1fdd35d3897dd210089d9bbb60.JPG', './Uploads/thumb//20120102/s_e96980178617fa1fdd35d3897dd210089d9bbb60.JPG', 4256, 2832, 1895, 'Mark Goldman/Icon SMI'),
(347, 181, 'xieyd', 1325456052, 1325248255, '326395963e073c6ff8111d71ab0e3bd509a3300f.JPG', 'HKN', './Uploads/images//20120102/326395963e073c6ff8111d71ab0e3bd509a3300f.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 100, 1, './Uploads/thumb//20120102/m_326395963e073c6ff8111d71ab0e3bd509a3300f.JPG', './Uploads/thumb//20120102/s_326395963e073c6ff8111d71ab0e3bd509a3300f.JPG', 4256, 2832, 1731, 'Mark Goldman/Icon SMI'),
(348, 181, 'xieyd', 1325456055, 1325248811, 'f213a7ff54477a6c52f37ccdd1ed30b03d719546.JPG', 'HKN', './Uploads/images//20120102/f213a7ff54477a6c52f37ccdd1ed30b03d719546.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 100, 1, './Uploads/thumb//20120102/m_f213a7ff54477a6c52f37ccdd1ed30b03d719546.JPG', './Uploads/thumb//20120102/s_f213a7ff54477a6c52f37ccdd1ed30b03d719546.JPG', 4256, 2832, 1481, 'Mark Goldman/Icon SMI'),
(349, 181, 'xieyd', 1325456057, 1325250629, '595cdf966399b1cf84acc011a72a47727f12759c.JPG', 'HKN', './Uploads/images//20120102/595cdf966399b1cf84acc011a72a47727f12759c.JPG', '时效图片', '', 'United States of America', 'DC', 'WASHINGTON', 'NHL: DEC 30 Sabres at Capitals', 100, 1, './Uploads/thumb//20120102/m_595cdf966399b1cf84acc011a72a47727f12759c.JPG', './Uploads/thumb//20120102/s_595cdf966399b1cf84acc011a72a47727f12759c.JPG', 4256, 2832, 1526, 'Mark Goldman/Icon SMI');

-- --------------------------------------------------------

--
-- 表的结构 `pc_urlrule`
--

DROP TABLE IF EXISTS `pc_urlrule`;
CREATE TABLE IF NOT EXISTS `pc_urlrule` (
  `urlruleid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `ishtml` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `showurlrule` varchar(255) NOT NULL,
  `showexample` varchar(255) NOT NULL,
  `listurlrule` varchar(255) NOT NULL,
  `listexample` varchar(255) NOT NULL,
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`urlruleid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_urlrule`
--

INSERT INTO `pc_urlrule` (`urlruleid`, `ishtml`, `showurlrule`, `showexample`, `listurlrule`, `listexample`, `listorder`) VALUES
(1, 0, '{$catdir}/show/{$id}.html|{$catdir}/show/{$id}_{$page}.html', 'news/show/1.html|news/show/1_1.html', '{$catdir}/|{$catdir}/{$page}.html', 'news/|news/1.html', 0),
(2, 0, 'show-{$catid}-{$id}.html|show-{$catid}-{$id}-{$page}.html', 'show-1-1.html|show-1-1-1.html', 'list-{$catid}.html|list-{$catid}-{$page}.html', 'list-1.html|list-1-1.html', 0),
(3, 0, '{$module}/show/{$id}.html|{$module}/show/{$id}-{$page}.html', 'Article/show/1.html|Article/show/1-1.html', '{$module}/list/{$catid}.html|{$module}/list/{$catid}-{$page}.html', 'Article/list/1.html|Article/list/1-1.html', 0),
(4, 1, '{$parentdir}{$catdir}/show_{$id}.html|{$parentdir}{$catdir}/show_{$id}_{$page}.html', 'news/show_1.html|news/show_1_1.html', '{$parentdir}{$catdir}/|{$parentdir}{$catdir}/{$page}.html', 'news/|news/1.html', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_user`
--

DROP TABLE IF EXISTS `pc_user`;
CREATE TABLE IF NOT EXISTS `pc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `realname` varchar(50) NOT NULL DEFAULT '',
  `question` varchar(50) NOT NULL DEFAULT '',
  `answer` varchar(50) NOT NULL DEFAULT '',
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `tel` varchar(50) NOT NULL DEFAULT '',
  `mobile` varchar(50) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `web_url` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `login_count` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `last_logintime` int(11) unsigned NOT NULL DEFAULT '0',
  `reg_ip` char(15) NOT NULL DEFAULT '',
  `last_ip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `point` smallint(5) unsigned NOT NULL DEFAULT '0',
  `avatar` varchar(120) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `pc_user`
--

INSERT INTO `pc_user` (`id`, `groupid`, `username`, `password`, `email`, `realname`, `question`, `answer`, `sex`, `tel`, `mobile`, `fax`, `web_url`, `address`, `login_count`, `createtime`, `updatetime`, `last_logintime`, `reg_ip`, `last_ip`, `status`, `amount`, `point`, `avatar`) VALUES
(1, 1, 'admin', '3da8dc80c8e1e6c5bbcf40806903345f50b5ca2d', 'admin2@yourphp.cn', '管理员', '', '', 1, '', '', '', '', '', 196, 1321685843, 1322066981, 1325494174, '127.0.0.1', '192.168.2.100', 1, 0.00, 0, ''),
(2, 2, 'wangxb', '3da8dc80c8e1e6c5bbcf40806903345f50b5ca2d', 'ncsy1983@126.com', '王小兵', '2222', '', 1, '13757190063', '13757190063', '12345678', '', '', 0, 1321860184, 1322148631, 0, '127.0.0.1', '', 1, 0.00, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_userrequire`
--

DROP TABLE IF EXISTS `pc_userrequire`;
CREATE TABLE IF NOT EXISTS `pc_userrequire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `title` varchar(512) CHARACTER SET utf8 NOT NULL,
  `people` varchar(30) CHARACTER SET utf8 NOT NULL,
  `place` varchar(50) CHARACTER SET utf8 NOT NULL,
  `time` int(25) NOT NULL,
  `size` int(20) NOT NULL,
  `starttime` int(25) NOT NULL,
  `endtime` int(25) NOT NULL,
  `value` int(25) NOT NULL,
  `state` varchar(32) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- 转存表中的数据 `pc_userrequire`
--

INSERT INTO `pc_userrequire` (`id`, `username`, `type`, `title`, `people`, `place`, `time`, `size`, `starttime`, `endtime`, `value`, `state`) VALUES
(19, '', '文娱历史', '这次测试标题', '你好', '杭州', 1324526400, 78, 1324526400, 1325304000, 900, '后说呢么 ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
