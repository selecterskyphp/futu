<?php
include_once 'PhotoChina/Lib/ORG/Exif.class.php';
$img = dirname(__FILE__).'/_DSC0006.JPG';
$exif = new Exif($img);
var_dump($exif->getInfo());
exit;
$data = array(
                'FileName'=>array('文件名', $infoAll['FileName']),
                'FileDateTime' => array('文件修改时间',$infoAll['FileDateTime']),//为整数
                'FileSize' => array('文件大小',$infoAll['FileSize']),//字节
                'FileType' => array('Exif文件类型',$this->getImgtype('Exif')),
                'MimeType' => array('Mime文件类型',$infoAll['MimeType']),
                'SectionsFound' => array('找到Sections',$infoAll['SectionsFound']),
                'html' => array('html中图片宽高',$infoAll['html']),
                'Height' => array('图片高度',$infoAll['Height']),//单位px
                'Width' => array('图片宽度',$infoAll['Width']),
                'IsColor' => array('是否彩色',$infoAll['IsColor'] == 1 ? '是' : '否'),
                'ByteOrderMotorola' => array('是否为Motorola字节顺序',$infoAll['ByteOrderMotorola'] == 1 ? '是' : '否'),
                'ApertureFNumber' => array('光圈数',$infoAll['ApertureFNumber']),
                'Comments' => array('作者注释',$infoAll['Comments']),
                'Author' => array('作者',$infoAll['Author']),
                'UserComment' => array('用户注释',$infoAll['UserComment']),
                'UserCommentEncoding' => array('用户注释编码',$infoAll['UserCommentEncoding']),
                'Thumbnail.FileType' => array('缩略图Exif文件类型',$this->getImgtype('Exif')),
                'Thumbnail.MimeType' => array('缩略图Mime文件类型',$infoAll['Thumbnail.MimeType']),
                'Make' => array('制造商',$infoAll['Make']),
                'Model' => array('型号',$infoAll['Model']),
                'Orientation' => array('方向',array_search($infoAll['Orientation'],array(
                    'top left side' => 1,
                    'top right side' => 2,
                    'bottom right side' => 3,
                    'bottom left side' => 4,
                    'left side top' => 5,
                    'right side top' => 6,
                    'right side bottom' => 7,
                    'left side bottom' => 8
                ))),
                'XResolution' => array('水平分辨率',$infoAll['XResolution']),
                'YResolution' => array('垂直分辨率',$infoAll['YResolution']),
                'ResolutionUnit' => array('分辨率单位',array_search($infoAll['ResolutionUnit'],array(
                    '无单位' => 1,
                    '英寸' => 2,
                    '厘米' => 3
                ))),
                'Software' => array('创建软件',$infoAll['Software']),
                'DateTime' => array('最后修改时间',$infoAll['DateTime']),
                'YCbCrPositioning' => array('YCbCr位置控制',$infoAll['YCbCrPositioning'] == 1 ? '像素阵列的中心' : '基准点'),
                'Exif_IFD_Pointer' => array('Exif图像IFD的指针',$infoAll['Exif_IFD_Pointer']),
                'Compression' => array('压缩方式',$infoAll['Compression'] == 6 ? 'jpeg压缩' : '无压缩'),
                'JPEGInterchangeFormat' => array('JPEG SOI偏移',$infoAll['JPEGInterchangeFormat']),
                'JPEGInterchangeFormatLength' => array('JPEG数据字节',$infoAll['JPEGInterchangeFormatLength']),
                'ExposureTime' => array('曝光时间',$infoAll['ExposureTime']),//单位秒
                'FNumber' => array('焦距比数',$infoAll['FNumber']),
                'ExposureProgram' => array('曝光程序',array_search($infoAll['ExposureProgram'],array(
                    '手动控制' => 1,
                    '程序控制' => 2,
                    '光圈优先' => 3,
                    '快门优先' => 4,
                    '景深优先' => 5,
                    '运动模式' => 6,
                    '肖像模式' => 7,
                    '风景模式' => 8
                ))),
                'ISOSpeedRatings' => array('ISO感光度',$infoAll['ISOSpeedRatings']),
                'ExifVersion' => array('Exif版本',$infoAll['ExifVersion']),
                'DateTimeOriginal' => array('拍摄时间',$infoAll['DateTimeOriginal']),
                'DateTimeDigitized' => array('数字化时间',$infoAll['DateTimeDigitized']),
                'ComponentsConfiguration' => array('分量配置',$infoAll['ComponentsConfiguration']),
                'CompressedBitsPerPixel' => array('图像压缩率',$infoAll['CompressedBitsPerPixel']),
                'ExposureBiasValue' => array('曝光补偿',$infoAll['ExposureBiasValue'] ),//单位：电子伏特
                'MaxApertureValue' => array('最大光圈值',$infoAll['MaxApertureValue']),
                'MeteringMode' => array('测光模式',array_search($infoAll['MeteringMode'],array(
                    '未知' => 0,
                    '平均' => 1,
                    '中央重点平均测光' => 2,
                    '点测' => 3,
                    '分区' => 4,
                    '评估' => 5,
                    '局部' => 6,
                    '其他' => 255
                ))),
                'LightSource' => array('光源',array_search($infoAll['LightSource'],array(
                    '未知' => 0,
                    '日光灯' => 1,
                    '荧光灯' => 2,
                    '钨丝灯' => 3,
                    '闪光灯' => 10,
                    '标准灯光A' => 17,
                    '标准灯光B' => 18,
                    '标准灯光C' => 19,
                    'D55' => 20,
                    'D65' => 21,
                    'D75' => 22,
                    '其他' => 255,
                ))),
                'Flash' => array('闪光灯',array_search($infoAll['Flash'],array(
                    '闪光灯未闪光' => 0,
                    '闪光灯已闪光' => 1,
                    '闪光灯已闪光但频闪观测器未检测到返回光源' => 5,
                    '闪光灯已闪光,频闪观测器检测到返回光源' => 7
                ))),
                'FocalLength' => array('焦距',$infoAll['FocalLength']),//单位：毫米
                'SubSecTime' => array('亚秒时间',$infoAll['SubSecTime']),
                'SubSecTimeOriginal' => array('亚秒级拍摄时间',$infoAll['SubSecTimeOriginal']),
                'SubSecTimeDigitized' => array('亚秒级数字化时间',$infoAll['SubSecTimeDigitized']),
                'FlashPixVersion' => array('FlashPix版本',$infoAll['FlashPixVersion']),
                'ColorSpace' => array('色彩空间',$infoAll['ColorSpace'] == 1 ? 'sRGB' : 'Uncalibrated'),
                'ExifImageWidth' => array('Exif图片宽度',$infoAll['ExifImageWidth']),//单位px
                'ExifImageLength' => array('EXif图片高度',$infoAll['ExifImageLength']),
                'InteroperabilityOffset' => array('IFD格式数据偏移量',$infoAll['InteroperabilityOffset']),
                'SensingMethod' => array('彩色区域传感器类型',$infoAll['SensingMethod'] == 2 ? '单色区传感器' : '其他'),
                'FileSource' => array('图片像源',$infoAll['FileSource'] == '0x03' ? '数码相机' : '其他'),
                'SceneType' => array('场景类型',$infoAll['SceneType'] == '0x01' ? '直接拍摄' : '其他'),
                'CFAPattern' => array('滤波阵列图案',$infoAll['CFAPattern']),
                'CustomRendered' => array('自定义图像处理',$infoAll['CustomRendered']),
                'ExposureMode' => array('曝光模式',$infoAll['CustomRendered'] == 1 ? '手动' : '自动'),
                'WhiteBalance' => array('白平衡',$infoAll['WhiteBalance'] == 1 ? '手动' : '自动'),
                'DigitalZoomRatio' => array('数位变焦倍率',$infoAll['DigitalZoomRatio']),
                'FocalLengthIn35mmFilm' => array('等价35mm焦距',$infoAll['FocalLengthIn35mmFilm']),//单位：毫米
                'SceneCaptureType' => array('取景模式',array_search($infoAll['SceneCaptureType'],array(
                    '自动' => 0,
                    '肖像场景' => 1,
                    '景观场景' => 2,
                    '运动场景' => 3,
                    '夜景' => 4,
                    '自动曝光' => 5,
                    '光圈优先自动曝光' => 256,
                    '快门优先自动曝光' => 512,
                    '手动曝光' => 768,
                ))),
                'GainControl' => array('增益控制',$infoAll['GainControl']),
                'Contrast' => array('对比度',array_search($infoAll['Contrast'],array(
                    '低' => -1,
                    '普通' => 0,
                    '高' => 1
                ))),
                'Saturation' => array('饱和度',array_search($infoAll['Saturation'],array(
                    '低' => -1,
                    '普通' => 0,
                    '高' => 1
                ))),
                'Sharpness' => array('清晰度',array_search($infoAll['Sharpness'],array(
                    '低' => -1,
                    '普通' => 0,
                    '高' => 1
                ))),
                'SubjectDistanceRange' => array('对焦距离',array_search($infoAll['SubjectDistanceRange'],array(
                    '未知' => 0,
                    '微距' => 1,
                    '近景' => 2,
                    '远景' => 3
                ))),
                'InterOperabilityIndex' => array('InterOperability指数',$infoAll['InterOperabilityIndex']),
                'InterOperabilityVersion' => array('InterOperability版本',$infoAll['InterOperabilityVersion']),
                'iptcTitle' =>array('标题',$iptc['2#005'][0]),
                'iptcKeyword'=>array('关键字',$iptc['2#025'][0]),
                'iptcMemo'=>array('说明 ',$iptc['2#040'][0]),//简单说明
                'iptcDesc'=>array('说明 ',$iptc['2#120'][0]),//详细说明
                'iptcAuth'=>array('作者',$iptc['2#122'][0]),//创建说明的作者
                'iptcBigTitle'=>array('大标题',$iptc['2#105'][0]),
                'iptcCate'=>array('类列',$iptc['2#015'][0])
            );