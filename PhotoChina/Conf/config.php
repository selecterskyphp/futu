<?php
$database = require ('./config.php');
$config	= array(
		'DEFAULT_THEME'		=> 'Default',
		'DEFAULT_CHARSET' => 'utf-8',
		'APP_GROUP_LIST' => 'Home,Admin,User,Blog',
        'APP_DEBUG'=>false,
		'DEFAULT_GROUP' =>'Home',
		'TMPL_FILE_DEPR' => '_',
		'DB_FIELDS_CACHE' => false,
		'DB_FIELDTYPE_CHECK' => true,
		'URL_CASE_INSENSITIVE'=>true,
		'URL_ROUTER_ON' => true,
		'URL_AUTO_REDIRECT' => false,
		'DEFAULT_LANG'   =>    'zh-cn',//默认语言
		'LANG_SWITCH_ON' => true, //多语言
		'TAG_EXTEND_PARSE' =>array(
			'if' => 'template_if',
			'else' =>'template_else',
			'elseif' => 'template_elseif'
		),
		'APP_AUTOLOAD_PATH'=> 'Think.Util.',
		'TAGLIB_LOAD' => true,
		//'TAGLIB_PRE_LOAD' => 'html,yp',
		'IMAGES_PATH'=>'./Uploads/images/', //原始图保存路径
		'THUMB_PATH'=>'./Uploads/thumb/',//缩略图保存路径
		'HEAD_IMAGES_PATH'=>'./Uploads/headimage/',//头像图片保存路径
		'ALLOW_IMAGE_EXT'=>'*.jpg,*.jpeg,*.psd',//允许上传的图片格式
		'USER_TYPE'=>array('下载用户','摄影师'),//用户类型
		//图组状态
		'IMAGE_STATUS'=>array(0=>'待审核',1=>'部分通过',2=>'全部通过',3=>'未通过',100=>'未上传图片',200=>'删除'),//0白色 1黄色 2绿色 3红色
		//图片状态
		'PHOTO_STATUS'=>array(1=>'通过',2=>'未通过',200=>'删除'),
		'IMAGE_STATUS_COLOR'=>array(0=>'qiu_white',1=>'qiu_yellow',2=>'qiu_gree',3=>'qiu_red',100=>'qiu_white',200=>'qiu_white',10=>'qiu_white'),
		'PHOTO_STATUS_COLOR'=>array(1=>'qiu_gree',2=>'qiu_red',200=>'qiu_white'),
);
return array_merge($database, $config);
?>