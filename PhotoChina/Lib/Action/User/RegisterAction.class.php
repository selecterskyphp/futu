<?php
class RegisterAction extends BaseAction
{
	
	function _initialize()
    {
		parent::_initialize();
		$this->dao = M('User');
    }
    public function index()
    {
		if($_COOKIE['YP_auth']) $this->success(L('login_ok'),$this->forward);
		$this->assign('bcid',0);
        $this->display();
    }

	public function doreg()
	{

		$username = trim($_POST['username']);
        $password = trim($_POST['password']);
		$email = trim($_POST['email']);
        $verifyCode = trim($_POST['verifyCode']);

        if(empty($username) || empty($password) || empty($email)){
           $this->error(L('empty_username_empty_password_empty_email'));
        }
		
		if($this->Config['user_reg_verify'] && md5($verifyCode) != $_SESSION['verify']){
           $this->error(L('error_verify'));
        }

		$_POST['groupid']=3;
		$_POST['login_count']=1;
		$_POST['createtime']=time();
		$_POST['updatetime']=time();
		$_POST['last_logintime']=time();
		$_POST['reg_ip']=get_client_ip();
		$_POST['status']=1;
		$_POST['password'] = sysmd5($_POST['password']);


		$user=$this->dao;
		if($data=$user->create()){
			if(false!==$user->add()){
				$uid=$user->getLastInsID();
				$ru['role_id']=$_POST['groupid'];
				$ru['user_id']=$uid;
				$roleuser=M('RoleUser');
				$roleuser->add($ru);		
				$this->assign('jumpUrl',$this->forward);
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($user->getError());
		}
 
	}


	function checkEmail(){

        $email=$_GET['email'];
		$userid=intval($_GET['userid']);
		if(empty($userid)){
			if($this->dao->getByEmail($email)){
				 echo 'false';
			}else{
				echo 'true';
			}
		}else{
			//�ж������Ƿ��Ѿ�ʹ��
			if($this->dao->where("id!={$userid} and email='{$email}'")->find()){
				 echo 'false';
			}else{
				echo 'true';
			}
		}
        exit;
	}

	function checkusername(){
		$username=$_GET['username'];
		if($this->dao->getByUsername($username)){
				 echo 'false';
			}else{
				echo 'true';
		}
		exit;
	}
}
?>