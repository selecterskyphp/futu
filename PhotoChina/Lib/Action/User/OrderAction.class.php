<?php
if(!defined("YOURPHP")) exit("Access Denied");
class OrderAction extends BaseAction
{

	function _initialize()
    {	
		parent::_initialize();

		$this->dao = M('User');
		$this->assign('bcid',0);
		$user = $this->dao->find($this->_userid);
		$this->assign('vo',$user);
    }

    public function index()
    {
		$keyword = $_REQUEST['keyword'];
		import('@.Action.Adminbase');
		$c=new AdminbaseAction();
		if($this->_userid || $keyword){ 
			$map['userid']=intval($this->_userid);
			$c->_list(MODULE_NAME,$map);
		}
        $this->display();
    }

	public function show()
    {
		$sn = $_REQUEST['sn'];
		$id= $_REQUEST['id'];
		$model =M('Order');
		$cart = $id ? $model->find($id) : $model->getBySn($sn) ;
		if(empty($cart)){
			  $this->error ( L('do_empty'));
		}

		$p=M($cart['module']);		 
		$cart['productlist']=unserialize($cart['productlist']);

		foreach((array)$cart['productlist'] as $key =>$rs){
			$cart['totalnum'] +=$rs['num'];
			$cart['totalprice'] += $rs['num']*$rs['price'];
			$cart['productlist'][$key]['countprice'] = $rs['num']*$rs['price'];
			$cart['productlist'][$key]['info']=$p->find($rs['id']);
		}
		$this->assign('cart',$cart);
		$this->display();
    }

	public function edit(){
		$sn = $_REQUEST['sn'];
		$id= $_REQUEST['id'];
		$model =M('Order');



		if($_POST['dosubmit']){

			if(!$this->_userid){
				$password = sysmd5($_POST['password']);
				$r=$model->where("id={$id} and password='{$password}'")->count();
				if(!$r)$this->success (L('do_empty'));
				unset($_POST['password']);
			}
			if (false === $model->create ()) {
				$this->error ( $model->getError () );
			}
			$orderid = $model->save();

			if (false!==$orderid) {
				$this->success (L('edit_ok'));
			}else{
				$this->error ($model->getDbError());
			}
		}else{
			$cart = $id ? $model->find($id) : $model->getBySn($sn) ;
			if(empty($cart)){
			  $this->error ( L('do_empty'));
			}
			$p=M($cart['module']);
			$cart['productlist']=unserialize($cart['productlist']);

			foreach((array)$cart['productlist'] as $key =>$rs){
				$cart['totalnum'] +=$rs['num'];
				$cart['totalprice'] += $rs['num']*$rs['price'];
				$cart['productlist'][$key]['countprice'] = $rs['num']*$rs['price'];
				$cart['productlist'][$key]['info']=$p->find($rs['id']);
			}
			$this->assign('cart',$cart);
		}
		$this->display();
		
	}


	
}
?>