<?php
class LoginAction extends BaseAction
{
	public $daoPhotoer;
	public $daoDownloader;
	function _initialize()
    {
		parent::_initialize();
		$this->daoPhotoer = M('Photoer');
		$this->daoDownloader = M('Downloader');
		$this->assign('bcid',0);
    }
    public function index()
    {
		if($this->_userid){		
			$forward = $_POST['forward'] ? $_POST['forward'] :$this->forward ;
			$this->assign('jumpUrl',$forward);
			$this->success(L('login_ok'));
		}
		
        $this->display();
    }
 
	public function dologin()
	{
		$username = trim($_POST['account']);
        $password = trim($_POST['password']);
        if(empty($username) || empty($password)){
            $this->assign('jumpUrl',U("Home-Index/index"));
           $this->error(L('empty_username_empty_password'));
        }

		$arrayName = array();
        $condition = array();
        $arrayName['account'] = $username;
     	$arrayName['email'] = array('eq',$username);
     	$arrayName['_logic']='or';

		$photoName = $this->daoPhotoer->where($arrayName)->find();
			
		$userName = $this->daoDownloader->where($arrayName)->find();

		if(false==$userName && false==$photoName)
		{
		    $this->assign('jumpUrl',U("Home-Index/index"));
		    $this->error(L('empty_userid'));
		}
		
       if(false != $photoName)
       {
           //如果登录是摄影师
    		if($photoName['password'] != sysmd5($_POST['password'])) 
    		{
    		    $this->assign('jumpUrl',U("Home-Index/index"));
    			$this->error(L('password_error'));	
    		}
			elseif($photoName['state'] !=0)
			{
			    $this->assign('jumpUrl',U("Home-Index/index"));
				$this->error(L('用户未激活或锁定状态'));	
			}
    		else
    		{
    		    $this->setLogin($photoName, 1);
				$this->redirect('Home-Photoer/index');
			}
        	
        }
        if(false != $userName)
        {
            //登录是的下载者用户
            if($userName['password'] != sysmd5($_POST['password'])) 
            {
            	$this->success(L('password_error'));
            }
			elseif($userName['state'] !=0)
			{	
			    $this->assign('jumpUrl',U("Home-Index/index"));
				$this->error(L('用户未激活或锁定状态'));	
			}
            else
            {
                $this->setLogin($userName, 0);
				$this->redirect('Home-Main/index');
            }
        }
	}

	public function getpass(){
		$this->display();
	}

	public function verify()
    {
        $type	 =	 isset($_GET['type'])?$_GET['type']:'gif';
        import("@.ORG.Image");
        Image::buildImageVerify(4,1,$type);
    }

	public function logout()
    {
        unset($_SESSION['login']);
        $this->assign('jumpUrl',U("Home-Index/index"));
		$this->success(L('loginouted'));
    }
}
?>