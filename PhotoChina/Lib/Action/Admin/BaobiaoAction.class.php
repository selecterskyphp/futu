<?php
class BaobiaoAction extends AdminbaseAction
{
	public function index()
    {
    	if( false==$this->isHaveAuth('baobiao','index') )
    		$this->error('对不起你没有该功能的权限');
    	
		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mName=$_POST['name'];
		$mCompany=$_POST['company'];
		$mProvince=$_POST['province'];
		$mCity=$_POST['city'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		$mTitle=$_POST['title'];
		$mPhotoid=$_POST['photoid'];
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."username like '%".$mAccount."%' ";
			$and=" and ";
		}
		if( !empty($mName)){
			$muser=M('downloader');
			$mwhere="name= '".$mName."'";
			$name=$muser->where($mwhere)->find();
			
			$where=$where.$and."username ='".$name['account']."'";
			$and=" and ";
		}
		
		if( !empty($mCompany)){
			$muser=M('downloader');
			$mwhere="name= '".$mCompany."'";
			$name=$muser->where($mwhere)->find();
			
			$where=$where.$and."username ='".$name['account']."'";
			$and=" and ";
		}
		
		if( !empty($mProvince) ){
			$where=$where.$and."province='".$mProvince."'";
			$and=" and ";
		}
		
		if( !empty($mCity) ){
			$where=$where.$and."city='".$mCity."'";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$begintime=strtotime($mRegStart);
			$where = $where.$and."buytime>='".$begintime."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$endtime=strtotime($mRegEnd);
			$where = $where.$and."buytime<='".$endtime."'";
			$and=" and ";
		}
		if(!empty($mTitle)){
			$where = $where.$and."phototitle='".$mTitle."'";
			$and=" and ";
			
			}
		if(!empty($mPhotoid)){
			$where = $where.$and."images_id='".$mPhotoid."'";
			$and=" and ";
			
			}
		$photoer=M('buy_images');
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign("page",$show);

		
		//今日注册人数	昨天注册人数	前天注册人数	前七天注册人数
		//前一月注册人数	总注册人数	总激活账号
		$curDate=date('Y-m-d 00:00:01',time());
		$where=" buytime>=".strtotime($curDate);
		$curCount=$photoer->where($where)->count(); //当天下载次数
		
		$yesDate=strtotime($curDate)-24*3600;
		$where=" buytime>=".$yesDate;
		$yesCount=$photoer->where($where)->count(); //昨天下载次数
		$yesCount=$yesCount-$curCount;
		
		$before7Date=strtotime($curDate)-24*3600*7;
		$where=" buytime>=".$before7Date;
		$before7count=$photoer->where($where)->count(); //一周下载次数
		
		
		$monthDay=strtotime("-1 month");
		$where=" buytime>=".$monthDay;
		$monthcount=$photoer->where($where)->count(); //月度下载次数
		
		
		$yearDay=strtotime("-1 year");
		$where=" buytime>=".$yearDay;
		$yearcount=$photoer->where($where)->count(); //年度下载次数
		
		//支付点数
		
		$totalValue=$photoer->sum('buy_values');
		
		$vo['today_reg_num']=$curCount;
		$vo['yer_reg_num']=$yesCount;
		$vo['before7_reg_num']=$before7count;
		$vo['month_reg_num']=$monthcount;
		$vo['year_reg_num']=$yearcount;
		$vo['total_reg_num']=$totalValue;
		//剩余点数
		$user=M('downloader');
		$uservalue=$user->sum('user_values');	
		$vo['user_reg_num']=$uservalue;	
		$this->assign('vo',$vo);
		//查询用户表中的信息 单位名称  用户类型 用户状态
	   for($i=0;$i<count($list);$i++){
		 $userinfo=M('downloader');
		$whereinfo="account = '".$list[$i]['username']."'";
		$memberinfo=$userinfo->where($whereinfo)->find();
		$mlist[$i]['id']=$list[$i]['id'];
		$mlist[$i]['username']=$list[$i]['username'];
		$mlist[$i]['work_company']=$memberinfo['work_company'];
		$mlist[$i]['images_id']=$list[$i]['images_id'];
		$mlist[$i]['url']=$list[$i]['url'];
		$mlist[$i]['title']=$list[$i]['title'];
		$mlist[$i]['buytime']=$list[$i]['buytime'];
		$mlist[$i]['buy_values']=$list[$i]['buy_values'];
		$mlist[$i]['down_id']=$list[$i]['down_id'];
		$mlist[$i]['role']=$memberinfo['role'];
		if($memberinfo['state']==0)
			$mlist[$i]['state']="正常";
			else
			$mlist[$i]['state']="未激活";	
	   }
		$this->assign('mlist',$mlist);
		
		$this->display();
    }
	//下载用户账号管理
	public function downer_info(){

		if( false==$this->isHaveAuth('baobiao','downer_info') )
    		$this->error('对不起你没有该功能的权限');

		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mName=$_POST['name'];
		$mEmail=$_POST['email'];
		$mCompany=$_POST['company'];
		$mProvince=$_POST['province'];
		$mCity=$_POST['city'];
		$mState=$_POST['state'];
		$mPhone=$_POST['phone'];
		
		
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account = '".$mAccount."'";
			$and=" and ";
		}
		if( !empty($mPhone) ){
			$where=$where.$and."phone =".$mPhone."'";
			$and=" and ";
		}
		if( !empty($mName)){
		
			$where=$where.$and."name= '".$mName."'";
			$and=" and ";
		}
		
		if( !empty($mCompany)){
			$where=$where.$and."work_company= '".$mCompany."'";
			$and=" and ";
		}
		
		if( !empty($mProvince) ){
			$where=$where.$and."province='".$mProvince."'";
			$and=" and ";
		}
		
		if( !empty($mCity) ){
			$where=$where.$and."city='".$mCity."'";
			$and=" and ";
		}
		
		if( !empty($mEmail) ){
			$where = $where.$and."email='".$mEmail."'";
			$and=" and ";
		}
		
		if( !empty($mState) ){
			if($mState=="正常")
				$state=0;
			else
				$state=1;
			$where = $where.$and."state='".$state."'";
			$and=" and ";
		}
		$photoer=M('downloader');
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)->select();

		$this->assign('mlist',$list);
		
		$this->display();
		}
	//下载用户充值查询				
	public function supplement(){

		if( false==$this->isHaveAuth('baobiao','supplement') )
    		$this->error('对不起你没有该功能的权限');

		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mName=$_POST['oper'];
		$mCompany=$_POST['company'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account = '".$mAccount."'";
			$and=" and ";
		}
		if( !empty($mName)){
			
			$where=$where.$and."oper ='".$mName."'";
			$and=" and ";
		}
		
		if( !empty($mCompany)){
			$muser=M('downloader');
			$mwhere="work_company like '%".$mCompany."%' ";
			$name=$muser->where($mwhere)->find();
			
			$where=$where.$and."account like '%".$name['account']."%' ";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$where = $where.$and."createtime>='".$mRegStart."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$where = $where.$and."createtime<='".$mRegEnd."'";
			$and=" and ";
		}
		$photoer=M('addvalue');
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign("page",$show);

		//查询用户表中的信息 单位名称 电话 手机  
	   for($i=0;$i<count($list);$i++){
		 $userinfo=M('downloader');
		$whereinfo="account = '".$list[$i]['account']."'";
		$memberinfo=$userinfo->where($whereinfo)->find();
		$mlist[$i]['id']=$list[$i]['id'];
		
		$mlist[$i]['account']=$list[$i]['account'];
		$mlist[$i]['work_company']=$memberinfo['work_company'];
		$mlist[$i]['oper_type']=$list[$i]['oper_type'];
		$mlist[$i]['phone']=$memberinfo['phone'];	
		$mlist[$i]['name']=$list[$i]['name'];
		$mlist[$i]['oper']=$list[$i]['oper'];
		
		$mlist[$i]['points']=$list[$i]['points'];	
			
		$mlist[$i]['createtime']=$list[$i]['createtime'];
		$mlist[$i]['remark']=$list[$i]['remark'];
	   }
		$this->assign('mlist',$mlist);	
			
		$this->display();
	}
	//摄影师图片统计
	public function phototj(){

		if( false==$this->isHaveAuth('baobiao','phototj') )
    		$this->error('对不起你没有该功能的权限');


		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mName=$_POST['name'];
		$mCompany=$_POST['company'];
		$mProvince=$_POST['province'];
		$mCity=$_POST['city'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account = '".$mAccount."'";
			$and=" and ";
		}
		if( !empty($mName)){
			
			$where=$where.$and."name ='".$mName."'";
			$and=" and ";
		}
		
		if( !empty($mCompany)){			
			$where=$where.$and."company_name like '%".$mCompany."%' ";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$begintime=strtotime($mRegStart);
			$where = $where.$and."createtime>='".$begintime."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$endtime=strtotime($mRegEnd);
			$where = $where.$and."createtime<='".$endtime."'";
			$and=" and ";
		}
		if( !empty($mProvince) ){
			$where=$where.$and."province='".$mProvince."'";
			$and=" and ";
		}
		
		if( !empty($mCity) ){
			$where=$where.$and."city='".$mCity."'";
			$and=" and ";
		}
		$photoer=M('photoer');
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign("page",$show);

	   for($i=0;$i<count($list);$i++){
		 $group=M('upload_group_detail');
		 $images=M('upload_images_detail');
		 $simages=M('images_detail');
		 $buy=M('buy_images');
		$whereinfo="up_account = '".$list[$i]['account']."'";
		$groupinfo=$group->where($whereinfo)->count();
		$imagesinfo=$images->where($whereinfo)->count();
		$simagesinfo=$simages->where($whereinfo)->count();
		$buyinfo=$buy->where($whereinfo)->count();	
		
		$mlist[$i]['id']=$list[$i]['id'];
		$mlist[$i]['role']=$list[$i]['role'];		
		$mlist[$i]['account']=$list[$i]['account'];
		$mlist[$i]['name']=$list[$i]['name'];	
		$mlist[$i]['company_name']=$list[$i]['company_name'];	
		$mlist[$i]['city']=$list[$i]['city'];
		$mlist[$i]['phone']=$list[$i]['phone'];
			
		$mlist[$i]['group']=$groupinfo;
		$mlist[$i]['images']=$imagesinfo;
		
		$mlist[$i]['simages']=$simagesinfo;	
			
		$mlist[$i]['sellnum']=$buyinfo;
		$mlist[$i]['values']=$list[$i]['values'];
		//已结算
		$mlist[$i]['remark']=$list[$i]['values'];
		//状态
		if($list[$i]['state']==0)
			$states="正常";
		else
			$states="未激活";
		$mlist[$i]['state']=$states;	
	   }
		$this->assign('mlist',$mlist);	
			
		$this->display();
	}
	//摄影师结算管理
	public function photojs(){

		if( false==$this->isHaveAuth('baobiao','photojs') )
    		$this->error('对不起你没有该功能的权限');

		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account = '".$mAccount."'";
			$and=" and ";
		}
		if( !empty($mRegStart) ){
			$begintime=strtotime($mRegStart);
			$where = $where.$and."createtime>='".$begintime."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$endtime=strtotime($mRegEnd);
			$where = $where.$and."createtime<='".$endtime."'";
			$and=" and ";
		}
		$photoer=M('application');
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign("page",$show);
		$this->assign('mlist',$list);
	

	$this->display();
	}
	function correctstate(){

		if( false==$this->isHaveAuth('baobiao','correctstate') )
    		$this->error('对不起你没有该功能的权限');

		
		if($_POST['0submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('downloader');
				$id=implode(',',$ids);
				$date['id']=$id;
				$date['state']=0;
				
				if(false!==$photoer->save($date)){
					$this->success(L('激活成功'));
				}else{
					$this->error(L('delete_error'));
				}
			}else{
				$this->error(L('do_empty'));
			
			}
		}
		else if($_POST['1submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('downloader');
				$id=implode(',',$ids);
				$date['id']=$id;
				$date['state']=1;
				
				if(false!==$photoer->save($date)){
					$this->success(L('锁定成功'));
				}else{
					$this->error(L('delete_error'));
				}
			}else{
				$this->error(L('do_empty'));
			
			}	
		}
		else{
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('downloader');
				$id=implode(',',$ids);
				$date['id']=$id;
				$date['state']=2;
				
				if(false!==$photoer->save($date)){
					$this->success(L('封杀成功'));
				}else{
					$this->error(L('delete_error'));
				}
			}else{
				$this->error(L('do_empty'));
			
			}
			
			
			}
	}
	
	//摄影师上传统计
	function photoerup()
	{
		import ('@.ORG.Page');
		//查询
		
		$username=$_POST['name'];
		
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$name=$_POST['group_name'];
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
		
		$vo['filed']=$orderfiled;
		$vo['order']=$order;
		
		$group=M('upload_group_detail');
		$and="";
		
		if( !empty($start) )
		{
			$where=$where.$and."a.up_time>= ".strtotime($start);
			$and=" and ";
		}
		if( !empty($end) )
		{
			$where=$where.$and."a.up_time<=".strtotime($end);
			$and=" and ";
		}
		if( !empty($name) )
			$where=$where.$and."a.title='".$name."'";
		
		$sql="select a.*,b.name as realname from pc_upload_group_detail a left join pc_photoer b on a.up_account=b.account where b.type_style=1 and b.state=0";
	
		$datacount=$group->query($sql);
		$count=count($datacount);
	
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
	
		$sqldata=$sql." order by a.".$orderfiled.' '.$order;
		$sqldata=$sqldata.' limit '.$page->firstRow.','.$page->listRows;
	
		$mlist=$group->query($sqldata);
	
		$this->assign('mlist',$mlist);
		$this->assign('vo',$vo);
	
		$this->display();
	}
	
	//通讯员上传统计
	function txyup()
	{
		import ('@.ORG.Page');
		//查询
	
		$username=$_POST['name'];
	
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$name=$_POST['group_name'];
	
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
	
		$vo['filed']=$orderfiled;
		$vo['order']=$order;
	
		$group=M('upload_group_detail');
		$and="";
	
		if( !empty($start) )
		{
			$where=$where.$and."a.up_time>= ".strtotime($start);
			$and=" and ";
		}
		if( !empty($end) )
		{
			$where=$where.$and."a.up_time<=".strtotime($end);
			$and=" and ";
		}
		if( !empty($name) )
			$where=$where.$and."a.title='".$name."'";
	
		$sql="select a.*,b.name as realname from pc_upload_group_detail a left join pc_photoer b on a.up_account=b.account where b.type_style=0 and b.state=0";
	
		$datacount=$group->query($sql);
		$count=count($datacount);
	
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
	
		$sqldata=$sql." order by a.".$orderfiled.' '.$order;
		$sqldata=$sqldata.' limit '.$page->firstRow.','.$page->listRows;
	
		$mlist=$group->query($sqldata);
	
		$this->assign('mlist',$mlist);
		$this->assign('vo',$vo);
	
		$this->display();
	}
	
	//摄影师发稿统计
	function photoerfg()
	{
		import ('@.ORG.Page');
		
		$b=M('buy_images');
		
		$starttime=$_POST['start_query'];
		$endtime=$_POST['end_query'];
		
		$and="";
		if( !empty($starttime) )
		{
			$where="a.buytime>=".strtotime($starttime);
			$and=" and ";
		}
		if( !empty($endtime) )
			$where=$where.$and." a.buytime<=".strtotime($endtime);
		
		$sql="select a.*,b.name as realname from pc_buy_images a left join pc_photoer b on a.up_account=b.account where b.type_style=1 and b.state=0 ";
		
		$count=count($b->query($sql));
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		$sql="select a.*,b.name as realname from pc_buy_images a left join pc_photoer b on a.up_account=b.account where b.type_style=1 and b.state=0 order by a.id desc ";
		$sql=$sql.' limit'. $page->firstRow.','.$page->listRows;
		
		$mlist=$b->query($sql);
		
		$this->assign('mlist',$mlist);
		$this->display();
	}
	
	//通讯员发稿统计
	function txyfg()
	{
		import ('@.ORG.Page');
	
		$b=M('buy_images');
	
		$starttime=$_POST['start_query'];
		$endtime=$_POST['end_query'];
	
		$and="";
		if( !empty($starttime) )
		{
			$where="a.buytime>=".strtotime($starttime);
			$and=" and ";
		}
		if( !empty($endtime) )
			$where=$where.$and." a.buytime<=".strtotime($endtime);
	
		$sql="select a.*,b.name as realname from pc_buy_images a left join pc_photoer b on a.up_account=b.account where b.type_style=1 and b.state=0 ";
	
		$count=count($b->query($sql));
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
	
		$sql="select a.*,b.name as realname from pc_buy_images a left join pc_photoer b on a.up_account=b.account where b.type_style=0 and b.state=0 order by a.id desc ";
		$sql=$sql.'limit '.$page->firstRow.','.$page->listRows;
	
		$mlist=$b->query($sql);
	
		$this->assign('mlist',$mlist);
		$this->display();
	}

}
