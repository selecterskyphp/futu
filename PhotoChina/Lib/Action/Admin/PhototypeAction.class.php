<?php
class PhototypeAction extends AdminbaseAction {
	
	function index() {
		
		if( false==$this->isHaveAuth('Phototype','index') )
			$this->error('对不起你没有该功能的权限');
		
		//��ѯ���е�һ���Ͷ�����
		$menu=M('column');
		$where="  parentid=0";
		$onelist=$menu->where($where)->select();

		$count=0;
		for($i=0;$i<count($onelist);$i++)
		{
			$clist[$count]['id']=$onelist[$i]['id'];
			$clist[$count]['parentid']=$onelist[$i]['parentid'];
			$clist[$count]['secondid']=$onelist[$i]['secondid'];
			$clist[$count]['onetypename']=$onelist[$i]['name'];
			$clist[$count]['twotypename']="";
			$clist[$count]['threetypename']="";
			$clist[$count]['groupid']=$onelist[$i]['groupid'];

			$count++;

			//查找对应的二级
			$where=" parentid=".$onelist[$i]['groupid'];
			$twolist=$menu->where($where)->select();
			for($j=0;$j<count($twolist);$j++)
			{
				$clist[$count]['id']=$twolist[$j]['id'];
				$clist[$count]['parentid']=$twolist[$j]['parentid'];
				$clist[$count]['secondid']=$twolist[$j]['secondid'];
				$clist[$count]['onetypename']="";
				$clist[$count]['twotypename']=$twolist[$j]['name'];
				$clist[$count]['threetypename']="";
				$clist[$count]['groupid']=$twolist[$j]['groupid'];

				$count++;

				//查找对应的三级
				$where=" parentid=".$twolist[$j]['groupid'];
				$threelist=$menu->where($where)->select();
				for($m=0;$m<count($threelist);$m++)
				{
					$clist[$count]['id']=$threelist[$m]['id'];
					$clist[$count]['parentid']=$threelist[$m]['parentid'];
					$clist[$count]['secondid']=$threelist[$m]['secondid'];
					$clist[$count]['onetypename']="";
					$clist[$count]['twotypename']="";
					$clist[$count]['threetypename']=$threelist[$m]['name'];
					$clist[$count]['groupid']=$threelist[$m]['groupid'];

					$count++;
				}
			}
		}
	
		$this->assign('clist',$clist);
		
		$this->display();
	}
	
	function querytype()
	{
		$parentid=$_POST['onetypename'];
		
		if(!empty($parentid))
		{
			$menu=M('column');
			$where="  parentid=".$parentid;
			$twolist=$menu->where($where)->select();
			
			$this->ajaxReturn($twolist,$twolist,1);
		}
	}

	//添加子菜单
	function addsubtype()
	{
		$id=$_GET['id'];

		$c=M('column');
		$clist=$c->where('id='.$id)->find();


		$this->assign('vo',$clist);
		$this->display();
	}

	function subadd()
	{
		//父类groupid
		$pgroupid=$_POST['parentid'];

		$name=$_POST['typename'];

		if( strlen($name)<=0 )
			$this->error('类型名称不能为空');

		//判断父亲是几级
		$c=M('column');
		$pcol=$c->where('groupid='.$pgroupid)->find();
		if( $pcol['parentid']==0 )
			$span=100;
		else
			$span=1;

		//找出下级最大的groupid
		$where='select max(groupid) as mid from pc_column where parentid='.$pcol['groupid'];
		$maxgroup=$c->query($where);
		if(false==$maxgroup)
		{
			//第一个下级
			$id=$pcol['groupid']+$span;

			$data['parentid']=$pcol['groupid'];
			$data['secondid']=0;
			$data['name']=$name;
			$data['groupid']=$id;

			if($c->add($data))
				$this->success('新增成功');
			else
				$this->error('新增失败');
		}
		else
		{
			$id=$maxgroup[0]['mid']+$span;

			$data['parentid']=$pcol['groupid'];
			$data['secondid']=0;
			$data['name']=$name;
			$data['groupid']=$id;

			if($c->add($data))
				$this->success('新增成功');
			else
				$this->error('新增失败');
		}
	}

	//删除类型
	function delsubtype()
	{
		$id=$_GET['id'];


		//判断是几级
		$c=M('column');
		$clist=$c->where('id='.$id)->find();
		if( $clist['groupid']%100 ==0 )
		{
			//删除二级，要同时删除对应的三级
			$c->where('parentid='.$clist['groupid'])->delete();

			$c->where('groupid='.$clist['groupid'])->delete();

		}
		else
		{
			//删除三级
			$c->where('groupid='.$clist['groupid'])->delete();
		}

		$this->success('删除成功');
	}

	//修改类型
	function modisubtype()
	{
		$id=$_GET['id'];

		$c=M('column');
		$clist=$c->where('id='.$id)->find();

		$this->assign('vo',$clist);
		$this->display();
	}

	function submtype()
	{
		$id=$_POST['id'];

		$name=$_POST['name'];

		if( strlen($name)<=0 )
			$this->error('类型名称不能为空');

		$c=M('column');

		$data['name']=$name;
		$c->where('id='.$id)->save($data);

		$this->success('修改成功');
	}
}
?>