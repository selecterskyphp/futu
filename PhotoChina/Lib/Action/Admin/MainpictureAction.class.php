<?php
class MainpictureAction extends AdminbaseAction {

	function _initialize() {
		parent::_initialize();
		
	}
	
	function index() {
		$picture=M('mainpicture');
		$curtime=time();
		$where="begintime <= ".$curtime." and endtime >= ".$curtime;	
		$info=$picture->where($where)->select();
		$this->assign('info',$info);
		$this->display('Mainpicture_index');
	}
	
	public function delete(){
		
	
		}
		
	public function update(){
		$id=$_GET['id'];
		if(!empty($id))
		{
			$user=M('Mainpicture');
			$where="id = ".$id;
			$vo=$user->where($where)->find();
		
			$vo['begintime']=date("Y-m-d h:i:s",$info['begintime']);
			$vo['endtime']=date("Y-m-d h:i:s",$info['endtime']);		
			$this->assign('vo',$vo);	
		}
		$this->display();
	}
		
	public function save(){
			$date['id']=$_POST['id'];

			import("@.ORG.UploadFile");
			$upload = new UploadFile(); // 实例化上传类
			$upload->maxSize  = 3145728 ; // 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
			$upload->savePath =  './Uploads/image/'; // 设置附件上传目录
			$upload->$uploadReplace = true;
			
			$time=sysmd5(date('Y-m-d H:i:s',time()));
			
			if(!$upload->upload('',$time.".jpg")) { // 上传错误 提示错误信息
				$this->error($upload->getErrorMsg());
			}else{ // 上传成功 获取上传文件信息
				$info =  $upload->getUploadFileInfo();
				$uploadfilename=$info[0]["savename"];
			}
			
			$date['imgurl']='./Uploads/image/'.$uploadfilename;
			$date['remark']=$_POST['remark'];
			$date['begintime']=strtotime($_POST['begintime']);
			$date['endtime']=strtotime($_POST['endtime']);
			$date['number']=$_POST['number'];
			$date['url']=$_POST['url'];
			$user=M('mainpicture');
			if(strlen($_POST['id'])>0)
			{
				if(false==$user->save($date))
					$this->error("保存失败");
				else
					$this->index();
			}
			else
			{
				//新增
				if(false==$user->add($date))
					$this->error('保存失败');
				else
					$this->index();
			}
		}	
}