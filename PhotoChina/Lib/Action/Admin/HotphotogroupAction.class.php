<?php
class HotphotogroupAction extends AdminbaseAction
{
	public function index()
	{
		if( false==$this->isHaveAuth('hotphotogroup','index') )
			$this->error('对不起你没有该功能的权限');
			 
		$hot=M('hot_picture');
		$curtime=time();
		$where="begintime <= ".$curtime." and endtime >= ".$curtime;	
		$info=$hot->where($where)->select();
		$this->assign('info',$info);
	
		$this->display();
	}
	public function delete(){
		
		$id=$_GET['id'];
		$user=M('hot_picture');
		if(false!==$user->delete($id)){
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$photoer->getDbError());
			}
		}
	
	public function update(){
		$id=$_GET['id'];
		$user=M('hot_picture');
		$where="id=".$id;
		$vo=$user->where($where)->find();
		$vo['begintime']=date("Y-m-d h:m:s",$vo['begintime']);
		$vo['endtime']=date("Y-m-d h:m:s",$vo['endtime']);
		$this->assign('vo',$vo);
		$this->display();
	}
	public function save(){
			$date['id']=$_POST['id'];
			$date['number']=$_POST['number'];
			$date['begintime']=strtotime($_POST['begintime']);
			
			$date['endtime']=strtotime($_POST['endtime']);
			$user=M('hot_picture');	
			if(false==$user->save($date))
				$this->error("保存失败");
			else
				$this->success("保存成功");
		}
}
?>