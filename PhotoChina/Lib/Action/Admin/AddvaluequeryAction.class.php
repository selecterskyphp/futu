<?php
class AddvalueQueryAction extends AdminbaseAction {
	public $dao;
	function _initialize() {
		parent::_initialize();
		$this->dao=D('addvalue');
		if( empty($this->dao) )
			echo "tujian is null";
	}
	
	function index() {
		
		if( false==$this->isHaveAuth('AddvalueQuery','index') )
			$this->error('对不起你没有该功能的权限');
		
		import ('@.ORG.Page');
	
		$account=$_POST['account'];
		$oper=$_POST['oper_persion'];
		$startdate=$_POST['startdate'];
		$enddate=$_POST['enddate'];
		
		$this->assign($_POST);
		
		$and="";
		if( !empty($account) ){
			$where=$where.$and."account='".$account."'";
			$and=" and ";
		}
		if( !empty($oper)){
			$where=$where.$and."name like '%".$oper."%' ";
			$and=" and ";
		}
		
		if( !empty($startdate)){
			$where=$where.$and."startdate='".$startdate."'";
			$and=" and ";
		}
		
		if( !empty($enddate) ){
			$where=$where.$and."enddate='".$enddate."'";
			$and=" and ";
		}
		
		$query=$this->dao;
		$count=$query->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
	
		$this->assign("page",$show);
		$list=$query->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign('mlist',$list);
		$this->display();
	}
	
	function insert(){
		$query=$this->dao;
		if($data=$query->create()){
			if(false!==$tuijian->add()){
				/*$uid=$photoer->getLastInsID();
				 $ru['role_id']=$_POST['groupid'];
				$ru['user_id']=$uid;
				$roleuser=M('RoleUser');
				$roleuser->add($ru);*/
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($tuijian->getError());
		}
	}
	
	function update(){
		$query=$this->dao;
		if($data=$query->create()){
			if(!empty($data['id'])){
				if(false!==$query->save()){
					$this->success(L('edit_ok'));
				}else{
					$this->error(L('edit_error').$photoer->getDbError());
				}
			}else{
				$this->error(L('do_error'));
			}
		}else{
			$this->error($photoer->getError());
		}
	}
	
	function _before_add(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function _before_edit(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function delete(){
		$id=$_GET['id'];
		$query=$this->dao;
		if(false!==$query->delete($id)){
			$roleuser=M('RoleUser');
			$roleuser->where('user_id ='.$id)->delete();
			delattach(array('moduleid'=>0,'catid'=>0,'id'=>0,'userid'=>$id));
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$photoer->getDbError());
		}
	}
	
	function deleteall(){
		$ids=$_POST['ids'];
		if(!empty($ids) && is_array($ids)){
			$photoer=$this->dao;
			$id=implode(',',$ids);
			if(false!==$photoer->delete($id)){
				/*$roleuser=M('RoleUser');
				 $roleuser->where('user_id in('.$id.')')->delete();
				delattach("moduleid=0 and catid=0 and id=0 and userid in($id)");
				*/
				$this->success(L('delete_ok'));
			}else{
				$this->error(L('delete_error'));
			}
		}else{
			$this->error(L('do_empty'));
		}
	}
}



?>