<?php
class realsephotoerAction extends AdminbaseAction {

	function _initialize() {
		parent::_initialize();

	}

	function index() {
		
		if( false==$this->isHaveAuth('realsephotoer','index') )
			$this->error('对不起你没有该功能的权限');
		
		import ('@.ORG.Page');

		//��ѯ���ͨ�� ��δ����ͼ����Ϣ
		$typeid=$_POST['typeid'];
		$user=$_POST['username'];
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$groupname=$_POST['group_name'];
		$type=$_POST['type'];
		$state=$_POST['state'];
		
		if(empty($state)) $state="正常";
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
		
		$vo['filed']=$orderfiled;
		$vo['order']=$order;

		$group=M('group_detail');
		
		if( !empty($state) )
		{
			$ns=0;
			if( $state=="待发布" ) $ns=0;
			else if( $state=="正常" ) $ns=1; //审核已通过
			else if( $state=="审核不通过" ) $ns=3;
			else if( $state=="已删除" ) $ns=10; //10为已删除
			else $ns=1000;
		
			if( $ns<1000)
			{
				$where=$where.$and."state=".$ns;
				$and=" and ";
			}
		}
		
		if( !empty($type) && $type>100 )
		{
			$twhere=sprintf(" and ( (type_one>=%s and type_one<%s) or (type_two>=%s and type_two<=%s) or (type_three>=%s and type_three<=%s) )",
										$type,$type+999,
										$type,$type+999,
										$type,$type+999);
			$where=$where.$twhere;
		}
		if( !empty($user) )
			$where=$where." and up_account='".$user."'";
	
		if( !empty($start) )
			$where=$where." and up_time>='".$start."'";

		if( !empty($end) )
			$where=$where.$and." and up_time<='".$end."'";

		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		$orderinfo=$orderfiled." ".$order;
		$glist=$group->order($orderinfo)->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		$this->assign('glist',$glist);
		$this->assign('vo',$vo);

		$this->display('Realsephotoer_index');
	}
	
	//查询摄影师图组
	public function photoquery() {
	
		import ('@.ORG.Page');
		//查询
		$type=$_POST['type'];
		$username=$_POST['name'];
		$state=$_POST['state'];
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$name=$_POST['group_name'];
		
		$rgid=$_GET['rgid'];
	
		if(empty($state)) $state="等待审核";
	
		$group=M('upload_group_detail');
		$and="";
		if($type!=="全部" && !empty($type))
		{
			$where = " (type_one='".$type."' or type_two='".$type."' or type_three='".$type."') ";
			$and=" and ";
		}
		if( !empty($state) )
		{
			$ns=0;
			if( $state=="等待审核" ) $ns=0;
			else if( $state=="正常" ) $ns=1;
			else if( $state=="审核不通过" ) $ns=3;
			else if( $state=="已删除" ) $ns=10; //10为已删除
			else $ns=1000;
	
			if( $ns<1000)
			{
				$where=$where.$and."check_state=".$ns;
				$and=" and ";
			}
		}
		if( !empty($start) )
		{
			$where=$where.$and."up_time>= ".strtotime($start);
			$and=" and ";
		}
		if( !empty($end) )
		{
			$where=$where.$and."up_time<=".strtotime($end);
			$and=" and ";
		}
		if( !empty($name) )
		{
			$where=$where.$and."title='".$name."'";
		}
	
		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
	
		$list=$group->order('id desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign('mlist',$list);
		
		$this->assign('rgid',$rgid);
	
		$this->display('Realsephotoer_photoquery');
	}
	
	//�޸ķ���ͼ����Ϣ
	function checkgroup()
	{
		$id=$_GET['id'];
		if(!empty($id))
		{
			//查询该图组
			$g=M('group_detail');
			$where=" id=".$id;
			$vo=$g->where($where)->find();
			if(false!==$vo)
			{
				$this->assign('vo',$vo);
				
				//查询具体图片信息
				$image=M('images_detail');
				$where=" group_id=".$id." and state<200";
				$imglist=$image->where($where)->select();
				
				$this->assign('imglist',$imglist);
			}
		}
	
		$this->display('Realsephotoer_checkgroup');
	}
	
	//审核通过不通过
	function checkimage()
	{
		$type=$_POST['checktype'];
		$id=$_POST['checkid'];
		
		$img=M('images_detail');
		$where=" id=".$id;
		$data['state']=$type;
		if($img->where($where)->save($data))
			$this->success('审核成功');
		else
			$this->error('审核失败，id='.$type);	
	}
	
	//设置封面
	function setface()
	{
		$id=$_POST['mainfaceid'];
	
		$image=M('images_detail');
		$where=" id=".$id;
		$ilist=$image->where($where)->find();
	
		$group=M('group_detail');

		$where=" id=".$ilist['group_id'];
		$data['main_url']=$ilist['small_url'];
		$group->where($where)->save($data);
		
		$glist=$group->where($where)->find();
		
		$imglist=$image->where(' group_id='.$ilist['group_id'])->select(); 
		
		$this->assign('glist',$glist);
		$this->assign('imglist',$imglist);
		
		//dump($imglist);
		
		//$this->display('Waitrealsephotoer_checkgroup');
		//$this->checkgroup();
		$this->redirect('realsephotoer/checkgroup',array('id'=>$ilist['group_id']));
	
	}
	
	//保存修改的组信息
	function editgroup()
	{
		$groupId=$_POST['groupid'];
	
		$gtitle=$_POST['gtitle'];
		$gremark=$_POST['gremark'];
	
		$g=M('group_detail');
		$data['title']=$gtitle;
		$data['group_remark']=$gremark;
	
		if($g->where(' id='.$groupId)->save($data))
			$this->ajaxReturn($groupId,'保存成功',1);
		else
			$this->ajaxReturn($groupId,'保存失败',0);
	}
	
	//保存图组的修改信息 具体图片的信息
	function savegroup()
	{
		$id=$_GET['id'];
	
		$groupId=$_POST['groupid'];
		$img=M('images_detail');
		if(empty($id))
		{
			//保存全部
			$where=" group_id=".$groupId;
			$ilist=$img->where($where)->select();
			for($i=0;$i<count($ilist);$i++)
			{
				$imgId=$ilist[$i]['id'];
		
				//获取相关值
				$imgPoint=$_POST['point'.$imgId];
				$imgRemark=$_POST['iremark'.$imgId];
				$imgKey=$_POST['ikey'.$imgId];
	
				if(!empty($imgPoint))
					$data['value']=$imgPoint;
				if(!empty($imgRemark))
					$data['image_remark']=$imgRemark;
				if(!empty($imgKey))
					$data['images_key']=$imgKey;
	
				//
				$img->where('id='.$imgId)->save($data);
			}
		}
		else 
		{
			//只保存一个
			//获取相关值
			$imgPoint=$_POST['point'.$id];
			$imgRemark=$_POST['iremark'.$id];
			$imgKey=$_POST['ikey'.$id];
	
			if(!empty($imgPoint))
				$data['point']=$imgPoint;
			if(!empty($imgRemark))
				$data['image_remark']=$imgRemark;
			if(!empty($imgKey))
				$data['key']=$imgKey;
	
			$img->where('id='.$imgId)->save($data);
		}
	
		$this->success('保存成功');
	}
	
	//添加图片
	function addimage()
	{
		$this->display();
	}
	
	//审核图片
	function checkgroupframe()
	{
		$group['id']=$_GET['id'];
		
		$this->assign('group',$group);
		$this->display();
	}
	
	function editphotoergroup()
	{
		$groupId=$_GET['id'];
		$rgid=$_GET['rgid'];
		
		//图组分类，创建时间，编辑时间
		$upGroup=M('upload_group_detail');
		$vo=$upGroup->where(' id='.$groupId)->find();
		
		
		$up=M('upload_images_detail');
		$imagelist=$up->where(' group_id='.$groupId)->select();
		
		$this->assign('vo',$vo);
		$this->assign('imagelist',$imagelist);
		$this->assign('rgid',$rgid);
		
		$this->display();
	}
	
	//审核和删除所选图组
	function checkwithdel()
	{
		$ids=$_POST['ids'];
		$g=M('upload_group_detail');
		if($_POST['notchecked']) //审核不通过
		{
			for($i=0;$i<count($ids);$i++)
			{
			$data['check_state']=3;
			$g->where(' id='.$ids[$i])->save($data);
			}
		}
		else if($_POST['delchecked']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
			$data['check_state']=10; //状态10为已删除
			$g->where(' id='.$ids[$i])->save($data);
			}
		}
	
		$this->photoquery();
	}
	
	//复制单张或多张图片
	function copyimage()
	{
		$username=$_SESSION['username'];
		$ids=$_POST['copyid'];
		$rgid=$_POST['rgid'];
	
		//拷贝该图片信息到编辑图组
		$image=M('upload_images_detail');
		$ids=explode(",",$ids);
		for($i=0;$i<count($ids);$i++)
		{
			$id=$ids[$i];
	
			$id=trim($id);
			if(strlen($id)<=0 )
				continue;
	
			$where=" id=".$id;
			$ilist=$image->where($where)->find();
			if(false==$ilist)
				$this->ajaxReturn('123','查找编辑图组失败,id:'.$id,0);
	
			$data['group_id']=$rgid;
			if(!empty($ilist['createtime']))
				$data['up_time']=$ilist['createtime'];
			$data['url']=$ilist['url'];
			if(!empty($ilist['big_url']))
				$data['big_url']=$ilist['big_url'];
			if(!empty($ilist['small_url']))
				$data['small_url']=$ilist['small_url'];
			$data['value']=$ilist['point'];
			$data['image_remark']=$ilist['remark'];
			$data['up_account']=$ilist['account'];
			$data['check_time']=time();
			$data['release_time']=time();
			$data['check_oper']=$_SESSION['username'];
			//if(!empty($ilist['key']))
				//$data['images_key']=$ilist['key'];
			//file_put_contents('./xxx.txt',var_export($data,TRUE));
			$edimage=M('images_detail');
			if(false==$edimage->add($data))
				$this->ajaxReturn('123','添加图片失败',0);
	
			//把图片直接审核通过
			$upimage=M('upload_images_detail');
			$updata['check_state']=1;
	
			$upimage->where('id='.$id)->save($updata);
			
			//更新图组图片数量加一
			$rg=M('group_detail');
			$sql="update pc_group_detail set image_count=image_count+1 where id=".$rgid;
			$rg->execute($sql);
		}
			
		$this->ajaxReturn('123','成功',1);
	}
	
	//单张或多张图片审核不成功
	function checknotpass()
	{
		$ids=$_POST['nopassid'];
	
		//审核不通过
		$image=M('upload_images_detail');
		$ids=explode(",",$ids);
		//file_put_contents('c:\\xxx.txt',var_export($ids,TRUE));
		for($i=0;$i<count($ids);$i++)
		{
			$id=$ids[$i];
	
			$id=trim($id);
			if(strlen($id)<=0 )
				continue;
	
			$sql='update pc_upload_images_detail set check_state=2 where id='.$id;
			$image->execute($sql);
		}
	
		$this->ajaxReturn('123','成功',1);
	}
	
	//添加热门图组
	function addhot()
	{
		$id=$_GET['id'];
		
		$g=M('group_detail');
		$where=" id=".$id;
		$glist=$g->where($where)->find();
		
		//找出封面图片的高宽
		$img=M('images_detail');
		$where=" group_id=".$id;
		$ilist=$img->where($where)->select();
		for($i=0;$i<count($ilist);$i++)
		{
			if( $ilist[$i]['small_url']==$glist['main_url'] )
			{
				$width=$ilist[$i]['width'];
				$height=$ilist[$i]['height'];
				break;
			}
		}
		
		$h=M('hot_picture');
		
		$data['group_id']=$id;
		$data['picturename']=$glist['title'];
		$data['url']=$glist['main_url'];
		$data['begintime']=time();
		$data['endtime']=time()+7*24*3600;
		$data['number']=1;
		$data['place']="热门图片展示";
		$data['width']=$width;
		$data['height']=$height;
		
		//序号先加一
		$sql="update pc_hot_picture set number=number+1";
		$h->execute($sql);
		
		$h->add($data);
		
		$this->success('添加成功');
	}
	
	//添加TOP图组
	function addtop()
	{
		$id=$_GET['id'];
		
		$g=M('group_detail');
		//$glist=$g->where(' id='.$id)->find();
		
		$data['is_top']=1;
		$g->where(' id='.$id)->save($data);
		
		$this->success('添加成功');
	}
	
	//添加图片TOP
	function setimagetop()
	{
		$id=$_POST['topimageid'];
		
		$img=M('images_detail');
		
		//查找Top
		$ilist=$img->where('id='.$id)->find();
		
		if( $ilist['is_top']==1 )
			$data['is_top']=0;
		else
			$data['is_top']=1;
		
		if($img->where('id='.$id)->save($data))
			$this->success('添加成功');
		else
			$this->success('添加失败');
	}
	
	//24小时热图
	function add24hot()
	{
		$id=$_GET['id'];
		
		$g=M('group_detail');
		$list=$g->where(' id='.$id)->find();
		if( $list['is_24hot']==0 )
			$data['is_24hot']=1;
		else
			$data['is_24hot']=0;
		
		if($g->where(' id='.$id)->save($data))
			$this->success('设置成功');
		else
			$this->success('设置失败');
	}
	
	//只保存组信息
	function savegroupinfo()
	{
		$id=$_GET['id'];
	
		$typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
	
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
	
		$m=M('column');
		if( strlen($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
		}
	
		if( strlen($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
		}
	
		if( strlen($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
		}
	
		//发生时间，发生地点
		$time=$_POST['gtime'];
		$location=$_POST['glocation'];
	
		//关键词
		$key=$_POST['gkey'];
	
		//标题
		$title=$_POST['gtitle'];
		$remark=$_POST['gremark'];
	
		//更新信息
		$group=M('group_detail');
		$data['type_one']=$typeone;
		$data['type_two']=$typetwo;
		$data['type_three']=$typethree;
	
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
	
		if(!empty($time))
			$data['time']=strtotime($time);
		//$data['location']=$location;
		$data['group_key']=$key;
		$data['title']=$title;
		$data['group_remark']=$remark;
		$where=" id='".$id."'";
		if(false==$group->where($where)->save($data))
		{
			$this->ajaxReturn('123',$group->getDbError(),0);
		}
		else
		{
			$this->ajaxReturn('123','更新成功',1);
		}
	}
	
	//保存图组中图片信息
	public function saveeditgroup()
	{
		$gid=$_GET['id'];
		$type=$_POST['subtype'];
	
		//找出该编辑图组中的图片信息
		$group=M('group_detail');
		$groupId=$gid;
	
		$image=M('images_detail');
		$where=" group_id=".$groupId;
		$imagelist=$image->where($where)->select();
	
		for($i=0;$i<count($imagelist);$i++)
		{
			$id=$imagelist[$i]['id'];
	
			//地区
			$country=$_POST['imgcountry'.$id];
			$province=$_POST['imgprovince'.$id];
			$city=$_POST['imgcity'.$id];
	
			//图片说明
			$remark=$_POST['imageremark'.$id];
	
			//key
			$key=$_POST['imagekey'.$id];
	
			//点数
			$point=$_POST['point'.$id];
	
			//人物姓名
			$rwname=$_POST['rwname'.$id];
	
			//艺名绰号
			$ymnick=$_POST['ymnick'.$id];
	
			//特写 半身 全身,0--全没选，1--特写 2--半身 3--全身
			$tx=0;
			if(!empty($_POST['txradio'.$id]))
			$tx=$_POST['txradio'.$id];
	
	
			//1人 2人 3人 合影，，0 1 2 3 4
			$hy=0;
			if( !empty($_POST['renradio'.$id]) )
			$hy=$_POST['renradio'.$id];
	
			//横图 竖图 方图  0,1,2,3
			$ht=0;
			if( !empty($_POST['htradio'.$id]) )
			$ht=$_POST['htradio'.$id];
	
			//保存
			$where=" id=".$id;
			$data['image_remark']=$remark;
			$data['images_key']=$key;
	
			$data['name']=$rwname;
	
			$data['yiming']=$ymnick;
			$data['texie']=$tx;
			$data['ren']=$hy;
			$data['position']=$ht;
			$data['value']=$point;
			$data['country']=$country;
			$data['province']=$province;
			$data['city']=$city;
	
			if($type==1)
			$data['state']=1;
	
			//file_put_contents('./zzz.txt',var_export($data,TRUE));
	
			$image->where($where)->save($data);
		}

		//刷新页面
		$this->index();
	}
	
	//移除编辑图组的图片，单张或多张
	function removeimg()
	{
		$ids=$_POST['removeid'];
		$ids=explode(",",$ids);
	
		$image=M('images_detail');
		for($i=0;$i<count($ids);$i++)
		{
			$id=$ids[$i];
	
			$id=trim($id);
			if(strlen($id)<=0 )
				continue;
	
			$sql="update  pc_images_detail set state=200 where id=".$id;
			$image->execute($sql);
		}
		
		//更新图组的数量
		$img=$image->where(' id='.$id)->find();
		$count=$img->where(' group_id='.$img[0]['group_id'])->count();
		
		$g=M('group_detail');
		$gdata['image_count']=$count;
		$g->where(' id='.$img[0]['group_id'])->save($gdata);
	
		$this->ajaxReturn('123','成功',1);
	}
	
	//删除整个图组
	function delgroup()
	{
		$id=$_GET['id'];
		
		$g=M('group_detail');
		$sql="update pc_group_detail set state=200 where id=".$id;
		$g->execute($sql);
		
		$this->ajaxReturn('123','成功',1);
	}
	
	//查找关键字
	function querykey()
	{
		$typename=$_POST['keyqueryname'];
	
		$t=M('keyword');
		$where=" type_name='".$typename."'";
		$tlist=$t->where($where)->select();
	
		$this->ajaxReturn($tlist,$tlist,1);
	}
	
	//旋转图片
	function imgRotation()
	{
		$id=$_POST['imgid'];
		$type=$_POST['rationtype'];
		
		$img=M('images_detail');
		$list=$img->where(' id='.$id)->find();
		
		if( $type==0 ) $degrees=90;
		else $degrees=-90;
		
		$filename=$list['url'];
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('url source','url source',0);
		
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
		
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,$filename,0);
		
		$filename=$list['big_url'];
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('big url source','big url source',0);
		
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
		
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,$filename,0);
		
		$filename=$list['small_url'];
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('small_url source:'.$filename,'small_url source:'.$filename,0);
		
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
		
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,$filename,0);
		
		$this->ajaxReturn($filename,$filename,1);
	}
}
?>