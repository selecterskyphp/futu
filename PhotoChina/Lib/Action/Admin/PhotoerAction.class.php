<?php
class PhotoerAction extends AdminbaseAction {

	public $dao;
	function _initialize() {
		parent::_initialize();
		$this->dao=D('Admin.photoer');
		if( empty($this->dao) )
			echo "photoer is null";
	}
	
	function index() {	
		
		if( false==$this->isHaveAuth('Photoer','index') )
			$this->error('对不起你没有该功能的权限');
		
		import ('@.ORG.Page');
		
		$mAccount=$_POST['account'];
		$mName=$_POST['name'];
		$mEmail=$_POST['email'];
		$mPhone=$_POST['phone'];
		$mProvince=$_POST['province'];
		$mCity=$_POST['city'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		$mState=$_POST['state'];
		
		$this->assign($_POST);
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account like '%".$mAccount."%' ";
			$and=" and ";
		}
		if( !empty($mName)){
			$where=$where.$and."name like '%".$mName."%' ";
			$and=" and ";
		}
		
		if( !empty($mEmail)){
			$where=$where.$and."email='".$mEmail."'";
			$and=" and ";
		}
		
		if( !empty($mPhone) ){
			$where=$where.$and."phone='".$mPhone."'";
			$and=" and ";
		}
		
		if( !empty($mProvince) ){
			$where=$where.$and."province='".$mProvince."'";
			$and=" and ";
		}
		
		if( !empty($mCity) ){
			$where=$where.$and."city='".$mCity."'";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$where = $where.$and."createtime>='".$mRegStart."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$where = $where.$and."createtime<='".$mRegEnd."'";
			$and=" and ";
		}
		if( !empty($mState) &&$mState!="全部" ){
			if($mState=="正常")
				$state=0;
			elseif($mState=="等待审核")
				$state=1;
			elseif($mState=="锁定")
				$state=2;
			elseif($mState=="已删除")
				$state=3;
			$where = $where.$and."state='".$state."'";
			$and=" and ";
		}
		
		$photoer=$this->dao;
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		
		//今日注册人数	昨天注册人数	前天注册人数	前七天注册人数	
		//前一月注册人数	总注册人数	总激活账号
		$curDate=date('Y-m-d 00:00:01',time());
		$where=" createtime>=".strtotime($curDate);
		$curCount=$photoer->where($where)->count(); //当天注册人数
		
		$yesDate=strtotime($curDate)-24*3600;
		$where=" createtime>=".$yesDate;
		$yesCount=$photoer->where($where)->count(); //昨天注册人数
		$yesCount=$yesCount-$curCount;
		
		$beforeDate=strtotime($curDate)-24*3600*2;
		$where=" createtime>=".$beforeDate;
		$beforeCount=$photoer->where($where)->count(); //前天注册人数
		$beforeCount=$beforeCount-$yesCount-$curCount;
		
		$before7Date=strtotime($curDate)-24*3600*7;
		$where=" createtime>=".$beforeDate;
		$before7count=$photoer->where($where)->count(); //前7天注册人数
		
		//前一个月注册人数
		$monthDay=strtotime("-1 month");
		$where=" createtime>=".$monthDay;
		$monthcount=$photoer->where($where)->count(); //前一个月注册人数
		
		//总注册人数
		$totalCount=$photoer->count();
		
		//总激活账号？？什么才叫激活账号?暂时全部激活
		$totalActive=$totalCount;
		
		$vo['today_reg_num']=$curCount;
		$vo['yer_reg_num']=$yesCount;
		$vo['before_reg_num']=$beforeCount;
		$vo['before7_reg_num']=$before7count;
		$vo['month_reg_num']=$monthcount;
		$vo['total_reg_num']=$totalCount;
		$vo['total_active_num']=$totalActive;
		
		$this->assign('vo',$vo);
		$this->assign('mlist',$list);
		$this->display();
	}
	
	function insert(){
		$photoer=$this->dao;
		if($data=$photoer->create()){
			if(false!==$photoer->add()){
				/*$uid=$photoer->getLastInsID();
				$ru['role_id']=$_POST['groupid'];
				$ru['user_id']=$uid;
				$roleuser=M('RoleUser');
				$roleuser->add($ru);*/
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($photoer->getError());
		}
	}
	
	function update(){
		//修改资料
		$uploadfilename="";

		import("@.ORG.UploadFile");    
		$upload = new UploadFile(); // 实例化上传类    
		$upload->maxSize  = 3145728 ; // 设置附件上传大小    
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型    
		$upload->savePath =  './Uploads/headimage/'; // 设置附件上传目录    
		$upload->$uploadReplace = true;

		if(!$upload->upload('',$_POST['head_image_path'])) { // 上传错误 提示错误信息    
			//$this->error($upload->getErrorMsg());    
		}else{ // 上传成功 获取上传文件信息    
			$info =  $upload->getUploadFileInfo(); 
			$uploadfilename=$info[0]["savename"];
		}    
		
		$photoer=$this->dao;
		if($data=$photoer->create()){
			if(!empty($data['id'])){
				$opw=$_POST['opwd'];
				$photoer->password=empty($photoer->password)?$opwd:sysmd5($photoer->password);
				if(false!==$photoer->save()){
					$this->success(L('edit_ok'));
				}else{
					$this->error(L('edit_error').$photoer->getDbError());
				}
			}else{
				$this->error(L('do_error'));
			}
		}else{
			$this->error($photoer->getError());
		}
	}
	
	function _before_add(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function _before_edit(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function delete(){
		$id=$_GET['id'];
		$photoer=$this->dao;
		if(false!==$photoer->delete($id)){
			$roleuser=M('RoleUser');
			$roleuser->where('user_id ='.$id)->delete();
			delattach(array('moduleid'=>0,'catid'=>0,'id'=>0,'userid'=>$id));
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$photoer->getDbError());
		}
	}
	
	function deleteall(){
		if($_POST['deletesubmit'])
		{
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids))
			{
				$photoer=M('Photoer');
				//$id=implode(',',$ids);
				for($i=0;$i<count($ids);$i++)
				{
					$date['id']=$ids[$i];
					$date['state']=3;
					dump($date);
					if(false==$photoer->save($date))
						$this->error(L('delete_error'));
				}
				$this->success(L('delete_ok'));
			}
			else{
				$this->error(L('do_empty'));
				}
			}
		
		elseif($_POST['2submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('Photoer');
				$id=implode(',',$ids);		
				for($i=0;$i<count($ids);$i++)
				{
					$date['id']=$ids[$i];
					$date['state']=2;
					if(false==$photoer->save($date))
						$this->error(L('锁定失败'));
				}
				$this->success(L('锁定成功'));
			}else{
				$this->error(L('do_empty'));
			
			}	
			
		}
		elseif($_POST['0submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('Photoer');
				$id=implode(',',$ids);
				for($i=0;$i<count($ids);$i++)
				{
					$date['id']=$ids[$i];
					$date['state']=0;
					if(false==$photoer->save($date))
						$this->error(L('激活失败'));
				}
				$this->success(L('激活成功'));
			}
			else
			{
				$this->error(L('do_empty'));	
			}
		}
	}
	
	//上传图片
	function upHeadImage() {
		print_r($_FILES['file']);
		
	}
}
?>