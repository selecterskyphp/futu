<?php

class PhotoerImageGroupAction extends AdminbaseAction {
	protected $dao;
	function _initialize()
	{

		parent::_initialize();
		//$this->dao=M('userrequire');
	}
	
	public function index() {

		if( false==$this->isHaveAuth('PhotoerImageGroup','index') )
			$this->error('对不起你没有该功能的权限');
		
		$this->display();
	}
	
	//图组编辑
	public function editGroup() {
		
		//刷新当前用户正在编辑的图组
		$username=$_SESSION['username'];
		
		//查找该用户正在编辑的图组
		$edGroup=M('editgroup');
		$where=" username='".$username."'";
		$vo=$edGroup->where($where)->find();
		if($vo==false)
		{
			$this->display('PhotoerImageGroup_groupnull');
			return;
		}
		
		//找出3种类型的名字
		$col=M('column');
		if(!empty($vo['type_one']) && $vo['type_one']>0 )
		{
			$where=" groupid=".$vo['type_one'];
			$nameone=$col->where($where)->find();
			$vo['type_one_name']=$nameone['name'];
		}
		if(!empty($vo['type_two']) && $vo['type_two']>0 )
		{
			$where=" groupid=".$vo['type_two'];
			$nametwo=$col->where($where)->find();
			$vo['type_two_name']=$nametwo['name'];
		}
		if(!empty($vo['type_three']) && $vo['type_three']>0 )
		{
			$where=" groupid=".$vo['type_three'];
			$namethree=$col->where($where)->find();
			$vo['type_three_name']=$namethree['name'];
		}
		
		$groupid=$vo['id'];
		
		$detail=M('editgroup_detail');
		$where=" group_id=".$groupid;
		$imagelist=$detail->where($where)->select();
		
		$this->assign('vo',$vo);
		$this->assign('imagelist',$imagelist);
		
		$this->display('PhotoerImageGroup_editGroup');
	}
	
	//查询
	public function query() {
		
		import ('@.ORG.Page');
		//查询
		$type=$_POST['type'];
		$username=$_POST['name'];
		$state=$_POST['state'];
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$name=$_POST['group_name'];
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
		
		$vo['filed']=$orderfiled;
		$vo['order']=$order;
		
		if(empty($state)) $state="等待审核";
		
		$group=M('upload_group_detail');
		$and="";
		if($type!=="全部" && !empty($type))
		{
			$where = " type='".$type."'";
			$and=" and ";
		}
		if( !empty($state) )
		{
			$ns=0;
			if( $state=="等待审核" ) $ns=0;
			else if( $state=="正常" ) $ns=2; //审核已通过
			else if( $state=="审核不通过" ) $ns=3;
			else if( $state=="已删除" ) $ns=10; //10为已删除
			else $ns=1000;
		
			if( $ns<1000)
			{
				if($ns==0)
					$where=$where.$and."(check_state=0 or check_state=1)";
				else
					$where=$where.$and."check_state=".$ns;
				$and=" and ";
			}
		}
		if( !empty($start) )
		{
			$where=$where.$and."up_time>= ".strtotime($start);
			$and=" and ";
		}
		if( !empty($end) )
		{
			$where=$where.$and."up_time<=".strtotime($end);
			$and=" and ";
		}
		if( !empty($name) )
		{
			$where=$where.$and."title='".$name."'";
		}
		
		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);

		$orderinfo=$orderfiled." ".$order;
		
		$list=$group->order($orderinfo)->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();

		//找出真实姓名
		for($i=0;$i<count($list);$i++)
		{
			//用户的真实姓名
			$acct=$list[$i]['up_account'];

			$p=M('photoer');
			$acct=$p->where("account='".$acct."'")->find();
			$list[$i]['check_oper']=$acct['name'];
		}
		
		$this->assign('mlist',$list);
		$this->assign('vo',$vo);
		
		$this->display('Photoerimagegroup_query');
	}
	
	//审核和删除所选图组
	function checkwithdel()
	{
		$ids=$_POST['ids'];
		$g=M('upload_group_detail');
		if($_POST['notchecked']) //审核不通过
		{ 
			for($i=0;$i<count($ids);$i++)
			{
				$data['check_state']=3;
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
		else if($_POST['delchecked']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['check_state']=10; //状态10为已删除
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
		
		$this->query();
	}
	
	//点开原始图组
	function editPhotoerGroup()
	{
		$groupId=$_GET['id'];
		
		//图组分类，创建时间，编辑时间
		$upGroup=M('upload_group_detail');
		$vo=$upGroup->where(' id='.$groupId)->find();
		
		$up=M('upload_images_detail');
		$imagelist=$up->where(' group_id='.$groupId)->select();
		
		$this->assign('vo',$vo);
		$this->assign('imagelist',$imagelist);
		
		$this->display();
	}
	
	//新建编辑图组
	function neweditgroup()
	{
		//创建该编辑的 编辑图组
		$username=$_SESSION['username'];
		$upgroupid=$_POST['groupid'];
		
		//清空之前的编辑图组
		//1.找出属于这个用户的GROUPid
		$eg=M('editgroup');
		$where =" username='".$username."'";
		$list=$eg->where($where)->find();
		if($list)
		{
			$groupid=$list['id'];
	
			//删除组下面的图片
			$where=" group_id=".$groupid;
			$img=M('editgroup_detail');
			$img->where($where)->delete();
			
			//删除图组
			$where=" id=".$groupid;
			$eg->where($where)->delete();
		}
	
		//取原图组的上传时间个上传人
		$ug=M('upload_group_detail');
		$ulist=$ug->where(' id='.$upgroupid)->find();
			
		//创建该用户的图组
		$data['username']=$username;
		$data['check_time']=time();
		$data['location']=$_POST['city'];
		$data['key']=$_POST['gkey'];
		$data['title']=$_POST['gtitle'];
		$data['remark']=$_POST['gremark'];
		$data['up_account']=$ulist['up_account'];
		$data['up_time']=$ulist['up_time'];
//file_put_contents('./xxx.txt',var_export($data,TRUE));
		file_put_contents('./xxx.txt',$upgroupid);
		if($eg->add($data))
			$this->ajaxReturn('123','成功',1);
		else
			$this->ajaxReturn('123','失败',0);
	}
	
	//该编辑没有图组
	function groupnull()
	{
		$username=$_SESSION['username'];
		
		$eg=M('editgroup');
		$where=" username='".$username."'";
		$glist=$eg->where($where)->find();
		
		if(false==glist)
		{
			//该编辑没有要编辑的图组
			$this->display('PhotoerImageGroup_groupnull');
		}
		else
		{
			//该编辑已经有在编辑的图组
			$this->editGroup();
		}
	}
	
	//复制单张或多张图片
	function copyimage()
	{
		$username=$_SESSION['username'];
		$ids=$_POST['copyid'];
		
		//判断该用户是否有编辑图组
		$eg=M('editgroup');
		$glist=$eg->where(" username='".$username."'")->find();
		if(false==$glist)
			$this->ajaxReturn('123','未建立编辑图组',0);
		
		$bisFace=true;
		if( strlen($glist['main_url'])<5 )
			$bisFace=false;
		

		//拷贝该图片信息到编辑图组
		$image=M('upload_images_detail');
		$ids=explode(",",$ids);
		for($i=0;$i<count($ids);$i++)
		{
			$id=$ids[$i];
			
			$id=trim($id);
			if(strlen($id)<=0 )
				continue;
			
			$where=" id=".$id;
			$ilist=$image->where($where)->find();
			if(false==$ilist)
				$this->ajaxReturn('123','查找编辑图组失败,id:'.$id,0);

			$gId=$ilist['group_id'];
			$data['group_id']=$glist['id'];
			$data['image_id']=$id;
			$data['up_time']=$ilist['createtime'];
			$data['url']=$ilist['url'];
			$data['big_url']=$ilist['big_url'];
			$data['small_url']=$ilist['small_url'];
			$data['is_main_face']=0;
			$data['points']=$ilist['point'];
			$data['remark']=$ilist['remark'];
			$data['up_acct']=$ilist['account'];
			$data['check_time']=time();
			if(!empty($ilist['key']))
				$data['key']=$ilist['key'];
			if(!empty($ilist['width']))
				$data['width']=$ilist['width'];
			if(!empty($ilist['height']))
				$data['height']=$ilist['height'];
			if(!empty($ilist['filesize']))
				$data['size']=$ilist['filesize'];
			if(!empty($ilist['country']))
				$data['country']=$ilist['country'];
			if(!empty($ilist['province']))
				$data['province']=$ilist['province'];
			if(!empty($ilist['city']))
				$data['city']=$ilist['city'];
	
			$edimage=M('editgroup_detail');
			if(false==$edimage->add($data))
				$this->ajaxReturn('123','添加图片失败',0);
		
			//把图片直接审核通过
			$upimage=M('upload_images_detail');
			$updata['check_state']=1;
			
			$upimage->where('id='.$id)->save($updata);	
			
			//判断图组是否有封面图片，如没有的话则默认第一张
			if($bisFace==false)
			{
				$dface['main_url']=$ilist['small_url'];
				$eg->where(' id='.$glist['id'])->save($dface);
				$bisFace=true;
			}
		}
		
		//更新整个图组的审核状态
		if($gId>0)
		{
			$count1=$image->where('check_state=0 and group_id='.$gId)->count();
			$count2=$image->where('check_state=1 and group_id='.$gId)->count();
			$count3=$image->where('check_state=2 and group_id='.$gId)->count();
			
			$gstate=0;
			if($count1<=0 && $count2<=0)
				$gstate=3;
			else if($count2<=0 && $count3<=0)
				$gstate=0;
			else if($count1<=0 && $count3<=0)
				$gstate=2;
			else if( $count2>0 && ($count3>0 || $count1>0) )
				$gstate=1;
			
			$gdata['check_state']=$gstate;
			
			$g=M('upload_group_detail');
			if(false==$g->where('id='.$gId)->save($gdata))
				;//$this->ajaxReturn('123',$gId,0);
		}
		
		$this->ajaxReturn('123','成功',1);
	}
	
	//单张或多张图片审核不成功
	function checknotpass()
	{
		$ids=$_POST['nopassid'];
		
		//审核不通过
		$image=M('upload_images_detail');
		$ids=explode(",",$ids);
		//file_put_contents('c:\\xxx.txt',var_export($ids,TRUE));
		for($i=0;$i<count($ids);$i++)
		{
			$id=$ids[$i];
		
			$id=trim($id);
			if(strlen($id)<=0 )
				continue;
			
			$sql='update pc_upload_images_detail set check_state=2 where id='.$id;
			$image->execute($sql);
		}
		
		$this->ajaxReturn('123','成功',1);
	}
	
	//移除编辑图组的图片，单张或多张
	function removeimg()
	{
		$ids=$_POST['removeid'];
		$ids=explode(",",$ids);
		
		$username=$_SESSION['username'];
		$eg=M('editgroup');
		$where=" username='".$username."'";
		$glist=$eg->where($where)->find();
		$groupId=$glist['id'];

		$image=M('editgroup_detail');
		for($i=0;$i<count($ids);$i++)
		{
			$id=$ids[$i];
		
			$id=trim($id);
			if(strlen($id)<=0 )
				continue;
			
			$sql="delete from pc_editgroup_detail where id=".$id;
			$image->execute($sql);
		}
		
		$this->ajaxReturn('123','成功',1);
	}
	
	//移除该编辑正在编辑的图组
	function delgroup()
	{
		$username=$_SESSION['username'];
		
		//找出编辑图组
		$eg=M('editgroup');
		$where=" username='".$username."'";
		$glist=$eg->where($where)->find();
		$groupId=$glist['id'];
		
		$image=M('editgroup_detail');
		$where=" group_id=".$groupId;
		$image->where($where)->delete();
		
		$where=" username='".$username."'";
		$eg->where($where)->delete();
		
		$this->ajaxReturn('123','成功',1);
	}
	
	//只保存组信息
	function savegroupinfo()
	{
		$username=$_SESSION['username'];
	
		$typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
		
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
		
		$m=M('column');
		if( strlen($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
		}
		
		if( strlen($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
		}
		
		if( strlen($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
		}
		
		//发生时间，发生地点
		$time=$_POST['gtime'];
		$location=$_POST['glocation'];
		
		//关键词
		$key=$_POST['gkey'];
		
		//标题
		$title=$_POST['gtitle'];
		$remark=$_POST['gremark'];
		
		//更新信息
		$group=M('editgroup');
		$data['type_one']=$typeone;
		$data['type_two']=$typetwo;
		$data['type_three']=$typethree;
		
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
		
		if(!empty($time))
			$data['time']=strtotime($time);
		$data['location']=$location;
		$data['key']=$key;
		$data['title']=$title;
		$data['remark']=$remark;
		$where=" username='".$username."'";
		if(false==$group->where($where)->save($data))
		{
			$this->ajaxReturn('123',$group->getDbError(),0);
		}
		else
		{
			$this->ajaxReturn('123','更新成功',1);
		}
	}
	
	//保存图组中图片信息
	public function saveeditgroup()
	{
		$type=$_POST['subtype'];
		$username=$_SESSION['username'];
		
		//找出该编辑图组中的图片信息
		$group=M('editgroup');
		$where=" username='".$username."'";
		$glist=$group->where($where)->find();
		$groupId=$glist['id'];
		
		$image=M('editgroup_detail');
		$where=" group_id=".$groupId;
		$imagelist=$image->where($where)->select();
		
		for($i=0;$i<count($imagelist);$i++)
		{
			$id=$imagelist[$i]['id'];
			
			//地区
			$country=$_POST['imgcountry'.$id];
			$province=$_POST['imgprovince'.$id];
			$city=$_POST['imgcity'.$id];
			
			//图片说明
			$remark=$_POST['imageremark'.$id];
			
			//key
			$key=$_POST['imagekey'.$id];
			
			//点数
			$point=$_POST['point'.$id];
			
			//人物姓名
			$rwname=$_POST['rwname'.$id];
			
			//艺名绰号
			$ymnick=$_POST['ymnick'.$id];
			
			//特写 半身 全身,0--全没选，1--特写 2--半身 3--全身
			$tx=0;
			if(!empty($_POST['txradio'.$id]))
				$tx=$_POST['txradio'.$id];
			
			
			//1人 2人 3人 合影，，0 1 2 3 4
			$hy=0;
			if( !empty($_POST['renradio'.$id]) )
				$hy=$_POST['renradio'.$id];
			
			//横图 竖图 方图  0,1,2,3
			$ht=0;
			if( !empty($_POST['htradio'.$id]) )
				$ht=$_POST['htradio'.$id];
			
			//保存
			$where=" id=".$id;
			$data['remark']=$remark;
			$data['key']=$key;
			$data['name']=$rwname;
			$data['yiming']=$ymnick;
			$data['texie']=$tx;
			$data['ren']=$hy;
			$data['position']=$ht;
			$data['points']=$point;
			$data['country']=$country;
			$data['province']=$province;
			$data['city']=$city;
			
			$image->where($where)->save($data);
		}
	
		if( $type==1 ) //保存后提交，即把该图组信息拷贝到正式表里
		{
			//拷贝图组信息
			$gdata['image_count']=count($imagelist);
			$gdata['check_time']=time();
			$gdata['check_oper']=$username;
			$gdata['main_url']=$glist['main_url'];
			$gdata['type_one']=$glist['type_one'];
			$gdata['type_two']=$glist['type_two'];
			$gdata['type_three']=$glist['type_three'];
			$gdata['type_one_name']=$glist['type_one_name'];
			$gdata['type_two_name']=$glist['type_two_name'];
			$gdata['type_three_name']=$glist['type_three_name'];
			
			$gdata['up_account']=$glist['up_account'];
			$gdata['up_time']=$glist['up_time'];
			
			$gdata['title']=$glist['title'];
			$gdata['group_key']=$glist['key'];
			$gdata['group_remark']=$glist['remark'];
			$gdata['state']=0;//0--已审核，1--已发布
			
			$eg=M('group_detail');
			$gId=$eg->add($gdata);
			
			//拷贝图像
			$eimg=M('images_detail');
			
			$where=" group_id=".$groupId;
			$imagelist=$image->where($where)->select();
			
			for($i=0;$i<count($imagelist);$i++)
			{
				$idata['group_id']=$gId;
				$idata['up_account']=$imagelist[$i]['up_acct'];
				$idata['up_time']=$imagelist[$i]['up_time'];
				$idata['check_time']=time();
				$idata['check_oper']=$_SESSION['username'];
				$idata['url']=$imagelist[$i]['url'];
				$idata['type_one']=$glist['type_one'];
				$idata['type_two']=$glist['type_two'];
				$idata['type_three']=$glist['type_two'];
				$idata['value']=$imagelist[$i]['points'];
				$idata['images_key']=$imagelist[$i]['key'];
				$idata['image_remark']=$imagelist[$i]['remark'];
				$idata['big_url']=$imagelist[$i]['big_url'];
				$idata['small_url']=$imagelist[$i]['small_url'];
				$idata['state']=0;
				
				$idata['name']=$imagelist[$i]['name'];
				$idata['yiming']=$imagelist[$i]['yiming'];
				$idata['texie']=$imagelist[$i]['texie'];
				$idata['ren']=$imagelist[$i]['ren'];
				$idata['position']=$imagelist[$i]['position'];

				$idata['country']=$imagelist[$i]['country'];
				$idata['province']=$imagelist[$i]['province'];
				$idata['city']=$imagelist[$i]['city'];
				
				$idata['width']=$imagelist[$i]['width'];
				$idata['height']=$imagelist[$i]['height'];
				$idata['filesize']=$imagelist[$i]['filesize'];
			
				$eimg->add($idata); //
			}
			
			//删除编辑图组
			$group->where(' id='.$groupId)->delete();
			
			//删除图像
			$image->where(' group_id='.$groupId)->delete();
		}
		
		//刷新页面
		$this->editGroup();
	}
	
	//设置封面图片
	function setface()
	{
		$id=$_POST['mainfaceid'];
		$username=$_SESSION['username'];
		
		$group=M('editgroup');
		//$where=" username='".$username."'";
		//$glist=$group->where($where)->find();
		
		$image=M('editgroup_detail');
		$where=" id=".$id;//" and group_id=".$glist['id'];
		$ilist=$image->where($where)->find();
		
		//$where=" id=".$glist['id'];
		$where=" username='".$username."'";
		$data['main_url']=$ilist['small_url'];
		$group->where($where)->save($data);

		$this->editGroup();
	}
	
	//查找关键字
	function querykey()
	{
		$typename=$_POST['keyqueryname'];
		
		$t=M('keyword');
		$where=" type_name='".$typename."'";
		$tlist=$t->where($where)->select();
		
		$this->ajaxReturn($tlist,$tlist,1);
	}
	
	//旋转图片
	function imgRotation()
	{
		$id=$_POST['imgid'];
		$type=$_POST['rationtype'];
	
		$img=M('editgroup_detail');
		$list=$img->where(' id='.$id)->find();
	
		if( $type==0 ) $degrees=90;
		else $degrees=-90;
	
		$filename=$list['url'];
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('url source','url source',0);
	
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
	
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,$filename,0);
	
		$filename=$list['big_url'];
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('big url source','big url source',0);
	
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
	
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,$filename,0);
	
		$filename=$list['small_url'];
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('small_url source:'.$filename,'small_url source:'.$filename,0);
	
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
	
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,$filename,0);
	
		$this->ajaxReturn($filename,$filename,1);
	}
}
?>