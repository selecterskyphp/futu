<?php
class topicimagegroupAction extends AdminbaseAction {

	function _initialize() {
		parent::_initialize();

	}

	function index() {

		import ('@.ORG.Page');
		
		if( false==$this->isHaveAuth('topicimagegroup','index') )
			$this->error('对不起你没有该功能的权限');
		
		//查找专题图组
		$typename=$_POST['type'];
		$title=$_POST['title'];
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$type=$_POST['type'];
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
		
		$vo['filed']=$orderfiled;
		$vo['order']=$order;
		
		$where=" is_topid=1 and state=1 ";
		if( !empty($title) )
			$where=$where." and title='".$title."'";
		if( !empty($start) )
			$where=$where." and createtime>='".strtotime($start)."'";
		if( !empty($end) )
			$where=$where." and createtime<='".strtotime($end)."'";
		if( !empty($type) && $type>100 )
		{
			$twhere=sprintf(" and ( (type_one>=%s and type_one<%s) or (type_two>=%s and type_two<=%s) or (type_three>=%s and type_three<=%s) )",
					$type,$type+999,
					$type,$type+999,
					$type,$type+999);
			$where=$where.$twhere;
		}
		
		$t=M('group_detail');
		$count=$t->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		$orderinfo=$orderfiled." ".$order;
		$glist=$t->order($orderinfo)->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		
		$this->assign('glist',$glist);
		$this->assign('vo',$vo);
		
		$this->display('Topicimagegroup_index');
	}
	
	function query()
	{
		import ('@.ORG.Page');
		
		$id=$_GET['id'];

		//��ѯ���ͨ�� ��δ����ͼ����Ϣ
		$typeid=$_POST['typeid'];
		$user=$_POST['username'];
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$groupname=$_POST['group_name'];
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
		
		$vo['filed']=$orderfiled;
		$vo['order']=$order;

		$group=M('group_detail');
		$where=" state=1 ";
		if( !empty($typeid) )
		{
			$twhere=sprintf(" and (type_one=%s or type_two=%s or type_three=%s)",$typeid,$typeid,$typeid);
			$where=$where.$twhere;
		}
		if( !empty($user) )
			$where=$where." and up_account='".$user."'";
	
		if( !empty($start) )
			$where=$where." and up_time>='".$start."'";

		if( !empty($end) )
			$where=$where.$and." and up_time<='".$end."'";

		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		$orderinfo=$orderfiled." ".$order;

		$glist=$group->order($orderinfo)->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		$this->assign('glist',$glist);
		$this->assign('tid',$id);
		$this->assign('vo',$vo);

		$this->display('Topicimagegroup_query');
	}
	
	//专题图组详细
	function editgroup()
	{
		$id=$_GET['id'];
		
		$t=M('group_detail');
		$vo=$t->where('id='.$id)->find();
		
	
		$tlist=$t->where(' topic_id='.$id)->select();
		
		$this->assign('tlist',$tlist);
		$this->assign('vo',$vo);
		
		$this->display();
	}
	
	//移动发布图组到专题图组
	function movegroup()
	{
		$ids=$_POST['ids'];
	}
	
	//
	function newtopic()
	{
		$typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
		
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
		
		$m=M('column');
		if( strlen($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
		}
		
		if( strlen($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
		}
		
		if( strlen($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
		}
		
		$title=$_POST['title'];
		$remark=$_POST['remark'];
		
		$data['title']=$title;
		$data['group_remark']=$remark;
		$data['group_count']=0;
		$data['type_one_id']=$typeone;
		$data['type_two_id']=$typetwo;
		$data['type_three_id']=$typethree;
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
		$data['check_time']=time();
		$data['release_time']=time();
		$data['up_time']=time();
		$data['state']=1;
		$data['is_topid']=1;
		$data['topic_id']=0;
		
		$t=M('group_detail');
		$t->add($data);
		
		$this->index();
	}
	
	//移动图组到专题图组
	function movetotopic()
	{
		$ids=$_POST['ids'];
		$id=$_GET['id'];
		
		$t=M('group_detail');
		for($i=0;$i<count($ids);$i++)
		{
			$data['topic_id']=$id;
			
			$t->where('id='.$ids[$i])->save($data);
		}
		
		$this->success('移动成功');
	}
	
	//保存专题图组信息
	function savegroupinfo()
	{
		$id=$_GET['id'];
		
		$title=$_POST['title'];
		$remark=$_POST['remark'];
		
		$t=M('group_detail');
		
		$data['title']=$title;
		$data['group_remark']=$remark;
		$data['type_one_id']=$_POST['typeoneid'];
		$data['type_two_id']=$_POST['typetwoid'];
		$data['type_three_id']=$_POST['typethreeid'];
		
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
		$typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
		$m=M('column');
		if( strlen($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
		}
		
		if( strlen($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
		}
		
		if( strlen($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
		}
		
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
		
		$t->where('id='.$id)->save($data);
		$this->success('保存成功');
	}
	
	//删除图组
	function deletegroup()
	{
		$ids=$_POST['ids'];
		$id=$_GET['id'];
		
		$t=M('group_detail');
		for($i=0;$i<count($ids);$i++)
		{
			$gid=$ids[$i];
			$data['topic_id']=0;
			$t->where(' id='.$gid)->save($data);
		}
		
		$this->success('删除成功');
	}
}

?>