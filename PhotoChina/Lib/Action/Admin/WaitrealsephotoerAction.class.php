<?php
class waitrealsephotoerAction extends AdminbaseAction {

	function _initialize() {
		parent::_initialize();
		
	}
	
	function index() {
		
		if( false==$this->isHaveAuth('waitrealsephotoer','index') )
			$this->error('对不起你没有该功能的权限');
		
		import ('@.ORG.Page');

		//查询审核通过 但未发布图组信息
		$typeid=$_POST['typeid'];
		$user=$_POST['username'];
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$groupname=$_POST['group_name'];
		$type=$_POST['type'];
		$state=$_POST['state'];
		
		if(empty($state)) $state="待发布";
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
		
		$vo['filed']=$orderfiled;
		$vo['order']=$order;

		$group=M('group_detail');
		
		if( !empty($state) )
		{
			$ns=0;
			if( $state=="待发布" ) $ns=0;
			else if( $state=="正常" ) $ns=1; //审核已通过
			else if( $state=="审核不通过" ) $ns=3;
			else if( $state=="已删除" ) $ns=10; //10为已删除
			else $ns=1000;
		
			if( $ns<1000)
				$where=$where.$and."state=".$ns;
		}
		
		if( !empty($type) && $type>100 )
		{
			$twhere=sprintf(" and ( (type_one>=%s and type_one<%s) or (type_two>=%s and type_two<=%s) or (type_three>=%s and type_three<=%s) )",
										$type,$type+999,
										$type,$type+999,
										$type,$type+999);
			$where=$where.$twhere;
		}
		if( !empty($user) )
			$where=$where." and up_account='".$user."'";
	
		if( !empty($start) )
			$where=$where." and up_time>=".strtotime($start);

		if( !empty($end) )
			$where=$where.$and." and up_time<=".strtotime($end);
		
		if ( !empty($groupname) )
			$where=$where.$and." and title='".$groupname."'";
		
		

		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		$orderinfo=$orderfiled." ".$order;
		$glist=$group->order($orderinfo)->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		$this->assign('glist',$glist);
		$this->assign('vo',$vo);

		$this->display('Waitrealsephotoer_index');
	}
	
	//修改未发布图组信息
	function checkgroup()
	{
		$id=$_GET['id'];
		if(!empty($id))
		{
			//查询该图组
			$g=M('group_detail');
			$where=" id=".$id;
			$vo=$g->where($where)->find();
			if(false!==$vo)
			{
				$this->assign('vo',$vo);
				
				//查询具体图片信息
				$image=M('images_detail');
				$where=" group_id=".$id." and state<200";
				$imglist=$image->where($where)->select();
				
				$this->assign('imglist',$imglist);
			}
		}
		
		$this->display('Waitrealsephotoer_checkgroup');
	}

	//保存修改的组信息
	function editgroup()
	{
		$groupId=$_POST['groupid'];
		
		$gtitle=$_POST['gtitle'];
		$gremark=$_POST['gremark'];
		
		$g=M('group_detail');
		$data['title']=$gtitle;
		$data['group_remark']=$gremark;
		
		if($g->where(' id='.$groupId)->save($data))
			$this->ajaxReturn($groupId,'保存成功',1);
		else
			$this->ajaxReturn($groupId,'保存失败',0);
	}
	
	//保存图组的修改信息 具体图片的信息
	function savegroup()
	{
		$id=$_GET['id'];
		
		$groupId=$_POST['groupid'];
		$img=M('images_detail');
		if(empty($id))
		{
			//保存全部
			$where=" group_id=".$groupId;
			$ilist=$img->where($where)->select();
			for($i=0;$i<count($ilist);$i++)
			{
				$imgId=$ilist[$i]['id'];
				
				//获取相关值
				$imgPoint=$_POST['point'.$imgId];
				$imgRemark=$_POST['iremark'.$imgId];
				$imgKey=$_POST['ikey'.$imgId];
				
				if(!empty($imgPoint))
					$data['value']=$imgPoint;
				if(!empty($imgRemark))
					$data['image_remark']=$imgRemark;
				if(!empty($imgKey))
					$data['images_key']=$imgKey;
				
				//
				$img->where('id='.$imgId)->save($data);	
			}
		}
		else {
			//只保存一个
			//获取相关值
			$imgPoint=$_POST['point'.$id];
			$imgRemark=$_POST['iremark'.$id];
			$imgKey=$_POST['ikey'.$id];
			
			if(!empty($imgPoint))
				$data['point']=$imgPoint;
			if(!empty($imgRemark))
				$data['image_remark']=$imgRemark;
			if(!empty($imgKey))
				$data['key']=$imgKey;
			
			$img->where('id='.$imgId)->save($data);
		}
		
		$this->success('保存成功');
	}
	
	//提交发布图组
	function submit()
	{
		$id=$_GET['id'];
		if(!empty($id))
		{
			$img=M('group_detail');
			
			$data['state']=1;
			$img->where('id='.$id)->save($data);
			
			$this->success('发布成功');
		}
		else
		{
			$this->error('请选择发布图组');
		}
	}
	
	//审核不通过及删除
	function checkwithdel()
	{
		$ids=$_POST['ids'];
		$g=M('group_detail');
		if($_POST['notchecked']) //审核不通过
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']=3;
				$g->where(' id='.$ids[$i])->save($data);
		}
		}
		else if($_POST['delchecked']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']=10; //状态10为已删除
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
		
		$this->index();
	}
	
	//设置封面图片
	function setface()
	{
		$id=$_POST['mainfaceid'];
	
		$image=M('images_detail');
		$where=" id=".$id;
		$ilist=$image->where($where)->find();
	
		$group=M('group_detail');

		$where=" id=".$ilist['group_id'];
		$data['main_url']=$ilist['small_url'];
		$group->where($where)->save($data);
		
		$glist=$group->where($where)->find();
		
		$imglist=$image->where(' group_id='.$ilist['group_id'])->select(); 
		
		$this->assign('glist',$glist);
		$this->assign('imglist',$imglist);
		
		//dump($imglist);
		
		//$this->display('Waitrealsephotoer_checkgroup');
		//$this->checkgroup();
		$this->redirect('Waitrealsephotoer/checkgroup',array('id'=>$ilist['group_id']));
	}
	
	//只保存组信息
	function savegroupinfo()
	{
		$id=$_GET['id'];

		$typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
	
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
	
		$m=M('column');
		if( strlen($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
		}
	
		if( strlen($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
		}
	
		if( strlen($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
		}
	
		//发生时间，发生地点
		$time=$_POST['gtime'];
		$location=$_POST['glocation'];
	
		//关键词
		$key=$_POST['gkey'];
	
		//标题
		$title=$_POST['gtitle'];
		$remark=$_POST['gremark'];
	
		//更新信息
		$group=M('group_detail');
		$data['type_one']=$typeone;
		$data['type_two']=$typetwo;
		$data['type_three']=$typethree;
	
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
	
		if(!empty($time))
			$data['time']=strtotime($time);
		//$data['location']=$location;
		$data['group_key']=$key;
		$data['title']=$title;
		$data['group_remark']=$remark;
		$where=" id='".$id."'";
		if(false==$group->where($where)->save($data))
		{
			$this->ajaxReturn('123',$group->getDbError(),0);
		}
		else
		{
			$this->ajaxReturn('123','更新成功',1);
		}
	}
	
	//保存图组中图片信息
	public function saveeditgroup()
	{
		$gid=$_GET['id'];
		$type=$_POST['subtype'];

		//找出该编辑图组中的图片信息
		$group=M('group_detail');
		$groupId=$gid;
	
		$image=M('images_detail');
		$where=" group_id=".$groupId;
		$imagelist=$image->where($where)->select();
	
		for($i=0;$i<count($imagelist);$i++)
		{
			$id=$imagelist[$i]['id'];
	
			//地区
			$country=$_POST['imgcountry'.$id];
			$province=$_POST['imgprovince'.$id];
			$city=$_POST['imgcity'.$id];
	
			//图片说明
			$remark=$_POST['imageremark'.$id];
	
			//key
			$key=$_POST['imagekey'.$id];
	
			//点数
			$point=$_POST['point'.$id];
	
			//人物姓名
			$rwname=$_POST['rwname'.$id];
	
			//艺名绰号
			$ymnick=$_POST['ymnick'.$id];
	
			//特写 半身 全身,0--全没选，1--特写 2--半身 3--全身
			$tx=0;
			if(!empty($_POST['txradio'.$id]))
			$tx=$_POST['txradio'.$id];
	
	
			//1人 2人 3人 合影，，0 1 2 3 4
			$hy=0;
			if( !empty($_POST['renradio'.$id]) )
			$hy=$_POST['renradio'.$id];
	
			//横图 竖图 方图  0,1,2,3
			$ht=0;
			if( !empty($_POST['htradio'.$id]) )
			$ht=$_POST['htradio'.$id];
	
			//保存
			$where=" id=".$id;
			$data['image_remark']=$remark;
			$data['images_key']=$key;
			
			$data['name']=$rwname;
			
			$data['yiming']=$ymnick;
			$data['texie']=$tx;
			$data['ren']=$hy;
			$data['position']=$ht;
			$data['value']=$point;
			$data['country']=$country;
			$data['province']=$province;
			$data['city']=$city;
			
			if($type==1)
				$data['state']=1;
			
			//file_put_contents('./zzz.txt',var_export($data,TRUE));
			
			$image->where($where)->save($data);
		}
		
		//file_put_contents('./aaa.txt','type:'.$type);
		if($type==1)
		{
			//发布图组
			$where=" id=".$groupId;
			$gdata['state']=1;
			
			$group->where($where)->save($gdata);
		}
	
		//刷新页面
		$this->index();
	}
	
	//移除编辑图组的图片，单张或多张
	function removeimg()
	{
		$ids=$_POST['removeid'];
		$ids=explode(",",$ids);
	
		$image=M('images_detail');
		for($i=0;$i<count($ids);$i++)
		{
			$id=$ids[$i];
	
			$id=trim($id);
			if(strlen($id)<=0 )
				continue;
	
			$sql="update  pc_images_detail set state=200 where id=".$id;
			$image->execute($sql);
		}
	
		$this->ajaxReturn('123','成功',1);
	}
	
	//查找关键字
	function querykey()
	{
		$typename=$_POST['keyqueryname'];
	
		$t=M('keyword');
		$where=" type_name='".$typename."'";
		$tlist=$t->where($where)->select();
	
		$this->ajaxReturn($tlist,$tlist,1);
	}
}
?>