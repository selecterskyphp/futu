<?php
if(!defined("YOURPHP")) exit("Access Denied");
class OrderAction extends BaseAction
{
    public function index()
    {	
		$buy = intval($_REQUEST['buy']);


		import("@.ORG.SysCrypt");
		$productid = $_REQUEST['productid'];
		$sc = new SysCrypt(C('ADMIN_ACCESS'));
		$catid = $_REQUEST['id'];

		$time=time();
		$_POST['userid'] = intval($this->_userid);
		$_POST['username'] = $_POST['username'] ? $_POST['username'] : $_COOKIE['YP_username'];
		$_POST['createtime'] = $time;
		$_POST['updatetime'] = $time;
		$_POST['ip'] = get_client_ip();
		
		$cart=$sc->php_decrypt($_COOKIE['yourphp_cart']);
		$cart=unserialize($cart);
		foreach((array)$cart['productlist'] as $key =>$rs){
			$cart['totalnum'] +=$rs['num'];
			$cart['totalprice'] += $rs['num']*$rs['price'];
			$cart['productlist'][$key]['countprice'] = $rs['num']*$rs['price'];
		}
		$_POST['price'] = $cart['totalprice'];
		$_POST['productlist'] = serialize($cart['productlist']);


 
		if($_POST['do']){
			
			if(!$this->_userid){
				if(empty($_POST['password'])) $this->success (L('do_empty'));
				$_POST['password'] = sysmd5($_POST['password']);
			}
			$model =M('Order');
			if (false === $model->create ()) {
				$this->error ( $model->getError () );
			}
			$orderid = $model->add();
			
			if ($orderid) {
				$sn =  str_replace("-","",date("Y-m-dH-i-s"));
				$sn = 'YP'.$sn. sprintf('%06d',$orderid); 				 
				$model->save(array(id=>$orderid,sn=>$sn));
			}else{
				$this->error ($model->getDbError());
			}

			cookie('yourphp_cart',null);
			$orderinfo = $model->find($orderid);
			$cart['sn']=$orderinfo['sn'];
			//$orderinfo['productlist'] = unserialize($orderinfo['productlist']);
		}

		$this->assign('cart',$cart);

		
		$p= max(intval($_REQUEST[C('VAR_PAGE')]),1);
		if(empty($catid)) $catid =  intval($_REQUEST['id']);		
		$cat = $this->categorys[$catid];
		$this->assign($cat);
		$this->assign('catid',$catid);
		$bcid = explode(",",$cat['arrparentid']); 
		$bcid = $bcid[1]; 
		if($bcid == '') $bcid=intval($catid);
		$this->assign('bcid',$bcid);
		$this->assign('buy',$buy);

        $this->display();
    }

	public function _before_insert(){
		$_POST['ip'] = get_client_ip();
	}

	public function ajax(){
		import("@.ORG.SysCrypt");
		$productid = $_REQUEST['productid'];
		$num = $_REQUEST['num'];
		$sc = new SysCrypt(C('ADMIN_ACCESS'));

		if($_REQUEST['del']){
			$cart=$sc->php_decrypt($_COOKIE['yourphp_cart']);
			$cart=unserialize($cart);
			unset($cart['productlist'][$productid]);
			$cart_str=$sc->php_encrypt(serialize($cart));
			cookie('yourphp_cart',$cart_str);
			unset($cart_str);
			echo json_encode($cart);exit;
		
		}

		if(!empty($_COOKIE['yourphp_cart'])){			
			$cart=$sc->php_decrypt($_COOKIE['yourphp_cart']);
			$cart=unserialize($cart);
		}else{
			$user=M('User')->find($this->_userid);
			$cart=array(
					'realname'=>$user['realname'],
					'tel'=>$user['tel'],
					'address'=>$user['address'],
					'email'=>$user['email'],
					'mobile'=>$user['mobile'],
					'fax'=>$user['fax'],
					'zipcode'=>$user['zipcode'],
					'productlist'=>array()
			);
			unset($user);			
		}
		$num = $num ? $num :1;
		if($productid){
			if(empty($cart['productlist'][$productid])){//cookie�޴�����
				$rs=M('Product')->find($productid);				
				$cart['productlist'][$rs['id']]=array('id'=>$rs['id'],'catid'=>$rs['catid'],'title'=>$rs['title'],'thumb'=>$rs['thumb'],'num'=>$num,'url'=>$rs['url'],'price'=>$rs['price']);
			}else{
				$cart['productlist'][$productid]['num'] =$num;
			}
		}
		$cart_str=$sc->php_encrypt(serialize($cart));
		cookie('yourphp_cart',$cart_str);
		unset($cart_str);
		echo json_encode($cart);exit;

	}
	public function show(){
		$this->assign('bcid',$bcid);
		$sn = $_REQUEST['sn'];
		$id= $_REQUEST['id'];
		$model =M('Order');
		$cart = $id ? $model->find($id) : $model->getBySn($sn) ;
		if(empty($cart)){
			  $this->error ( L('do_empty'));
		}
		$p=M($cart['module']);
		$cart['productlist']=unserialize($cart['productlist']);

		foreach((array)$cart['productlist'] as $key =>$rs){
			$cart['totalnum'] +=$rs['num'];
			$cart['totalprice'] += $rs['num']*$rs['price'];
			$cart['productlist'][$key]['countprice'] = $rs['num']*$rs['price'];
			$cart['productlist'][$key]['info']=$p->find($rs['id']);
		}
		$this->assign('cart',$cart);
		$this->display();		
	}

}
?>