<?php
class PhotoerAction extends BaseAction {
    protected $login;
    public function _initialize()
    {
        if(!isset($_SESSION['login']))
		{
		    $this->assign('jumpUrl',U('Index/index'));
			$this->error('您还没有登录或者登录超时，请重新登录');
		}
		//默认查询用户
		$this->login = $_SESSION['login'];
		$this->assign('login',$_SESSION['login']);
		$this->assign('begindate',time());
		$this->assign('enddate',time());
    }
    
	public function index()
	{
	    //import('@.ORG.Exif');//add cjx
        //$exif = new Exif();
        //$file = $exif->getInfo('basic','e:\300819_002.jpg');
        
        //var_dump($file);
       // exit;
	    $keyword = isset($_GET['keyword'])?trim($_GET['keyword']):'';
	    $startdate = isset($_GET['startdate'])?strtotime($_GET['startdate']):'';
	    $enddate = isset($_GET['enddate'])?strtotime($_GET['enddate']):'';
	    $where['up_account'] = $this->login['account'];
	    $where['check_state'] = array('neq',100);
	    if(strlen($keyword)>0)
	    {
	        $where['title']=array('like',$keyword);
	    }
	    if($startdate)
	    {
	        $where['createtime'] = array('gt',$startdate);
	    }
	    if($enddate)
	    {
	        $where['createtime'] = array('lt',$enddate);
	    }
	    $mod = M('upload_group_detail');
	    import ( '@.ORG.Page' );
	    
	    $count=$mod->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show_li();
		$this->assign("page",$show);
		$list=$mod->order('id desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('mlist',$list);
		//var_dump($list);
		$this->assign('color',C('IMAGE_STATUS_COLOR'));
		//var_dump(C('IMAGE_STATUS_COLOR'));
		$this->display('Photoer_index');
	}
	
    public function imageslist()
	{
	    $keyword = isset($_GET['keyword'])?trim($_GET['keyword']):'';
	    $begindate = isset($_GET['begindate'])?strtotime($_GET['begindate']):'';
	    $enddate = isset($_GET['enddate'])?strtotime($_GET['enddate']):'';
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $where['account'] = $this->login['account'];
	    if($id > 0 )
	    {
	        $where['group_id'] = $id;
	    }
	    if(strlen($keyword)>0)
	    {
	        $where['title']=array('like','%'.$keyword.'%');
	        $this->assign('keyword',$keyword);
	    }
	    
	    if($begindate>0)
	    {
	        $where['createtime'] = array('gt',$begindate);
	        $begin = $begindate;
	        $this->assign('begindate',$begindate);
	    }
	    else 
	    {
	        $begin = 0;
	    }
	    if($enddate>0)
	    {
	        $end = $enddate;
	        $this->assign('enddate',$enddate);
	    }
	    else 
	    {
	        $end = 0;
	    }
	    if($begin == $end && $begin != 0)
	    {
	        $where['createtime'] = $begin;
	    }
	    else 
	    {
    	    if($end == 0)
    	    {
    	        $end = time();
    	    }
    	    $where['createtime']=array('between',array($begin,$end));
	    }
	    $mod = M('upload_images_detail');
	    import ( '@.ORG.Page' );
	    $count=$mod->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show_li();
		$this->assign("page",$show);
		$list=$mod->order('id desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//var_dump($mod->getLastSql());
		//exit;
		$this->assign('mlist',$list);
		$this->assign('color',C('IMAGE_STATUS_COLOR'));
		$this->display();
	}
	
	//查询修改资料
	public function edit()
	{
		$this->display();
	}
	
	//基本信息更新
	public function update()
	{
		
		//修改资料
		$uploadfilename="";
		
		import("@.ORG.UploadFile");
		$upload = new UploadFile(); // 实例化上传类
		$upload->maxSize  = 3145728 ; // 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
		$upload->savePath =  C('HEAD_IMAGES_PATH'); // 设置附件上传目录
		$upload->$uploadReplace = true;
		
		if(!$upload->upload('',$_POST['head_image_file'])) { // 上传错误 提示错误信息
			//$this->error($upload->getErrorMsg());
		}else{ // 上传成功 获取上传文件信息
			$info =  $upload->getUploadFileInfo();
			$_POST['head_image_path'] = $upload->savePath . '/' . $info[0]["savename"];
		}
		
		$photoer=M('Photoer');
		$_POST['id'] = $this->login['id'];
	    if(false!==$photoer->save($_POST))
		{
			$data = $photoer->getById($_POST['id']);
			$this->setLogin($data, 1);
			$this->success(L('修改成功'));
		}
		else
		{
			$this->error(L('edit_error').$photoer->getDbError());
		}
	}
	
	//修改密码
	public function editPwd()
	{
		$this->display();
	}
	
	//更新密码
	public function updatePwd()
	{
		$user=$this->login['account'];
		$photoer=M('Photoer');
		
		$oldpwd=$_POST['oldpwd'];
		$newpwd=$_POST['newpwd'];
		
		//判断老密码是否对
		if(sysmd5($oldpwd) != $this->login['password'])
		{
		    $this->error('老密码输入错误');
		}
		//校验通过
		$data['password']=sysmd5($newpwd);
		$contidition['account']=$user;
		if( $photoer->where($contidition)->save($data) )
		{
		    $_SESSION['login']['password'] = $data['password'];
			$this->success(L('修改成功!'));
		}
		else
			$this->error($photoer->getError());		
	}
	
	public function upimage()
	{	
		$this->display();
	}
	
	public function upImageOne()
	{
		$user=$this->login['account'];
		$photoer=M('Photoer');
		$where="account='".$user."'";
		$ilist=$photoer->where($where)->find();
		
		//插入组信息
		$data['title']=$_POST['title'];
		$data['up_account']=$user;
		$data['up_time']=time();
		$data['type']=$_POST['type'];
		$mode = isset($_POST['mode'])?intval($_POST['mode']):0;
		if(0 == $mode)
		{
    		$data['key']=$_POST['key'];
    		$data['remark']=$_POST['remark'];
    		$data['photo_date']=strtotime($_POST['date']);
    		//将国家省份存入session中，方便添加图片时使用
    		$_SESSION['country'] = isset($_POST['country'])?trim($_POST['country']):'';
    		$_SESSION['province'] = isset($_POST['province'])?trim($_POST['province']):'';
    		$_SESSION['city'] = isset($_POST['city'])?trim($_POST['city']):'';
    		$_SESSION['photo_time'] = isset($_POST['photo_time'])?strtotime($_POST['photo_time']):time();
    		if('中国' != trim($_POST['country']))
    		{
    		    $_SESSION['country'] = isset($_POST['country1'])?trim($_POST['country1']):'';
        		$_SESSION['province'] = isset($_POST['province1'])?trim($_POST['province1']):'';
        		$_SESSION['city'] = isset($_POST['city1'])?trim($_POST['city1']):'';
    		}
		}
		else 
		{
		    //自动上传则清用上次用的国家省份
		    unset($_SESSION['country'],$_SESSION['province'],$_SESSION['city']);
		}
		$data['check_state']=100;//状态100表示保存还未提交
		
		$group=M('upload_group_detail');
		$group->add($data);
		$groupId=$group->getLastInsID() ;
		$this->assign("groupid",$groupId);
		$this->assign("upload_mode",$mode);
		$this->display();
	}
	
	//只负责文件的上传
	public function upImageTwo()
	{	
		$user=$this->login['account'];
	    
		$groupId=isset($_GET['groupid'])?intval($_GET['groupid']):0;
		$type = isset($_GET['type'])?intval($_GET['type']):0;
		$mode = isset($_GET['mode'])?intval($_GET['mode']):0;
		
		$photoer=M('Photoer');
		
		$vo['gourpid']=$groupId;
	
		import("@.ORG.UploadFile");
		import("@.ORG.Image");
		$upload = new UploadFile(); // 实例化上传类
		$upload->maxSize  = 31457280 ; // 设置附件上传大小
		$upload->allowExts  = array('jpg', 'jpeg', 'psd'); // 设置附件上传类型
		$upload->savePath =  C("IMAGES_PATH").$this->login['id'].'/'.date('Ymd',time()).'/'; // 设置附件上传目录
		//var_dump($upload->savePath);
		//exit;
		@mkdir(C("IMAGES_PATH").$this->login['id']);
		@mkdir(C("IMAGES_PATH").$this->login['id'].'/'.date('Ymd',time()));
		$upload->$uploadReplace = true;
		 //设置需要生成缩略图，仅对图像文件有效 
        $upload->thumb = true; 
		
		 //设置需要生成缩略图的文件后缀 
        $upload->thumbPrefix = 'm_,s_';  //生产2张缩略图 
        $upload->thumbPath = C('THUMB_PATH').$this->login['id'].'/'.date('Ymd',time()).'/';  //缩略图 保存路径
        @mkdir(C('THUMB_PATH'));
        @mkdir(C("THUMB_PATH").$this->login['id']);
		@mkdir(C("THUMB_PATH").$this->login['id'].'/'.date('Ymd',time()));
        //设置缩略图最大宽度 
        $upload->thumbMaxWidth = '650,160'; 
        //设置缩略图最大高度 
        $upload->thumbMaxHeight = '65000,16000'; 
	
		if(!$upload->uploadmany($user)) { // 上传错误 提示错误信息
		    $this->ajaxReturn('',$upload->getErrorMsg(),1);
		}else
		{ // 上传成功 获取上传文件信息
			$info =  $upload->getUploadFileInfo();
			//var_dump($info);
			//先查询组信息
			$tGroup=M('upload_group_detail');
			$tGroupinfo = $tGroup->where('id='.$groupId)->find();
			if(1 == $mode)
			{
			    //如果是自动 上传，则自动 添加组的描述信息
			    if(!$tGroupinfo['title'])
			    {
			        $gtitle = isset($info[0]['exif']['iptcBigTitle'][1])?$info[0]['exif']['iptcBigTitle'][1]:'';
    			    $gremark = isset($info[0]['exif']['iptcDesc'][1])?$info[0]['exif']['iptcDesc'][1]:'';
    			    $gcity = isset($info[0]['exif']['iptcCity'][1])?$info[0]['exif']['iptcCity'][1]:'';
    			    $dataG['title'] = str_replace('\'', '', $gtitle);
    			    $dataG['remark'] = str_replace('\'', '', $gremark);
    			    $dataG['city'] = str_replace('\'', '', $gcity);
    			    $tGroup->data($dataG)->where('id='.$groupId)->save();
			    }
			    //$this->ajaxReturn('',$tGroup->getLastSql(),1);
			    //var_dump($tGroup->getLastSql());
			    //exit;
			}
			else 
			{
			    //批量上传 读取 session里面的省份城市
			    $data['country'] = isset($_SESSION['country'])?$_SESSION['country']:'';
			    $data['province'] = isset($_SESSION['province'])?$_SESSION['province']:'';
			    $data['city'] = isset($_SESSION['city'])?$_SESSION['city']:'';
			    $data['photo_time'] = isset($_SESSION['photo_time'])?$_SESSION['photo_time']:time();
			}
			//文件上传成功后进行入库
			$upfiles=M('upload_images_detail');
			$data['group_id']=$groupId;
			$data['account']=$user;
			$data['createtime']=time();
			$data['type'] = (0 == $type)?'时效图片':'资料图片';
			
			$vo['count']=count($info);
			for($i=0;$i<count($info);$i++)
			{
				$data['filename']=$info[$i]["savename"];
				$data['url']=$info[$i]['savepath'].$info[$i]["savename"];
				$data['big_url']=$info[$i]['thumbPath'].'m_'.$info[$i]['savename'];
				$data['small_url']=$info[$i]['thumbPath'].'s_'.$info[$i]['savename'];				
				$imgInfo=Image::getImageInfo($info[$i]['savepath'].$info[$i]["savename"]);
				
				$data['width']=$imgInfo['width'];
				$data['height']=$imgInfo['height'];
				$data['filesize']=ceil($imgInfo['size']/1024);
				
				if(1 == $mode)
				{
				    //自动 上传
				    $data['country'] = isset($info[$i]['exif']['iptcCountry'][1])?str_replace('\'', '', $info[$i]['exif']['iptcCountry'][1]):'';
				    $data['province'] = isset($info[$i]['exif']['iptcProvince'][1])?str_replace('\'', '', $info[$i]['exif']['iptcProvince'][1]):'';
				    $data['city'] = isset($info[$i]['exif']['iptcCity'][1])?str_replace('\'', '', $info[$i]['exif']['iptcCity'][1]):'';
				    //分类 
				    $data['cate'] = isset($info[$i]['exif']['iptcCate'][1])?str_replace('\'', '', $info[$i]['exif']['iptcCate'][1]):'';
				    $data['remark'] = isset($info[$i]['exif']['iptcTitle'][1])?str_replace('\'', '', $info[$i]['exif']['iptcTitle'][1]):'';
				    if(strlen($data['remark'])<2)
				    {
				        //如果描述信息 获取失败 则获取大标题的信息$_SESSION['photo_time']
				        $data['remark'] = isset($info[0]['exif']['iptcBigTitle'][1])?str_replace('\'', '',$info[0]['exif']['iptcBigTitle'][1]):'';
				    }
				    $data['photo_time'] = isset($info[$i]['exif']['DateTimeOriginal'][1])?strtotime($info[$i]['exif']['DateTimeOriginal'][1]):0;
				    if(0 == $data['photo_time'])
				    {
				        $data['photo_time'] = time();
				    }
				    $data['key'] = isset($info[$i]['exif']['iptcKeyword'][1])?str_replace('\'', '', $info[$i]['exif']['iptcKeyword'][1]):'';
				    $data['author'] = isset($info[$i]['exif']['iptcAuth'][1])?str_replace('\'', '', $info[$i]['exif']['iptcAuth'][1]):'';
				    if(strlen($data['author'])<2)
				    {
				        //如果IPTC的作者获取失败，则获取exif的作者
				        $data['author'] = isset($info[$i]['exif']['Artist'][1])?str_replace('\'', '', $info[$i]['exif']['Artist'][1]):'';
				    }
				}
				//var_dump($data,$info[$i]['exif']);
				//exit;
				$id=$upfiles->add($data);
				if($id==false)
				{
					$this->ajaxReturn('',$upfiles->getLastSql(),2);
				}	
					
			}
		}
        $this->ajaxReturn('','11',0);
        exit;
	}
	
	//下一步
	function nextstep()
	{
		$user=$this->login['account'];
		$groupid=isset($_GET['groupid'])?intval($_GET['groupid']):0;
		$mod = M('upload_images_detail');
		$modg = M('upload_group_detail');
		$where['account']=$user;
		$where['group_id']=$groupid;
		$vlist = $mod->where($where)->select();
		$ginfo = $modg->getByid($groupid);
		//var_dump($vlist,$mod->getLastSql());
		//exit;
		$this->assign('vlist',$vlist);
		$this->assign('ginfo',$ginfo);
		$this->assign('groupid',$groupid);
		$this->display('Photoer_upImageTwo');
	}
	
	//保存图片说明
	public function imagesave()
	{
		$ids = $_POST['ids'];
		$groupid=isset($_GET['groupid'])?intval($_GET['groupid']):0;
		$upImage=M('upload_images_detail');
		if(is_array($ids))
		{
		    foreach ($ids as $v)
		    {
		        $data = array();
		        $data['remark']=isset($_POST['remark'.$v])?str_replace('\'', '', trim($_POST['remark'.$v])):'';
		        //$data['photo_time']=isset($_POST['photo_time'.$v])?strtotime(trim($_POST['photo_time'.$v])):time();
		        //$data['city']=isset($_POST['city'.$v])?str_replace('\'', '', trim($_POST['city'.$v])):'';
		        //$data['point']=isset($_POST['point'.$v])?intval($_POST['point']):100;
		        $data['point'] = 100;
		        unset($where);
		        $where['id']=$v;
		        $upImage->where($where)->save($data);
		    }
		}
		$count = count($ids);
		$v = isset($_POST['main'])?intval($_POST['main']):$ids[0];
		$main_url = isset($_POST['img'.$v])?trim($_POST['img'.$v]):'';
		$city = isset($_POST['city'.$v])?trim($_POST['city'.$v]):'';
		$gedit = isset($_POST['gedit'])?intval($_POST['gedit']):0;
		unset($gdata);
		//if(1 === $gedit && isset($_POST['gtitle']) && isset($_POST['gremark']))
		//{
		    $gdata['title']=str_replace('\'', '', $_POST['gtitle']);
		    $gdata['remark']=str_replace('\'', '', $_POST['gremark']);
		//}
		//保存图片说明后 直接修改图组的状态
	
		$upGroup=M('upload_group_detail');
		$gdata['check_state']=0;
		$gdata['image_num']=$count;
		$gdata['main_url']=$main_url;
		if(strlen($city)>1)
		{
		    $gdata['city']=$city;
		}
		$upGroup->where(' id='.$groupid)->save($gdata);
		
		//删除没有上传完成的图组
		$upGroup->where('check_state=100 and up_account=\''.$this->login['account'].'\'')->delete();
		
		//设置缓存所有已经上传的图片总数
		$mod  = M('upload_images_detail');
		unset($where);
		$where['account']=$this->login['account'];
		$count = $mod->where($where)->count();
		$_SESSION['login']['pic_count']=$count;
		
		$this->assign('jumpUrl',U('Photoer/index'));
		$this->success('操作成功');
	}
	
	//销售记录
	public function selldetail()
	{
		$user=$this->login['account'];
		$photoer=M('Photoer');
		$where="account='".$user."'";
		$ilist=$photoer->where($where)->select();
		
		//返回用户名(或昵称)
		$vo['nick']=$ilist[0]['name'];
		$vo['head_image_path']=$ilist[0]['head_image_path'];
		
		//统计该用户总共上传了多少张图片
		$upDetail=M('upload_images_detail');
		$where="account='".$user."'";
		$upCount=$upDetail->where($where)->count();
		$vo['up_count']=$upCount;
		
		$this->assign('vo',$vo);
		/*
		//id,uptime tpoints times tjifen,lasttime
		$tsDetail=M('sell_detail');
		
		$sql="select image_id as id, sum(points) as tpoint,count(*) as times, ";
		$sql=$sql."sum(jifen) as tjifen,FROM_UNIXTIME(max(trade_time),'%Y-%m-%d %H:%i:%s') as lasttime,";
		$sql=$sql." FROM_UNIXTIME(max(image_up_time),'%Y-%m-%d') as uptime,max(url) as url ";
		$sql=$sql."from pc_trade_detail WHERE seller='".$user."' group by image_id ";
		
		$slist=$tsDetail->query($sql);		
		for($i=0;$i<count($slist);$i++)
			$slist[$i]['id']=sprintf("%09d",$slist[$i]['id']);
		$this->assign('slist',$slist);
		*/
		//销售情况
		$selldetail=M('buy_images');
		$where="up_account = '".$this->login['account']."' order by id desc";
		$slist=$selldetail->where($where)->select();
		$this->assign('slist',$slist);
	
				
		//查询用户下载图片的数量
		
		$down_picture=M('images_detail');
		$username=$this->login['account'];
		
		$where="up_account = '".$username."' order by down_number desc";
		$down_info=$down_picture->where($where)->find();
		$this->assign('down_info',$down_info);
		
		$this->display();
	}
	
	public function creditinfo() {
		$user=$this->login['account'];
		$photoer=M('Photoer');
		$where="account='".$user."'";
		$ilist=$photoer->where($where)->select();
		
		//返回用户名(或昵称)
		$vo['nick']=$ilist[0]['name'];
		$vo['head_image_path']=$ilist[0]['head_image_path'];
		
		//统计该用户总共上传了多少张图片
		$upDetail=M('upload_images_detail');
		$where="account='".$user."'";
		$upCount=$upDetail->where($where)->count();
		$vo['up_count']=$upCount;
		
		$this->assign('vo',$vo);
		
		$credit=M('credit_detail');
		$where=" account='".$user."' order by id desc";
		$clist=$credit->where($where)->select();
		
		for($i=0;$i<count($clist);$i++)
		{
			$clist[$i]['send_time']=date('Y-m-d',$clist[$i]['send_time']);
			$clist[$i]['pay_time']=date('Y-m-d',$clist[$i]['pay_time']);
		}
		$this->assign('clist',$clist);
		//查询用户下载图片的数量
		
		$down_picture=M('images_detail');
		$username=$this->login['account'];
		
		$where="up_account = '".$username."' order by down_number desc";
		$down_info=$down_picture->where($where)->find();
		$this->assign('down_info',$down_info);
		
		$this->display();
	}
	
	public function siteInfo() {
		
		$user=$this->login['account'];
		
		$photoer=M('Photoer');
		$where="account='".$user."'";
		$ilist=$photoer->where($where)->select();
		
		//返回用户名(或昵称)
		$vo['nick']=$ilist[0]['name'];
		$vo['head_image_path']=$ilist[0]['head_image_path'];
		
		//统计该用户总共上传了多少张图片
		$upDetail=M('upload_images_detail');
		$where="account='".$user."'";
		$upCount=$upDetail->where($where)->count();
		$vo['up_count']=$upCount;
		
		$this->assign('vo',$vo);
		//查询用户下载图片的数量
		
		
		$down_picture=M('images_detail');
		$username=$this->login['account'];
		
		$where="up_account = '".$username."' order by down_number desc";
		$down_info=$down_picture->where($where)->find();
		$this->assign('down_info',$down_info);
		
		$this->display();
	}
	
	//下载单张图片
	public function download() {
		$id=$_GET['id'];
		if(!empty($id)) {
			$image=M('upload_images_detail');
			$where=" id=".$id;
			$list=$image->where($where)->select();
			
			if($list)
			{
				import("@.ORG.Http");
				
				$down=new Http();
				$down->download("./".$list[0]['url'],$list[0]['oldfilename']);
			}
		}
	}
	
	private function delDir($dir) {
	
		if( is_dir($dir) ) {
			if( $dh=opendir($dir) ) {
				while( ($file=readdir($dh))!==false ) {
					if( is_file($dir.$file) ) {
						unlink($dir.$file);
						$n++;
					}
				}
	
				closedir($dh);
			}
		}
	}
	
	//图组下载
	public function groupdownload() {
		
		$id=$_GET['id'];

		//
		$group=M('upload_images_detail');
		$where=" group_id=".$id;
		$ilist=$group->where($where)->select();
		if($ilist) {
			
			//删除源文件夹
			$this->delDir("./temp/zip/");
 	
			//把这些文件拷贝到一个临时文件夹
			for($i=0;$i<count($ilist);$i++) {
				

				$source="./".$ilist[$i]['url'];
				 $dest="./temp/zip/".$ilist[$i]['filename'];
			
				copy($source,$dest);
					
			}
			
			//拷贝完成后再对整个文件夹进行压缩并下载
			import("@.ORG.Phpzip");
			$gdown=new Phpzip();
			$gdown->ZipAndDownload("./temp/zip");
		}

	}
}

?>