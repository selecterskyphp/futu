<?php
class MainAction extends BaseAction
{
    public function index()
    {				
		$_SESSION[recordplace]=100;	
					
	 	if($_SESSION[login][role]=="游客"||$_SESSION[login][role]=="下载用户"){
			$userinfo=M('photoer');
			$where_one="type_style=1 and state=0";
			$where_two="type_style=0 and state=0";
			$user_one=$userinfo->where($where_one)->select();
			$user_two=$userinfo->where($where_two)->select();
			$this->assign('user_two',$user_two);
			$this->assign('user_one',$user_one);
		}
		if($_SESSION[login][role]=="摄影师"){
			if($_SESSION[login][type_style]==1){
				
				$userinfo=M('photoer');
				$where_one="account= '".$_SESSION[login][account]."'";
				$user_one=$userinfo->where($where_one)->select();
				$this->assign('user_one',$user_one);
					
			}
			else{
				$userinfo=M('photoer');
				$where_two="account='".$_SESSION[login][account]."'";
				$user_two=$userinfo->where($where_two)->select();
				
				$this->assign('user_two',$user_two);
				
				}
				
		}
		
		//判断是否选中显示多少组
		if(!$_POST[selectvalue]){
			
			if(!$_SESSION[page][currentshowpages])	
				$_POST[selectvalue]=20;
			else
				$_POST[selectvalue]=$_SESSION[page][currentshowpages];
		}
		//导入page类
		import('@.ORG.Page');
		$user=M('group_detail');
		if($_SESSION[login][role]=="游客"){
			$curtime=time();
			$time7=$curtime-7*24*3600;
			$where="state=1 and up_time<".$time7;
		}
		else
			$where="state=1";	
		
		$count=$user->where($where)->count();
		//总共多少页
		$_SESSION[page][pagecount]=ceil($count/$_POST[selectvalue]);
		
		$page=new Page($count,$_POST[selectvalue]);
		$list=$user->order('up_time  desc')->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		$count=count($list);
		//显示组图片的地点
		for($i=0;$i<$count;$i++){
			$imageswhere="group_id=".$list[$i]['id'];
			$imageinfo=M('images_detail');
			$voimage=$imageinfo->where($imageswhere)->find();
			$list[$i]['city']=$voimage['city'];
			
			$userwhere="account='".$list[$i]['up_account']."'";
			
			$user=M('photoer');
			$username=$user->where($userwhere)->find();
			$list[$i]['name']=$username['name'];
			}
		
		$this->assign('list',$list);
		$_SESSION[page][currentshowpages]=$_POST[selectvalue];
		
		if($_GET[p])
			$_SESSION[page][curpage]=$_GET[p];
		else
			$_SESSION[page][curpage]=1;	
		$show=$page->show();
		$this->assign('page',$show);
		
		//默认搜索图片库的二级栏目
		$decidestype=M('Column');
		$where="parentid=1000";
		
		$wherecolumn="parentid=1100";
		
		$menu=$decidestype->where($where)->select();
		$column=$decidestype->where($wherecolumn)->select();
		
		$_SESSION[menuurl]="index.php?m=Main&a=decide&t=1";
		$_SESSION[columnurl]="index.php?m=Main&a=decide&t=1&q=1100";
		$_SESSION[first]="图片库";
		
		$this->assign('menu',$menu);
		$this->assign('column',$column);
		//默认搜素图片库片下的 时事的三级栏目
		$thirdwhere="parentid=1100";
		$thirdcolumn=$decidestype->where($thirdwhere)->select();
		$_SESSION[currentcloumn]=0;
	
		$this->assign('thirdcolumn',$thirdcolumn);
		$_SESSION[type_one]=1;
		
        $this->display();
    }
	
	
  
	///////decide
	public function decide(){
		
		if($_SESSION[login][role]=="游客"||$_SESSION[login][role]=="下载用户"){
			$userinfo=M('photoer');
			$where_one="type_style=1 and state=0";
			$where_two="type_style=0 and state=0";
			$user_one=$userinfo->where($where_one)->select();
			$user_two=$userinfo->where($where_two)->select();
			$this->assign('user_two',$user_two);
			$this->assign('user_one',$user_one);
		}
		if($_SESSION[login][role]=="摄影师"){
		
			if($_SESSION[login][type_style]==1){
				
				$userinfo=M('photoer');
				$where_one="account= '".$_SESSION[login][account]."'";
				$user_one=$userinfo->where($where_one)->select();
				$this->assign('user_one',$user_one);
				
			}
			else{
				$userinfo=M('photoer');
				$where_two="account='".$_SESSION[login][account]."'";
				$user_two=$userinfo->where($where_two)->select();
				$this->assign('user_two',$user_two);
				
				}
				
		}

		$_SESSION[type_one]=$_GET[t];
		//游客登录查看图片的判断
		if($_GET['role']==4){
			$_SESSION['login']['account']="游客";
			$_SESSION['login']['role']="游客";
		}
		if(($_GET[t]==5||$_GET[t]==6)&&$_SESSION['login']['account']=="游客"){
			$this->assign('jumpUrl',U("Home-Index/index"));
		    $this->error(L('游客不具有查看一周内的图片的权限，请首页登录。'));}
		
		if($_GET[p])
			$_SESSION[page][curpage]=$_GET[p];
		else
			$_SESSION[page][curpage]=1;
		//三级栏目中点中的是第几个
		if(!$_GET[w])
			$_SESSION[threecolumn]=0;
		else
			$_SESSION[threecolumn]=$_GET[w]%10;
					
		//二级栏目中点中的是第几个
		if(!$_GET[t])
			$url = "index.php?m=Main&a=decide&t=1";
					
		else
			$url = "index.php?m=Main&a=decide&t=".$_GET[t];
			
			//session 记录传递打开的链接（素材图片->自然-》）
					$get_w=$_GET[w]%10;
					$get_q=$_GET[q]/100;
					$get_q=$get_q%10;
					$_SESSION[recordplace]=$_GET[t]*100+$get_q*10+$get_w;	
					
		
		//二级栏目中点中的是第几个
		if(!$_GET[q])
			$currentcolumn =0;
		else if($_GET[t]==1||$_GET[t]==6||$_GET[t]==5||$_GET[t]==4)
			$currentcolumn=($_GET[q]-1000)/100;
		else
			$currentcolumn=($_GET[q]-$_GET[t]*1000)/100;
		$_SESSION[currentcloumn]=$currentcolumn;	
		$_SESSION[menuurl]=$url;
		
				
		//获取点击下一页时的url地址
		if(!$_GET[q]){
			if($_GET[w])
				$_SESSION[nexturl]=$url."$w=".$_GET[w];
			else
				$_SESSION[nexturl]=$url;
		}
		else{
			if($_GET[w])
				$_SESSION[nexturl]=$url."$q=".$_GET[q]."$w=".$_GET[w];
			else
				$_SESSION[nexturl]=$url."$q=".$_GET[q];}
					
					
			
				
		//传递三级栏目地址
		if(!$_GET[q]){
			if($_GET[t]==1||$_GET[t]==6)
				$_SESSION[columnurl]=$url."&q=1100";
			elseif($_GET[t]==5||$_GET[t]==2||$_GET[t]==3)
				$_SESSION[columnurl]=$url;
					
			else
				$_SESSION[columnurl]=$url."&q=".($_GET[t]*1000+100);
		}
		else
			$_SESSION[columnurl]=$url."&q=".$_GET[q];
				
		//搜素图片
		// 设置时间 今日  一周 一月
		$curDate=date('Y-m-d 00:00:01',time());
		$time1=strtotime($curDate);
		$time7=time()-7*24*3600;
		$time31=time()-31*24*3600;
		
		if($_GET[t]==5)
			$wherephp="state=1 and up_time >".$time1;
		elseif($_GET[t]==6)
			$wherephp="state=1 and up_time >".$time7;
		elseif($_GET[t]==4)
			$wherephp="state=1 and up_time >".$time31;
		else
			$wherephp="state=1";
		
		if(!$_GET[w]){
				if($_GET[t]==5||$_GET[t]==6||$_GET[t]==4){
					if(!$_GET[q])
					{
						$wherephp=$wherephp." and (type_one<2000 and type_one>=1000)";	}
					else
						$wherephp=$wherephp." and (type_one<".($_GET[q]+100)." and type_one>=".$_GET[q].")";
				}
				else if($_GET[t]==1){
					if(!$_GET[q])
						$wherephp="state=1";
					else
						$wherephp=$wherephp." and  (type_one<".($_GET[q]+100)." and type_one>=".$_GET[q].")";					
					}
				else{
					if(!$_GET[q]){
						$downnumber=($_GET[t])*1000;
						$upnumber=($_GET[t]+1)*1000;
						$wherephp="state=1 and (type_one<".$upnumber." and type_one>=".$downnumber.")";
							}
					else
						$wherephp="state=1 and (type_one<".($_GET[q]+100)." and type_one>=".$_GET[q].")";
					
					}
				}
			else
				$wherephp=$wherephp." and  type_one= ".$_GET[w];
	
				
			if(!$_GET[t]){
				$_GET[t]=1000;//默认显示2为新闻图片
				$where="type_one<2000";
			}
			else if($_GET[t]==1||$_GET[t]==6||$_GET[t]==5||$_GET[t]==4){
					$second=1000;	
			}
			else{
				$downnumber=($_GET[t]-1)*1000;
				$second=$_GET[t]*1000;
					
					}
					//搜索二级栏目
				$decidestype=M('Column');
		
				$where="parentid=$second";
				
				$menu=$decidestype->where($where)->select();
				$this->assign('menu',$menu);
				//搜索三级栏目
				if(!$_GET[q]){
					
					
					if($_GET[t]==1||$_GET[t]==6||$_GET[t]==5||$_GET[t]==4)
						$thirdwhere="parentid=1100";
					else
							$thirdwhere="parentid=".($_GET[t]*1000+100);
					
				}
				else{
					$thirdwhere="parentid=".$_GET[q];
				}
				//搜素图片
				
				$thirdcolumn=$decidestype->where($thirdwhere)->select();
			
				$this->assign('thirdcolumn',$thirdcolumn);
			
		
				if(!$_POST[selectvalue]){
					if(!$_SESSION[page][currentshowpages])	
						$_POST[selectvalue]=20;	
					else
						$_POST[selectvalue]=$_SESSION[page][currentshowpages];
				}	
				
						if($_GET[t]==5)
							$_SESSION[first]="今日图组>>";
						else if($_GET[t]==2)
							$_SESSION[first]="专题>>";
						else if($_GET[t]==3)
							$_SESSION[first]="资料图片>>";
						else if($_GET[t]==4)
							$_SESSION[first]="一月图组>>";
						else if($_GET[t]==1)
							$_SESSION[first]="图片库>>";
						if($_GET[t]==6)
							$_SESSION[first]="一周图组>>";
					$url="index.php?m=Main&a=decide&t=".$_GET[t];
					$_SESSION['firstnurl']=$url;
					
					if($_GET[q]){
						$selectwhere="groupid=".$_GET[q];
						$navigation1=$decidestype->where($selectwhere)->find();
						$_SESSION[secondation]=$navigation1[name].">>";
						$url="index.php?m=Main&a=decide&t=".$_GET[t]."&q=".$_GET[q];	
						$_SESSION[secondnurl]=$url;
					}
					else
						$_SESSION[secondation]="";
					if($_GET[w]){
						$selectwhere="groupid=".$_GET[w];
						$navigation2=$decidestype->where($selectwhere)->find();
						$_SESSION[third]=$navigation2[name];
						if($_GET[w])
							$url="index.php?m=Main&a=decide&t=".$_GET[t]."&q=".$_GET[q]."&w=".$_GET[w];	
						else
							$url="index.php?m=Main&a=decide&t=".$_GET[t]."&w=".$_GET[w];
						$_SESSION[thirdurl]=$url;
					}
					else
						$_SESSION[third]="";
					
				//游客排除查看一周内的图片
				if($_SESSION['login']['account']=="游客"){
					if($_GET[t]==1||$_GET[t]==6||$_GET[t]==5||$_GET[t]==4)
						$wherephp=" up_time< ".$time7." and (".$wherephp.")";
				}
				
				import('@.ORG.Page');
				$user=M('group_detail');
				$count=$user->where($wherephp)->count();		
				$page=new Page($count,$_POST[selectvalue]);
				//总共多少页
				$_SESSION[page][pagecount]=ceil($count/$_POST[selectvalue]);
				
				$list=$user->order('up_time desc')->where($wherephp)->limit($page->firstRow.','.$page->listRows)->select();
				$count=count($list);
				//显示组图片的地点
				for($i=0;$i<$count;$i++){
			$imageswhere="group_id=".$list[$i]['id'];
			$imageinfo=M('images_detail');
			$voimage=$imageinfo->where($imageswhere)->find();
			$list[$i]['city']=$voimage['city'];
			
			$userwhere="account='".$list[$i]['up_account']."'";
			
			$user=M('photoer');
			$username=$user->where($userwhere)->find();
			$list[$i]['name']=$username['name'];
			}
			
				$this->assign('list',$list);
				$_SESSION[page][currentshowpages]=$_POST[selectvalue];		
				$this->display();	
	}
	
	public function usershow(){
		
		if($_SESSION[login][role]=="游客"){
			
			$this->assign('jumpUrl',U("Home-Index/index"));
		    $this->error(L('游客不能查看记者的相关图片，请登录以方便查看'));
			}
		
		if($_SESSION[login][role]=="下载用户"){
			$userinfo=M('photoer');
			$where_one="type_style=1 and state=0";
			$where_two="type_style=0 and state=0";
			$user_one=$userinfo->where($where_one)->select();
			$user_two=$userinfo->where($where_two)->select();
			$this->assign('user_two',$user_two);
			$this->assign('user_one',$user_one);
		}
		if($_SESSION[login][role]=="摄影师"){
		
			if($_SESSION[login][type_style]==1){
				
				$userinfo=M('photoer');
				$where_one="account= '".$_SESSION[login][account]."'";
				$user_one=$userinfo->where($where_one)->select();
				$this->assign('user_one',$user_one);
				
			}
			else{
				$userinfo=M('photoer');
				$where_two="account='".$_SESSION[login][account]."'";
				$user_one=$userinfo->where($where_two)->select();
				$this->assign('user_two',$user_two);
				
				}
				
		}

		
		if(!$_POST[selectvalue]){
					if(!$_SESSION[page][currentshowpages])	
						$_POST[selectvalue]=20;	
					else
						$_POST[selectvalue]=$_SESSION[page][currentshowpages];
				}	
		
		if($_GET[p])
			$_SESSION[page][curpage]=$_GET[p];
		else
			$_SESSION[page][curpage]=1;
		
			$_SESSION[threecolumn]=0;

		//获取点击下一页时的url地址
		$url="index.php?m=Main&a=usershow&id=".$_GET[id]."&p=".$_SESSION[page][curpage];
		
		$_SESSION[nexturl]=$url;
		$photowhere="id=".$_GET[id];
		$photoer=M('photoer');
		$photoinfo=$photoer->where($photowhere)->find();
		$groupwhere="up_account='".$photoinfo['account']."'";
		$groupp=M('group_detail');
		
		import('@.ORG.Page');
		$count=$groupp->where($photowhere)->count();
		$page=new Page($count,$_POST[selectvalue]);
				//总共多少页
		$_SESSION[page][pagecount]=ceil($count/$_POST[selectvalue]);
		
		$list=$groupp->order('up_time desc')->where($groupwhere)->limit($page->firstRow.','.$page->listRows)->select();
		$count=count($list);
				//显示组图片的地点
		for($i=0;$i<$count;$i++){
			$imageswhere="group_id=".$list[$i]['id'];
			$imageinfo=M('images_detail');
			$voimage=$imageinfo->where($imageswhere)->find();
			$list[$i]['city']=$voimage['city'];
			
			$userwhere="account='".$list[$i]['up_account']."'";
			
			$user=M('photoer');
			$username=$user->where($userwhere)->find();
			$list[$i]['name']=$username['name'];
		}
			//栏目位置
			$_SESSION[first]=$photoinfo['name']."上传的图片";
			$_SESSION[secondation]="";
			$_SESSION[third]="";
			
		$this->assign('list',$list);
		$_SESSION[page][currentshowpages]=$_POST[selectvalue];		
		$this->display();		
		}
 
}

?>